#include <vector>
#include <unordered_set>

class Node {
  protected:
    char data;
    int id;

  public:
    Node(char name, int loc) {
      data = name;
      id = loc;
    }

    char getData() {
      return data;
    }

    int getLoc() {
      return id;
    }
};

class Graph {
  protected:
    std::vector<Node *> *nodes;
    std::vector<std::vector<Node *> *> *edgeList;
    int numNodes;

  public:
    Graph() {
      nodes = new std::vector<Node *>();
      edgeList = new std::vector<std::vector<Node *> *>();
      numNodes = 0;
    }
    ~Graph() {
      delete nodes;
      delete edgeList;
    }

    void addNode(char data) {
      Node *next = new Node(data, numNodes);
      nodes->push_back(next);
      std::vector<Node *> *nextDestList = new std::vector<Node *>();
      edgeList->push_back(nextDestList);
      numNodes ++;
    }

    bool addEdge(char src, char dest) {
      /* Find the start node */
      unsigned int index = 0;
      unsigned int size = nodes->size();
      unsigned int srcInd = -1;
      unsigned int destInd = -1;
      Node *srcNode = NULL;
      Node *destNode = NULL;
      while (index < size) {
        char nextNodeId = nodes->at(index)->getData();
        if (nextNodeId == src) {
          srcInd = index;
          srcNode = nodes->at(index);
        } else if (nextNodeId == dest) {
          destInd = index;
          destNode = nodes->at(index);
        }
        index ++;
      }
      if (srcInd == (-1) || destNode == NULL) {
        return false;
      }
      
      bool forwardExists = false;
      for (index = 0; index < edgeList->at(srcInd)->size(); index ++) {
        if (edgeList->at(srcInd)->at(index)->getData() == dest) {
          forwardExists = true;
        }
      }
      if (!forwardExists) {
        edgeList->at(srcInd)->push_back(destNode);
      }
      
      for (index = 0; index < edgeList->at(destInd)->size(); index ++) {
        if (edgeList->at(destInd)->at(index)->getData() == src) {
          return true;
        }
      }
      edgeList->at(destInd)->push_back(srcNode);
      
      return true;
    }

    void printBFS(char srcName) {
      /* Find the start node */
      unsigned int index = 0;
      unsigned int size = nodes->size();
      unsigned int srcInd = -1;
      while (index < size) {
        if (nodes->at(index)->getData() == srcName) {
          srcInd = index;
          index = size;
        }
        index ++;
      }
      if (srcInd == (-1)) {
        return;
      }

      /* Now, do the actual BFS search */
      std::cout << "===========" << std::endl;
      std::vector<Node *> *queue = new std::vector<Node *>();
      std::vector<int> *nodeLevels = new std::vector<int>();
      std::unordered_set<Node *> *seen = new std::unordered_set<Node*>();
      queue->push_back(nodes->at(srcInd));
      nodeLevels->push_back(0);
      seen->insert(nodes->at(srcInd));
      int level = 0;
      unsigned int node_iter = 0;
      while (queue->size() > 0) {
        /* Pre-visit (just print the node data for this application) */
        Node *next = queue->front();
        if (level < nodeLevels->at(node_iter)) {
          std::cout << std::endl;
          level ++;
        } else if (level > 0) {
          std::cout << ", ";
        }
        std::cout << next->getData();
        queue->erase(queue->begin());

        for (index = 0; index < edgeList->at(next->getLoc())->size(); index ++) {
          Node *neighbor = edgeList->at(next->getLoc())->at(index);
          if (seen->find(neighbor) == seen->end()) {
            queue->push_back(neighbor);
            seen->insert(neighbor);
            nodeLevels->push_back(level+1);
          }
        }
        node_iter ++;
      }
      std::cout << std::endl;
      std::cout << "===========" << std::endl;
    }
};
