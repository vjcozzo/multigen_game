/* 
    
    
 */

#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

int main(int argc, char **argv) {
  std::ifstream inpFile("testInput2.txt", std::ios::in);
  
  std::string lineBuffer, crBuffer;
//  while (std::getline(inpFile, lineBuffer)) {
//    std::cout<<lineBuffer<<std::endl;
//    std::stringstream ss(lineBuffer);
//    while (std::getline(ss, crBuffer, '\r')) {
///*      names->push_back(std::string(crBuffer));*/
//      std::cout<<crBuffer<<std::endl;
//    }
//  }
  int x = 0;
  if (inpFile.is_open()) {
    while (!inpFile.eof()) {
      std::string token/*, buffer*/;
      inpFile>>token;
/*      std::cout<<token<<std::endl;*/
      
/*      std::regex pattern("(\\d+)", std::regex::extended);*/
      std::regex pattern("^(\\d+)|(\\(\\d+\\))$"/*, std::regex::basic*/);
      std::sregex_iterator startOfList = std::sregex_iterator(token.begin(),
                                          token.end(), pattern);
      std::sregex_iterator endOfList = std::sregex_iterator();
      std::cout<<"There are "<< std::distance(startOfList, endOfList)
        <<" solutions"<<std::endl;
      for (std::sregex_iterator iter = startOfList; iter != endOfList; iter ++) {
        std::smatch next_match = *iter;
        std::string strForm = next_match.str();
        
/*        std::cout << strForm << std::endl;*/
        std::stringstream(token) >> x;
        std::cout<<"Updated value of x: "<<x<<std::endl;
      }
    }
  }

//  int x = 0;
//  inpFile >> x;
//  std::cout << x << std::endl;

  inpFile.close();
  
  return 0;
}
