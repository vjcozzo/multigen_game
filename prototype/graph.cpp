#include <iostream>
#include "graph.hpp"

int main() {
  Graph *testGraph = new Graph();

  testGraph->addNode('A');
  testGraph->addNode('B');
  testGraph->addNode('C');
  testGraph->addNode('D');
  testGraph->addNode('E');
  testGraph->addNode('F');
  testGraph->addNode('G');
  testGraph->addNode('H');
  testGraph->addNode('I');
  testGraph->addNode('J');
  testGraph->addNode('M');

  testGraph->addEdge('A', 'B');
  testGraph->addEdge('C', 'B');
  testGraph->addEdge('D', 'B');
  testGraph->addEdge('A', 'F');
  testGraph->addEdge('F', 'G');
  testGraph->addEdge('G', 'H');
  testGraph->addEdge('H', 'I');
  testGraph->addEdge('I', 'J');
  testGraph->addEdge('J', 'H');
  testGraph->addEdge('J', 'M');

  testGraph->printBFS('A');
  std::cout << std::endl;
  std::cout << std::endl;
  testGraph->printBFS('B');
  std::cout << std::endl;
  std::cout << std::endl;
  testGraph->printBFS('J');

  return 0;
}
