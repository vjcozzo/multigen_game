#pragma once

/* Sentinel value!! Set it once here,
 * use it everywhere! And probably DO NOT CHANGE ANYWHERE
 * (specifically, it must be larger than the maximum
 *  possible edge cost) */
#define INFINITE_DIST 0xFFFF

#ifdef __linux__
  #include <SDL2/SDL.h>
#else
  #include <SDL.h>
#endif

#include <random>

/* First: Let's make a global PRNG */
/* This one works when compiling for 64-bit machines only. */
static std::mt19937_64 generator(1559);

/* TODO maybe change these arrays to Maps from TERRAIN_TYPE to int?
 * Then we could also read it from a file and make it dynamically set */
const int EVASION_BONUS[7] = {
    /* Ocean */ 0,
    /* Plains */ 0,
    /* Forest */ 15,
    /* Hills */ 10,
    /* Ice */ 0,
    /* Sand */ 0,
    /* Mountains */ 30
};

const int DEF_BONUS[7] = {
    /* Ocean */ 0,
    /* Plains */ 0,
    /* Forest */ 1,
    /* Hills */ 2,
    /* Ice */ 0,
    /* Sand */ 0,
    /* Mountains */ 3
};

const int MOVE_COST[7] = {
    /* Ocean */ INFINITE_DIST,
    /* Plains */ 1,
    /* Forest */ 1,
    /* Hills */ 1,
    /* Ice */ 1,
    /* Sand */ 1,
    /* Mountains */ 1};
const int EXP_NEEDED[13] = {100, 300, 600, 1000, 1500,
  2100, 2800, 3600, 4500, 5500, 6600, 7800, 9100 };

namespace SDL_Formats {
    static SDL_PixelFormat *rgba_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888);
};

