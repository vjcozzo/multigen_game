SHELL=/bin/bash
CXX=$(CROSS)g++
LD=$(CROSS)ld
AR=$(CROSS)ar
PKG_CONFIG=$(CROSS)pkg-config

#CXX=clang
#CXXFLAGS=-I/usr/local/include/ -Wall -Wextra -std=c++11 # -Wshadow -Werror
CXXFLAGS=-I./include/ -Wall -Wextra -std=c++11 # -Wshadow -Werror
#CXXFLAGS=-I/usr/local/include/SDL2 -Wall -Wextra -std=c++11 # -Wshadow -Werror
# Flags for the RELEASE branch (no debugging symbols)
REL_DYN=-O2
REL_STATIC=-O2 -static
# Flags for the DEBUG branch (debugging symbols active)
DEBUG_DYN=-g -O0
DEBUG_STC=-g -O0 -static
# Flags for the OPTIMIZE_SIZE branch
SIZE_DYN=-Os
SIZE_STC=-Os -static

# Library flags from allegro 5, obtained via pkg-config
#LIBS=-lallegro_primitives -lallegro_main -lallegro_acodec -lallegro_dialog -lallegro_ttf -lallegro_audio -lallegro_font -lallegro_image -lallegro
#LIBS=-L/usr/local/lib -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf 
LIBS=-L./dynamic-libs -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf 
# -I/opt/SDL,/opt/SDL2_image-2.0.3,/opt/SDL2_mixer-2.0.2,/opt/SDL2_ttf-2.0.14
# -D_REENTRANT -I/usr/local/include/SDL2 -L/usr/local/lib -lSDL2_image -lSDL2_mixer -lSDL2_ttf -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags -lSDL2
STATIC_LIBS=-D_REENTRANT -I/usr/local/include/SDL2 -L/usr/local/lib -Wl,-rpath,/usr/local/lib -Wl,--enable-new-dtags -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -Wl,--no-undefined -lm -ldl -lpthread -lrt
SAVED_DATE=`/bin/date +%F`
EXEC=oo-tmb game-loop
SHARED_DIR=/usr/lib # dynamic-libs/

# Last attempt at a cross compiler command
#clang++ -c -std=c++11 -stdlib=libc++ --target=x86_64-w64-ming32 -o game-loop-exe-obj.o ./src/game-loop.cpp

# The following line is very specific to my machine.
# FUTURE DEVELOPERS - Please change it when running on your machine!
# you could also have it saved to an external drive for security.
BACKUP_DIR=/home/chozorho/git/chocorho/mgg-game-release

execs: $(EXEC) # major executable files, then make clean
#all: game-loop oo-tmb set-config clean release backups
all: game-loop oo-tmb release backups
#all: aio oo-tmb release backups
#objects: game-loop oo-tmb set-config release backups# NOTE: NO `clean` !

.PHONY: clean
clean:
	@echo "Cleaning up all object files..."
	@rm -f *.o bin/*.o
	@echo "Finished cleaning."

SDL_FontCache.o: src/SDL_FontCache.hpp # Compile it here? Or TODO make a static library?
	${CXX} -c -o obj/dyn-pie/release/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dyn-pie/debug/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_DEBUG} -pie -fPIE
	${CXX} -c -o obj/dynamic/release/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_DYN}
	${CXX} -c -o obj/dynamic/debug/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${DEBUG_DYN}
	${CXX} -c -o obj/static/release/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/SDL_FontCache.o src/SDL_FontCache.cpp ${CXXFLAGS} ${DEBUG_STC}
#	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/SDL_FontCache.cpp

game-loop: SDL_FontCache.o animation.o tmb.o game-loop.o Game.cpp.o
	${CXX} -o bin/dynamic/release/game-loop obj/dynamic/release/SDL_FontCache.o obj/dynamic/release/Game.cpp.o obj/dynamic/release/animation.o obj/dynamic/release/tmb.cpp.o obj/dynamic/release/game-loop-rel.cpp.o -L${SHARED_DIR} ${LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/ # no pie
	${CXX} -o bin/dynamic/debug/game-loop obj/dynamic/debug/SDL_FontCache.o obj/dynamic/debug/Game.cpp.o obj/dynamic/debug/animation.o obj/dynamic/debug/tmb.cpp.o obj/dynamic/debug/game-loop-debug.cpp.o -L${SHARED_DIR} ${LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/ # no pie
	${CXX} -o bin/dynamic/opt_size/game-loop obj/dynamic/release/SDL_FontCache.o obj/dynamic/opt_size/Game.cpp.o obj/dynamic/opt_size/animation.o obj/dynamic/opt_size/tmb.cpp.o obj/dynamic/opt_size/game-loop.cpp.o -L${SHARED_DIR} ${LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/ # no pie
	${CXX} -o bin/static/release/game-loop obj/static/release/SDL_FontCache.o obj/static/release/animation.o obj/static/release/tmb.cpp.o obj/static/release/Game.cpp.o obj/static/release/game-loop.cpp.o ${STATIC_LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/
	${CXX} -o bin/static/debug/game-loop obj/static/debug/SDL_FontCache.o obj/static/debug/animation.o obj/static/debug/tmb.cpp.o obj/static/debug/Game.cpp.o obj/static/debug/game-loop.cpp.o ${STATIC_LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/
	${CXX} -o bin/static/opt_size/game-loop obj/static/release/SDL_FontCache.o obj/static/opt_size/animation.o obj/static/opt_size/tmb.cpp.o obj/static/opt_size/Game.cpp.o obj/static/opt_size/game-loop.cpp.o ${STATIC_LIBS} # -Wl,-rpath,for-static-release-dynamic-libs/
	${CXX} -o bin/libstd-static/release/game-loop obj/static/release/SDL_FontCache.o obj/static/release/animation.o obj/static/release/tmb.cpp.o obj/static/release/Game.cpp.o obj/static/release/game-loop.cpp.o ${STATIC_LIBS} # -static-libstdc++
	${CXX} -o bin/libstd-static/debug/game-loop obj/static/debug/SDL_FontCache.o obj/static/debug/animation.o obj/static/debug/tmb.cpp.o obj/static/debug/Game.cpp.o obj/static/debug/game-loop.cpp.o ${STATIC_LIBS} # -static-libstdc++
	${CXX} -o bin/libstd-static/opt_size/game-loop obj/static/release/SDL_FontCache.o obj/static/opt_size/animation.o obj/static/opt_size/tmb.cpp.o obj/static/opt_size/Game.cpp.o obj/static/opt_size/game-loop.cpp.o ${STATIC_LIBS} # -static-libstdc++
#	${CXX} -o bin/dyn-pie/release/game-loop obj/dynamic/release/game-loop-rel.cpp.o -L${SHARED_DIR} -pie -fPIE ${LIBS}
#	${CXX} -o bin/dyn-pie/debug/game-loop obj/dynamic/debug/game-loop-debug.cpp.o -L$(SHARED_DIR) -pie -fPIE ${LIBS}
#	${CXX} -o bin/dyn-pie/opt_size/game-loop obj/dynamic/opt_size/game-loop.cpp.o -L$(SHARED_DIR) -pie -fPIE ${LIBS}

animation.o: src/animation.hpp src/animation.cpp
	${CXX} -c -o obj/dynamic/release/animation.o src/animation.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/animation.o src/animation.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/animation.o src/animation.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/static/release/animation.o src/animation.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/animation.o src/animation.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/animation.o src/animation.cpp ${CXXFLAGS} ${SIZE_STC}

tmb.o: src/ai.hpp src/entity.hpp src/global.hpp src/graph.hpp src/item.hpp src/unit.hpp src/weapon.hpp src/SDL_FontCache.hpp src/tmb.cpp
	${CXX} -c -o obj/dynamic/release/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/static/release/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/tmb.cpp.o src/tmb.cpp ${CXXFLAGS} ${SIZE_STC}

Game.cpp.o: src/Game.hpp src/game-loop.hpp src/tmb.hpp src/global.hpp src/unit.hpp src/tmb.hpp src/animation.hpp src/animation.hpp src/entity.hpp src/item.hpp src/weapon.hpp src/SDL_FontCache.hpp src/Game.cpp
	${CXX} -c -o obj/dynamic/release/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/release/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${REL_DYN}
	${CXX} -c -o obj/dynamic/debug/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${DEBUG_DYN}
	${CXX} -c -o obj/dynamic/opt_size/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${SIZE_DYN}
	${CXX} -c -o obj/static/release/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/Game.cpp.o src/Game.cpp ${CXXFLAGS} ${SIZE_STC}

game-loop.o: src/*.hpp
	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/main.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/main.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/main.cpp ${CXXFLAGS} ${REL_DYN}
	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/main.cpp ${CXXFLAGS} ${DEBUG_DYN}
	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${SIZE_DYN}
	${CXX} -c -o obj/static/release/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/game-loop.cpp.o src/main.cpp ${CXXFLAGS} ${SIZE_STC}
#	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/game-loop.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/game-loop.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
#	${CXX} -c -o obj/dynamic/release/game-loop-rel.cpp.o src/game-loop.cpp ${CXXFLAGS} ${REL_DYN}
#	${CXX} -c -o obj/dynamic/debug/game-loop-debug.cpp.o src/game-loop.cpp ${CXXFLAGS} ${DEBUG_DYN}
#	${CXX} -c -o obj/dynamic/opt_size/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${SIZE_DYN}
#	${CXX} -c -o obj/static/release/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${REL_STATIC}
#	${CXX} -c -o obj/static/debug/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${DEBUG_STC}
#	${CXX} -c -o obj/static/opt_size/game-loop.cpp.o src/game-loop.cpp ${CXXFLAGS} ${SIZE_STC}

oo-tmb: SDL_FontCache.o animation.o tmb.o oo-tmb.o
#	${CXX} -o bin/dyn-pie/release/oo-tmb bin/dyn-pie/release/oo-tmb-rel.cpp.o -L${SHARED_DIR} -pie -fPIE ${LIBS} # -Wl,rpath=./$(SHARED_DIR) -z nodefaultlib
#	${CXX} -o bin/dyn-pie/debug/oo-tmb bin/dyn-pie/debug/oo-tmb-debug.cpp.o -L${SHARED_DIR} -pie -fPIE ${LIBS} # -Wl,rpath=./$(SHARED_DIR) -z nodefaultlib
#	${CXX} -o bin/dyn-pie/opt_size/oo-tmb bin/dyn-pie/opt_size/oo-tmb.cpp.o -L${SHARED_DIR} -pie -fPIE ${LIBS}
	${CXX} -o bin/static/release/oo-tmb obj/static/release/animation.o obj/static/release/SDL_FontCache.o obj/static/release/tmb.cpp.o obj/static/release/oo-tmb-rel.cpp.o ${STATIC_LIBS}
	${CXX} -o bin/static/debug/oo-tmb obj/static/release/animation.o obj/static/release/SDL_FontCache.o obj/static/debug/tmb.cpp.o obj/static/debug/oo-tmb-debug.cpp.o ${STATIC_LIBS}
	${CXX} -o bin/static/opt_size/oo-tmb obj/static/release/animation.o obj/static/release/SDL_FontCache.o obj/static/opt_size/tmb.cpp.o obj/static/opt_size/oo-tmb.cpp.o ${STATIC_LIBS}

oo-tmb.o: src/tmb.hpp
#	${CXX} -c -o obj/dyn-pie/release/oo-tmb-rel.cpp.o src/oo-tmb.cpp ${CXXFLAGS} ${REL_DYN} -pie -fPIE
#	${CXX} -c -o obj/dyn-pie/debug/oo-tmb-debug.cpp.o src/oo-tmb.cpp ${CXXFLAGS} ${DEBUG_DYN} -pie -fPIE
#	${CXX} -c -o obj/dyn-pie/opt_size/oo-tmb.cpp.o src/oo-tmb.cpp ${CXXFLAGS} ${SIZE_DYN} -pie -fPIE
	${CXX} -c -o obj/static/release/oo-tmb-rel.cpp.o src/oo-tmb.cpp ${CXXFLAGS} ${REL_STATIC}
	${CXX} -c -o obj/static/debug/oo-tmb-debug.cpp.o src/oo-tmb.cpp ${CXXFLAGS} ${DEBUG_STC}
	${CXX} -c -o obj/static/opt_size/oo-tmb.cpp.o src/oo-tmb.cpp ${CXXFLAGS} ${SIZE_STC}
#
#set-config: set-config.o
#	$(CXX) -o bin/dyn-pie/release/set-config set-config-rel.o -L$(SHARED_DIR) $(CXXFLAGS_rel) $(LIBS)
#	$(CXX) -o bin/dyn-pie/debug/set-config set-config-debug.o -L$(SHARED_DIR) $(CXXFLAGS_debug) $(LIBS)
#
#set-config.o: src/set-config.c src/set-config.h
#	$(CXX) -c -o set-config-rel.o src/set-config.c $(CXXFLAGS_rel)
#	$(CXX) -c -o set-config-debug.o src/set-config.c $(CXXFLAGS_debug) 

release: gz_release bzip2_release
backups: gz_backup bzip2_backup

emergency:
	tar -czf mgg-$(SAVED_DATE)-emergency.tar.gz *
	@wc -c mgg-$(SAVED_DATE)-emergency.tar.gz
	@cp -p mgg-$(SAVED_DATE)-emergency.tar.gz -t $(BACKUP_DIR) && echo "Success" || echo "ERR: Please be sure to copy the tarball (in parent directory) to the proper location."
	@sha512sum mgg-$(SAVED_DATE)-emergency.tar.gz | tee $(BACKUP_DIR)/$(SAVED_DATE).DIGEST.asc
	@mv mgg-$(SAVED_DATE)-emergency.tar.gz -t tarball/

gz_release:
	@echo "Creating release... "
#	tar -czf mgg-$(SAVED_DATE)-release.tar.gz bin/static/release/* bin/libstd-static/release/* bin/static/opt_size/* bin/libstd-static/opt_size/* res/* for-static-release-dynamic-libs/* static-libs/*
	tar -czf mgg-$(SAVED_DATE)-release.tar.gz bin/static/release/* bin/libstd-static/release/* res/* for-static-release-dynamic-libs/* static-libs/* changelog.txt
	@wc -c mgg-$(SAVED_DATE)-release.tar.gz
#	@cp -p mgg-$(SAVED_DATE)-release.tar.gz -t $(BACKUP_DIR) && echo "Success" || echo "ERR: Please be sure to copy the tarball (in parent directory) to the proper location."
#	@sha512sum mgg-$(SAVED_DATE)-release.tar.gz | tee $(BACKUP_DIR)/$(SAVED_DATE).DIGEST.asc
	@mv mgg-$(SAVED_DATE)-release.tar.gz -t tarball/

gz_backup:
	@echo "Creating backups... "
	tar -czf mgg-$(SAVED_DATE)-dev.tar.gz MGG/src/* MGG/res/* MGG/dynamic-libs/* MGG/for-static-release-dynamic-libs/* MGG/Makefile MGG/CMakeLists.txt MGG/changelog.txt MGG/bin/static/debug/*
#	@chmod -r ./src/*
	@wc -c mgg-$(SAVED_DATE)-dev.tar.gz
#	@cp -p mgg-$(SAVED_DATE)-dev.tar.gz -t $(BACKUP_DIR) && echo "Success" || echo "ERR: Please be sure to copy the tarball (in parent directory) to the proper location."
#	@sha512sum mgg-$(SAVED_DATE)-dev.tar.gz | tee $(BACKUP_DIR)/$(SAVED_DATE).DIGEST.asc
	@mv mgg-$(SAVED_DATE)-dev.tar.gz -t tarball/

bzip2_release:
	tar -cjf mgg-$(SAVED_DATE)-release.tar.bz2 bin/static/release/* bin/libstd-static/release/* res/* for-static-release-dynamic-libs/* static-libs/* changelog.txt
#	tar -cjf mgg-$(SAVED_DATE)-release.tar.bz2 bin/static/release/* bin/libstd-static/release/* bin/static/opt_size/* bin/libstd-static/opt_size/* res/* for-static-release-dynamic-libs/* static-libs/*
	@wc -c mgg-$(SAVED_DATE)-release.tar.bz2
#	@cp -p mgg-$(SAVED_DATE)-release.tar.bz2 -t $(BACKUP_DIR) && echo "Success" || echo "ERR: Please be sure to copy the tarball (in parent directory) to the proper location."
#	@sha512sum mgg-$(SAVED_DATE)-release.tar.bz2 | tee $(BACKUP_DIR)/$(SAVED_DATE).DIGEST.asc
	@mv mgg-$(SAVED_DATE)-release.tar.bz2 -t tarball/

bzip2_backup:
	cp -pi src/aio/*.cpp src/aio/*.hpp -t src/
	tar -cjf mgg-$(SAVED_DATE)-dev.tar.bz2 src/* res/* dynamic-libs/* static-libs/* for-static-release-dynamic-libs/* client-Makefiles/Makefile .*.sh changelog.txt # bin/*
	@chmod -r ./src/*
	@wc -c mgg-$(SAVED_DATE)-dev.tar.bz2
#	@cp -p mgg-$(SAVED_DATE)-dev.tar.bz2 -t $(BACKUP_DIR) && echo "Success" || echo "ERR: Please be sure to copy the tarball (in parent directory) to the proper location."
#	@sha512sum mgg-$(SAVED_DATE)-dev.tar.bz2 | tee -a $(BACKUP_DIR)/$(SAVED_DATE).DIGEST.asc
	@mv mgg-$(SAVED_DATE)-dev.tar.bz2 -t tarball/

devlog:
	@echo "Creating today's developer log... "
	touch logs/dev-log-$(SAVED_DATE).txt

devlog-send:
	@echo "Copying developer logs... "
	@cp -p dev-log-$(SAVED_DATE).txt -t $(BACKUP_DIR) && echo "Success" || echo "ERR: Failed to copy the developer logs to backup dir."

directories:
	@mkdir -p bin/{dyn-pie,dynamic,static,libstd-static}/{release,debug,opt_size}
	@mkdir -p obj/{dyn-pie,dynamic,static}/{release,debug,opt_size}


