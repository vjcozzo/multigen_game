#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS
#endif
#include "tmb.hpp"

/* TMB Constructor */
TacticalMapBattle::TacticalMapBattle(/*SDL_Renderer *render_displayTMB, */
                  const char *map_file_name, const char *terrain_types,
                  /*ALLEGRO_DISPLAY *render_displayTMB, */std::vector<Unit *> &players,
                  std::vector<Unit *> &enemies/*, int cond*/, int verbose_copy) {
  /* Initialize a PRNG? For enemy AI and other things
   * that will be done in this class... */
  verbosity = verbose_copy;
  /*      condition = cond;*/
#ifdef __linux__
  struct timespec ctt;
  clock_gettime(CLOCK_REALTIME, &ctt);
  long nano = ctt.tv_nsec;
  std::mt19937_64 gen(nano);
  generator = gen;
#elif _WIN64
  std::mt19937_64 gen(1557);
  generator = gen;
#endif
  
  terrain_board = nullptr;
  unit_board = nullptr;
  chosen = nullptr;
  target = nullptr;
  
  tmb_wind = SDL_CreateWindow(map_file_name, TMB_SCREEN_WIDTH, 0, TMB_SCREEN_WIDTH,
                              TMB_SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
  displayTMB = SDL_CreateRenderer(tmb_wind, -1, SDL_RENDERER_ACCELERATED);
  
  /* Load map data from files */
  load_map(map_file_name);
  load_terrain_tiles(terrain_types);
  player_units = players;
  enemy_units = enemies;
  
//  moved_set = new std::vector<Unit *>();
//  debilitated_players = new std::vector<Unit *>();
//  debilitated_enemies = new std::vector<Unit *>();
//  living_players = nullptr;
  
  game_state = 1;
  
  spawn_locate_units();
  load_units(player_units, enemy_units);
  int reserved_width = (TMB_SCREEN_WIDTH / 5);
  if ((TMB_SCREEN_HEIGHT / tiles_y) < ((TMB_SCREEN_WIDTH - reserved_width)/tiles_x)) {
    /*        printf("First if-case is true (because %d / %d is less than %d / %d)\n", TMB_SCREEN_HEIGHT, tiles_y, TMB_SCREEN_WIDTH-reserved_width, tiles_x);*/
    terrain_map_width = TMB_SCREEN_HEIGHT;
    tile_side_length = TMB_SCREEN_HEIGHT/tiles_y;
  } else {
    /*        printf("Second if-case is true (because %d / %d is NOT less than %d / %d)\n", TMB_SCREEN_HEIGHT, tiles_y, TMB_SCREEN_WIDTH-reserved_width, tiles_x);*/
    terrain_map_width = TMB_SCREEN_WIDTH-reserved_width;
    tile_side_length = ((TMB_SCREEN_WIDTH-reserved_width)/tiles_x);
  }
  terrain_map_width = tile_side_length*tiles_x;
  terrain_map_height = tile_side_length*tiles_y;
  terrain_base = SDL_CreateTexture(displayTMB, SDL_PIXELFORMAT_RGBA8888,
                                   SDL_TEXTUREACCESS_TARGET, terrain_map_width, terrain_map_height);
  layer2 = SDL_CreateTexture(displayTMB, SDL_PIXELFORMAT_RGBA8888,
                             SDL_TEXTUREACCESS_TARGET, terrain_map_width, terrain_map_height);
  SDL_SetTextureBlendMode(layer2, SDL_BLENDMODE_BLEND);
  menu_img = SDL_CreateTexture(displayTMB, SDL_PIXELFORMAT_RGBA8888,
                               SDL_TEXTUREACCESS_TARGET, TMB_SCREEN_WIDTH - terrain_map_width, terrain_map_height);
  if (menu_img == nullptr) {
    fprintf(stderr, "[DEBUG] FATAL ERR -- `menu_img` is null!\n");
    fprintf(stderr, "terrain_map_width = %d*%d = %d\n", tiles_x, tile_side_length, terrain_map_width);
    fprintf(stderr, "terrain_map_height = %d*%d = %d\n", tiles_y, tile_side_length, terrain_map_height);
  }
  
  cons_menu_options[0] = "Attack";
  cons_menu_options[1] = "Item";
  cons_menu_options[2] = "Trade";
  cons_menu_options[3] = "Rescue";
  cons_menu_options[4] = "Drop";
  cons_menu_options[5] = "Pass";
  cons_menu_options[6] = "Ability";
  cons_menu_options[7] = "Wait";
  cons_menu_options[8] = "Back";
  
  is_province = false;
  
  dframe = new SDL_Rect();
  
  font_luxirb_vvsmall = TTF_OpenFont("./res/fonts/luxirb.ttf", 16);
  if (font_luxirb_vvsmall == nullptr) {
    std::cerr << "Failed to load the font luxirb.ttf in VERY small size" << std::endl;
    SDL_DestroyRenderer(displayTMB);
    return;
  }
  font_luxirb_med = TTF_OpenFont("./res/fonts/luxirb.ttf", 36);
  if (font_luxirb_med == nullptr) {
    std::cerr << "Failed to load the font luxirb.ttf in medium size" << std::endl;
    SDL_DestroyRenderer(displayTMB);
    return;
  }
  SDL_Surface *enemy_phase_surf = TTF_RenderUTF8_Solid(font_luxirb_vvsmall, "ENEMY PHASE", black_text);
  enemy_phase_text = SDL_CreateTextureFromSurface(displayTMB, enemy_phase_surf);
  
  SDL_Surface *player_phase_surf = TTF_RenderUTF8_Solid(font_luxirb_vvsmall, "PLAYER PHASE", black_text);
  player_phase_text = SDL_CreateTextureFromSurface(displayTMB, player_phase_surf);
  
  SDL_Surface *victory_text_surf = TTF_RenderUTF8_Solid(font_luxirb_med, "*** VICTORY ***", black_text);
  victory_text = SDL_CreateTextureFromSurface(displayTMB, victory_text_surf);
  
  SDL_Surface *defeat_text_surf = TTF_RenderUTF8_Solid(font_luxirb_med, "*** DEFEAT ***", black_text);
  defeat_text = SDL_CreateTextureFromSurface(displayTMB, defeat_text_surf);
  
  redraw_mut = SDL_CreateMutex();
  phase_control = SDL_CreateMutex();
  cheat_control = SDL_CreateMutex();
  state_control = SDL_CreateMutex();
  
//  const SDL_MessageBoxData init_box = {SDL_MESSAGEBOX_INFORMATION, tmb_wind,
//    "Tactical Decision", "Enemies encountered! Do you want to autoresolve, retreat to a" \
//    " neighboring area, or fight it out?",
//    3, init_tmb_buttons, nullptr};
//  int result = 0;
//  SDL_ShowMessageBox(&init_box, &result);
//  if (result == 1) {
//    int autoRes = (generator() % 200);
//    int total = player_units.size() + enemy_units.size();
//    int threshold = (200 * (player_units.size())) / total;
//#ifdef DEBUG_MSG
//    std::cout << "[DEBUG]    auto-resolve value: " << autoRes << std::endl;
//    std::cout << "[DEBUG]    the passing threshold: " << threshold << std::endl;
//#endif
//    
//    /* TODO do some fancy math shit 
//     * to figure out how much damage to do to each side.
//     * 
//     * For now, keep it simple (for testing purposes). */
//    if (autoRes > (threshold + 10)) {
//      game_state = -1;
//    } else if (autoRes < (threshold - 10)) {
//      game_state = -2;
//    } else {
//      /* within ten points of the threshold -- */
//      /* in this case, only save the strongest unit
//       * on the winning side. */
//      if (autoRes >= threshold) {
//        int maxLev = -1;
//        size_t maxInd = 0;
//        /* NOTE, do NOT try replacing these with 
//         * for-each loops (at least not without 
//         * having some other way to store the 
//         * INDEXOF value)! */
///*        for (const Unit *iter : player_units) {
//          if (iter->getLevel() > maxLev) {
//            maxLev = iter->getLevel();
//            maxInd = ind;
//          }
//        }*/
//        for (size_t ind=0; ind < player_units.size(); ind ++) {
//          if (player_units.at(ind)->getLevel() > maxLev) {
//            maxLev = player_units.at(ind)->getLevel();
//            maxInd = ind;
//          }
//        }
//        for (size_t ind=0; ind < player_units.size(); ind ++) {
//          if (ind != maxInd) {
//            debilitated_players.push_back(player_units.at(ind));
//            player_units.erase(player_units.begin()+ind);
//            ind--;
//          }
//        }
//        game_state = -1;
//      } else {
//        debilitated_players = players;
//        int maxLev = -1;
//        size_t maxInd = 0;
//        /*for (const Unit *nextEU : enemy_units) {
//          if (nextEU->getLevel() > maxLev) {
//            maxLev = nextEU->getLevel();
//            maxInd = ind;
//          }
//        }*/
//        for (size_t ind=0; ind < enemy_units.size(); ind ++) {
//          if (enemy_units.at(ind)->getLevel() > maxLev) {
//            maxLev = enemy_units.at(ind)->getLevel();
//            maxInd = ind;
//          }
//        }
//        for (size_t ind=0; ind < enemy_units.size(); ind ++) {
//          if (ind != maxInd) {
//            debilitated_enemies.push_back(enemy_units.at(ind));
//            enemy_units.erase(enemy_units.begin()+ind);
//            ind--;
//          }
//        }
//        game_state = -2;
//      }
//    }
//  } else if (result == 2) {
//    game_state = -20;
//  } else /*if (result == 3) */{
//    /* basically a no-op */
//    game_state = 1;
//  }
}

/*virtual*/ TacticalMapBattle::~TacticalMapBattle() {
  unsigned int delete_ind;
  unsigned int upper_bound = NUM_STOCK_OPTIONS;
/*  delete moved_set;*/
/*  delete debilitated_players;*/
  enemy_units.clear();
/*  delete enemy_units;*/
  /*      delete cons_menu_options;*/
  delete dframe;
  //      leftover from the previous idea,
  //      viewing the graph as an adjacency matrix:
  for (int ind=0; ind < tiles_x*tiles_y; ind++) {
    delete graph[ind];
  }
  delete graph;
  for (int ind=0; ind < tiles_y; ind++) {
    delete locale[ind];
  }
  delete locale;
  
  TTF_CloseFont(font_luxirb_vvsmall);
  SDL_DestroyMutex(redraw_mut);
  SDL_DestroyMutex(phase_control);
  SDL_DestroyMutex(cheat_control);
  SDL_DestroyMutex(state_control);
}


int TacticalMapBattle::getTurnPhase() {
  SDL_LockMutex(phase_control);
  int turn_phase_cpy = turn_phase;
  SDL_UnlockMutex(phase_control);
  return turn_phase_cpy;
}
void TacticalMapBattle::setTurnPhase(int newTurn) {
  SDL_LockMutex(phase_control);
  turn_phase = newTurn;
  SDL_UnlockMutex(phase_control);
}

void TacticalMapBattle::repaint() {
  SDL_LockMutex(redraw_mut);
  redraw = true;
  SDL_UnlockMutex(redraw_mut);
}

bool TacticalMapBattle::isCheating() {
  SDL_LockMutex(cheat_control);
  bool cheat_cpy = cheat[2];
  SDL_UnlockMutex(cheat_control);
  return cheat_cpy;
}

int TacticalMapBattle::getGameState() {
  SDL_LockMutex(state_control);
  int g_state_copy = game_state;
  SDL_UnlockMutex(state_control);
  return g_state_copy;
}
void TacticalMapBattle::setGameState(int new_state) {
  SDL_LockMutex(state_control);
  game_state = new_state;
  SDL_UnlockMutex(state_control);
}

void TacticalMapBattle::clearMovedSet() {
  /*      SDL_LockMutex(moved_control);*/
  moved_set.clear();
  /*      SDL_UnlockMutex(moved_control);*/
}

/* Postcondition:
 *   will draw the TMB side menu ONTO the menu_img image object,
 *     based on the options listed in the vector `menu` */
void TacticalMapBattle::update_menu_img() {
  SDL_SetRenderTarget(displayTMB, menu_img);
  SDL_SetRenderDrawColor(displayTMB, 10, 70, 131, 121);
  SDL_RenderFillRect(displayTMB, nullptr);
  /* Halloween colors! */
  SDL_SetRenderDrawColor(displayTMB, 250, 120, 80, 60);
  unsigned int ind = 0;
  unsigned int upper_bound = menu.size();
  /*      std::cout << "menu size after the answer-click call: " << upper_bound << std::endl;*/
  if (TMB_SCREEN_WIDTH < 1000) {
    if (upper_bound) {
      FC_Font *fc_luxirb_vs = FC_CreateFont();
      FC_LoadFont(fc_luxirb_vs, displayTMB, "./res/fonts/luxirb.ttf", 16, FC_MakeColor(1,1,1,255), TTF_STYLE_NORMAL);
      if (fc_luxirb_vs == nullptr) {
        std::cerr<<"Failed to load luxirb.ttf in size 16"<<std::endl;
        SDL_DestroyRenderer(displayTMB);
        SDL_RemoveTimer(tmbTicker);
        SDL_RemoveTimer(tmbEnemyAI);
        SDL_SetRenderTarget(displayTMB, nullptr);
        return;
      }
      while (ind < upper_bound) {
        /*            printf("\tind = %d, %s\n", ind, menu.at(ind)->c_str());*/
        std::string next_choice = menu.at(ind);
        //            if (next_choice == nullptr) {
        //              std::cerr << "[!!] ERR - menu option is actually nullptr at index " << ind << " update_menu_img method" << std::endl;
        //            }/* else {
        //              std::cout << "Now processing menu option \'" << *next_choice << "\'" << std::endl;
        //            }*/
        SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);
        FC_Draw(fc_luxirb_vs, displayTMB, 8, 2+(ind*MENU_OPTION_HEIGHT/2), next_choice.c_str());
        ind ++;
      }
      FC_FreeFont(fc_luxirb_vs);
    }
  } else /*if()*/ {
    if (upper_bound) {
/*          TTF_Font *fc_luxirb_player = TTF_OpenFont("./res/fonts/luxirb.ttf", 12);*/
      FC_Font *fc_luxirb_vs = FC_CreateFont();
      FC_LoadFont(fc_luxirb_vs, displayTMB, "./res/fonts/luxirb.ttf", 12, FC_MakeColor(1,1,1,255), TTF_STYLE_NORMAL);
      if (fc_luxirb_vs == nullptr) {
        std::cerr<<"Failed to load luxirb.ttf in size 12"<<std::endl;
        SDL_DestroyRenderer(displayTMB);
        SDL_RemoveTimer(tmbTicker);
        SDL_RemoveTimer(tmbEnemyAI);
        SDL_SetRenderTarget(displayTMB, nullptr);
        return;
      }
      
      while (ind < upper_bound) {
        /*            printf("\tind = %d, %s\n", ind, menu.at(ind)->c_str());*/
        std::string next_choice = menu.at(ind);
        //            if (next_choice == nullptr) {
        //              std::cerr<<"[!!] ERR - menu option is actually nullptr at index "
        //                << ind << " update_menu_img method" << std::endl;
        //            }/* else {
        //              std::cout << "Now processing menu option \'" << *next_choice << "\'" << std::endl;
        //            }*/
        SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);
        FC_Draw(fc_luxirb_vs, displayTMB, 8, 2+(ind*MENU_OPTION_HEIGHT),
                next_choice.c_str());
        ind ++;
      }
      FC_FreeFont(fc_luxirb_vs);
    }
  }
  SDL_SetRenderTarget(displayTMB, nullptr);
}

/* Postcondition: Sets the values in the `menu' vector,
 * which consists of STRINGS to be printed later
 * in the ABOVE member function, update_menu_img(). */
void TacticalMapBattle::make_stop_menu() {
  unsigned int ind = 0;
  unsigned int upper_bound = menu.size();
  /*      std::cout << "Making the UNIT HALT menu... upper bound is " << upper_bound << std::endl;*/
  if (upper_bound > 0) {
    upper_bound--;
    menu.clear();
  }
  setGameState(EXPECT_MENU_CHOICE);
  
  if (can_attack(chosen)) {
    //std::cerr << "UNIT CAN ATTACK";
    menu.push_back(cons_menu_options[0]/*->at(0)*/);
  } else {
    //std::cerr << "UNIT CANNOT ATTACK";
  }
  if (chosen->hasItem()) {
    menu.push_back(cons_menu_options[1]/*->at(1)*/);
  }
  if (can_trade(chosen)) {
    menu.push_back(cons_menu_options[2]/*->at(2)*/);
  }
  if (can_rescue(chosen)) {
    menu.push_back(cons_menu_options[3]/*->at(3)*/);
  }
  if (can_drop(chosen)) {
    menu.push_back(cons_menu_options[4]/*->at(4)*/);
  }
  if (can_pass(chosen)) {
    menu.push_back(cons_menu_options[5]/*->at(5)*/);
  }
  if (chosen->getCanAbility()) {
    menu.push_back(cons_menu_options[6]/*->at(6)*/);
  }
  menu.push_back(cons_menu_options[7]/*->at(7)*/);
}

void TacticalMapBattle::renderNextChosenAnimationFrame(int dCoordX, int dCoordY) {
  /* The main Render Sequence will be:
   * terrain, then all OTHER Units, then the moving unit. */
  SDL_SetRenderTarget(displayTMB, nullptr);
  SDL_SetRenderDrawColor(displayTMB, 255, 255, 255, 255);
  SDL_RenderClear(displayTMB);
  display_terrain_map(0);
  
  /* For good measure, also render the RHS menu img */
  SDL_QueryTexture(menu_img, nullptr, nullptr, &(dframe->w),
    &(dframe->h));
  dframe->x = terrain_map_width+1;
  dframe->y = 1;
  SDL_RenderCopy(displayTMB, menu_img, nullptr, dframe);
  
  /* The following call is fine, AS LONG AS `chosen` is not 
   * actually anywhere in the `unit_board` array */
  display_units(tile_side_length, tile_side_length);
  SDL_SetRenderTarget(displayTMB, nullptr);
  display_chosen_unit_frame(tile_side_length, tile_side_length,
    chosen, dCoordX, dCoordY, false);
  SDL_RenderPresent(displayTMB);
  SDL_Delay(1000 / FPS);
}

/* Preconditions:
 *  x_click is a positive value representing the x-coordinate of the click;
 *  y_click is a positive value representing the y-coordinate of the click;
 * Postcondition:
 *  Processes the user's selection and updates the game state. */
int TacticalMapBattle::answer_click(int x_click, int y_click) {
  int x_ind = (x_click / tile_side_length);
  int y_ind = (y_click / tile_side_length);
  /*      std::cout<<"In \'answer_click\':"<<std::endl<<"y_ind "<<y_ind
   <<"; x_ind "<<x_ind<<std::endl;*/
  switch (game_state) {
    case IDLE: /* ** No unit selected yet; Unit selection expected ** */
    {
      if ((x_click < terrain_map_width) &&
          (y_click < terrain_map_height)) {
        Unit *probe = unit_board[y_ind][x_ind];
        if ((probe != nullptr) && (probe->getTeam() == 0) &&
            !(has_moved(probe))) {
          setGameState(EXPECT_DEST);
          chosen = probe;
          
          /* SHOW MOVEMENT RANGE HERE */
          /*                SDL_SetRenderTarget(displayTMB, layer2);*/
          SDL_SetRenderTarget(displayTMB, menu_img);
          SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
          SDL_RenderFillRect(displayTMB, nullptr);
          /*              if (al_get_new_bitmap_flags() & ALLEGRO_FORCE_LOCKING) {
           al_set_new_bitmap_flags(ALLEGRO_VIDEO_BITMAP);
           std::cout<<"Within the show move range force locking is active WOO"<<std::endl;
           } else {
           al_set_new_bitmap_flags(ALLEGRO_FORCE_LOCKING);
           std::cout<<"within the show move range force Locking off..."<<std::endl;
           }*/
          show_move_range(probe->getSpeed(), x_ind, y_ind);
          SDL_SetRenderTarget(displayTMB, nullptr);
          /*              show_move_range_flying(probe->getSpeed(), x_ind, y_ind);*/
          
          /* First, we can primitively find the weapon
           * with the greatest range */
          unsigned int weapon_ind = 0;
          int maxR = -5;
          int minR = tiles_x+tiles_y;
          while (probe->getWeapons()[weapon_ind] != nullptr
                 && weapon_ind < WEAPON_CAPACITY) {
            Weapon *nextW =  probe->getWeapons()[weapon_ind];
            if (maxR < nextW->getMaxRange()) {
              maxR = nextW->getMaxRange();
            }
            if (minR > nextW->getMinRange()) {
              minR = nextW->getMinRange();
            }
            weapon_ind ++;
          }
          /*              show_weapon_range(maxR, minR-1,  x_ind, y_ind);*/
        } /*else if (probe != nullptr && probe->getTeam() == 1) {
           std::cout<<"unit_board is definitely non-nullptr, but that unit is an enemy."<<std::endl;
           }*/
        if (verbosity) {
          std::cout<<"Just set \'chosen\' to the Unit at ("<<y_ind<<", "<<x_ind<<")"<<std::endl;
        }
        return 0;
      } else {
        return -1;
      }
      break;
    }
    case EXPECT_DEST: /* ** Destination click was expected ** */
    {
      if ((x_click < terrain_map_width) &&
          (y_click < terrain_map_height)) {
        if (canMove(chosen, x_ind, y_ind)) {
          if (locale[y_ind][x_ind] <= chosen->getSpeed()) {
            SDL_SetRenderTarget(displayTMB, layer2);
            SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
            SDL_RenderFillRect(displayTMB, nullptr);
            SDL_SetRenderTarget(displayTMB, nullptr);
            
            unit_board[chosen->getY()][chosen->getX()] = nullptr;
            
            if (showAnimation) {
              /* Now, for something ambitious:
               * Play a movement animation! */
  
              /* Step 0. Now that the heap is empty and all
               * nodes have been processed AND the destination is now well-defined,
               * develop the actual PATH structure, a std::list. */
              Node dest = Node(x_ind, y_ind);
              path.push_front(Entity(dest.getX(), dest.getY()));
              Entity src = previous.at(dest);
              /* IF src is a valid key inside the previous structure... */
              while (Node(src.getX(), src.getY()) !=
                     Node(chosen->getX(), chosen->getY()) &&
                     previous.find(Node(src.getX(), src.getY())) != previous.end()) {
                path.push_front(src); /* then APPEND to the PATH structure ;) */
                dest = Node(src.getX(), src.getY());
                src = previous.at(Node(src.getX(), src.getY()));
                
              /* For Debug purposes only!
                std::cout<<"[DEBUG]    Next node in the sequence is ("<<src.getX()
                  <<", "<<src.getY()<<")"<<std::endl;
                SDL_Delay(200);*/
              }
              int curX = tile_side_length*(chosen->getX());
              int curY = tile_side_length*(chosen->getY());
              int block = (tile_side_length)/FRAMES_PER_TILE;
              float excess = 0.0f;
              
              SDL_SetRenderTarget(displayTMB, menu_img);
              SDL_SetRenderDrawColor(displayTMB, 10, 70, 131, 121);
              SDL_RenderClear(displayTMB);
              /* Step 1. cycle through the intermediate tiles in the path,
               * until you get to the destination */
              /* Idea in use: change the Unit position, and
               * experiment with making it a FRACTION of the
               * way toward the next tile!!! */
              /* TODO maybe refactor this into its own function too. */
              for (const auto &nextTile : path) {
                int nextX = nextTile.getX();
                int nextY = nextTile.getY();
                if (chosen->getX() < nextX) { /* EAST */
                  chosen->setDir(EAST);
                  for (size_t fr=0; fr < FRAMES_PER_TILE; fr ++) {
                    curX += block;
                    excess += ((float)(tile_side_length) / FRAMES_PER_TILE) - block;
                    if (excess >= 1.0f) {
                      curX ++;
                      excess -= 1.0f;
                    }
                    renderNextChosenAnimationFrame(curX, curY);
                  }
                } else if (chosen->getY() < nextY) { /* SOUTH */
                  chosen->setDir(SOUTH);
                  for (size_t fr=0; fr < FRAMES_PER_TILE; fr ++) {
                    curY += block;
                    excess += ((float)(tile_side_length) / FRAMES_PER_TILE) - block;
                    if (excess >= 1.0f) {
                      curY ++;
                      excess -= 1.0f;
                    }
                    renderNextChosenAnimationFrame(curX, curY);
                  }
                } else if (chosen->getX() > nextX) { /* WEST */
                  chosen->setDir(WEST);
                  for (size_t fr=0; fr < FRAMES_PER_TILE; fr ++) {
                    curX -= block;
                    excess += ((float)(tile_side_length) / FRAMES_PER_TILE) - block;
                    if (excess >= 1.0f) {
                      curX --;
                      excess -= 1.0f;
                    }
                    renderNextChosenAnimationFrame(curX, curY);
                  }
                } else if (chosen->getY() > nextY) { /* NORTH */
                  chosen->setDir(NORTH);
                  for (size_t fr=0; fr < FRAMES_PER_TILE; fr ++) {
                    curY -= block;
                    excess += ((float)(tile_side_length) / FRAMES_PER_TILE) - block;
                    if (excess >= 1.0f) {
                      curY --;
                      excess -= 1.0f;
                    }
                    renderNextChosenAnimationFrame(curX, curY);
                  }
                } else /* same tile!! :O :O :O */ {

                }
                /* THE FOLLOWING IS VITAL, DO NOT COMMENT OUT
                 * OR DELETE IT!!!
                 * It allows the animation to be updated 
                 * square-by-square with the proper DIRECTION. */
                chosen->setX(nextTile.getX());
                chosen->setY(nextTile.getY());
              }
            }
            unit_board[y_ind][x_ind] = chosen;
            chosen->setX(x_ind);
            chosen->setY(y_ind);
            make_stop_menu();
            setGameState(EXPECT_MENU_CHOICE);
          }/* else { // clear the layer2 image? But why? Commented.
            SDL_SetRenderTarget(displayTMB, layer2);
            SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
            SDL_RenderFillRect(displayTMB, nullptr);
            SDL_SetRenderTarget(displayTMB, nullptr);
          }*/
        }
        return 0;
      } else {
        return -1;
      }
      break;
    }
    case EXPECT_MENU_CHOICE: /* ** Menu click expected ** */
    {
      if ((x_click > terrain_map_width) &&
          (y_click < terrain_map_height)) {
        unsigned int menu_choice = 2*y_click / MENU_OPTION_HEIGHT;
        unsigned int ind, upper = menu.size()-1;
        std::stringstream loc_des;
        if (menu_choice >= upper) {
          /* "Wait" option selected */
          /*                std::cout<<"\twhich now means \"Wait\""<<std::endl;*/
          /* PERFORM THE FOLLOWING AT THE END OF EACH Unit's turn */
          end_unit_turn(upper+1);
          menu.clear();
        } else {
          std::string option = menu.at(menu_choice);
          if (option == "Attack") {
            /* Change game state to 4 and show all units within range */
            menu.clear();
            /* Get / calculate max range weapon again */
            unsigned int weapon_ind = 0;
            int maxR = -1;
            int minR = tiles_x+tiles_y;
            while (chosen->getWeapons()[weapon_ind] != nullptr
                   && weapon_ind < WEAPON_CAPACITY) {
              Weapon *nextW =  chosen->getWeapons()[weapon_ind];
              if (maxR < nextW->getMaxRange()) {
                maxR = nextW->getMaxRange();
              }
              if (minR < nextW->getMinRange()) {
                minR = nextW->getMinRange();
              }
              weapon_ind ++;
            }
            
            for (ind=0; ind < in_range.size(); ind ++) {
              std::string nextUName = in_range.at(ind)->toString();
              menu.push_back(nextUName);
            }
            menu.push_back(cons_menu_options[8]/*->at(8)*/);
            /*                  find_enemies(maxR, x_ind, y_ind);*/
            /*                  show_weapon_range_efficient(maxR, minR, x_ind, y_ind);*/
            setGameState(EXPECT_ENEMY_CHOICE);
          } else if (option == "Item") {
            menu.clear();
            /*                printf("THIS CHOSEN UNIT HAS %u ITEMS\n", chosen->getItems()->size());*/
            std::cout << "SIZE OF this Unit's inventory: " << chosen->getInventory().size()
            << std::endl;
            for (ind = 0, upper = chosen->getInventory().size()-1; ind < upper; ind++) {
              std::string nextIName = chosen->getInventory().at(ind)->toString();
              menu.push_back(nextIName);
            }
            
            menu.push_back(cons_menu_options[8]/*->at(8)*/);
            
            setGameState(EXPECT_ITEM_USE);
          } else if (option == "Trade") {
            int cur_x = chosen->getX();
            int cur_y = chosen->getY();
            menu.clear();
            in_range.clear();
            if ((cur_x > 0)
                && (unit_board[cur_y][cur_x-1] != nullptr)
                && (unit_board[cur_y][cur_x-1]->getTeam() == 0)) {
              in_range.push_back(unit_board[cur_y][cur_x-1]);
              menu.push_back(unit_board[cur_y][cur_x-1]->toString());
            }
            if ((cur_y > 0)
                && (unit_board[cur_y-1][cur_x] != nullptr)
                && (unit_board[cur_y-1][cur_x]->getTeam() == 0)) {
              in_range.push_back(unit_board[cur_y-1][cur_x]);
              menu.push_back(unit_board[cur_y-1][cur_x]->toString());
            }
            if (((cur_x+1) < tiles_x)
                && (unit_board[cur_y][cur_x+1] != nullptr)
                && (unit_board[cur_y][cur_x+1]->getTeam() == 0)) {
              in_range.push_back(unit_board[cur_y][cur_x+1]);
              menu.push_back(unit_board[cur_y][cur_x+1]->toString());
            }
            if (((cur_y+1) < tiles_y)
                && (unit_board[cur_y+1][cur_x] != nullptr)
                && (unit_board[cur_y+1][cur_x]->getTeam() == 0)) {
              in_range.push_back(unit_board[cur_y+1][cur_x]);
              menu.push_back(unit_board[cur_y+1][cur_x]->toString());
            }
            menu.push_back(cons_menu_options[8]/*->at(8)*/);
            setGameState(EXPECT_TRADE_PARTNER);
          } else if (option == "Rescue") {
            int cur_x = chosen->getX();
            int cur_y = chosen->getY();
            menu.clear();
            if ((cur_x > 0)
                && (unit_board[cur_y][cur_x-1] != nullptr)
                && (unit_board[cur_y][cur_x-1]->getTeam() == 0)
                && (unit_board[cur_y][cur_x-1]->getHeld() == nullptr)) {
              loc_des << "(" << (cur_x-1) << ", " << cur_y << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if ((cur_y > 0)
                && (unit_board[cur_y-1][cur_x] != nullptr)
                && (unit_board[cur_y-1][cur_x]->getTeam() == 0)
                && (unit_board[cur_y-1][cur_x]->getHeld() == nullptr)) {
              loc_des << "(" << cur_x << ", " << (cur_y-1) << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if (((cur_x+1) < tiles_x)
                && (unit_board[cur_y][cur_x+1] != nullptr)
                && (unit_board[cur_y][cur_x+1]->getTeam() == 0)
                && (unit_board[cur_y][cur_x+1]->getHeld() == nullptr)) {
              loc_des << "(" << (cur_x+1) << ", " << cur_y << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if (((cur_y+1) < tiles_y)
                && (unit_board[cur_y+1][cur_x] != nullptr)
                && (unit_board[cur_y+1][cur_x]->getTeam() == 0)
                && (unit_board[cur_y+1][cur_x]->getHeld() == nullptr)) {
              loc_des << "(" << cur_x << ", " << (cur_y+1) << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            menu.push_back(cons_menu_options[8]);
            setGameState(EXPECT_RESCUED_UNIT);
          } else if (option == "Drop") {
            /* Look for a valid square
             * on which to set the held unit. */
            int cur_x = chosen->getX();
            int cur_y = chosen->getY();
            menu.clear();
            if ((cur_x > 0)
                && (unit_board[cur_y][cur_x-1] == nullptr)
                && (terrain_board[cur_y][cur_x-1] != 0)) {
              loc_des << "(" << (cur_x-1) << ", " << cur_y << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if ((cur_y > 0)
                && (unit_board[cur_y-1][cur_x] == nullptr)
                && (terrain_board[cur_y-1][cur_x] != 0)) {
              loc_des << "(" << cur_x << ", " << (cur_y-1) << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if (((cur_x+1) < tiles_x)
                && (unit_board[cur_y][cur_x+1] == nullptr)
                && (terrain_board[cur_y][cur_x+1] != 0)) {
              loc_des << "(" << (cur_x+1) << ", " << cur_y << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if (((cur_y+1) < tiles_y)
                && (unit_board[cur_y+1][cur_x] == nullptr)
                && (terrain_board[cur_y+1][cur_x] != 0)) {
              loc_des << "(" << cur_x << ", " << (cur_y+1) << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            menu.push_back(cons_menu_options[8]/*->at(8)*/);
            setGameState(EXPECT_DROP_LOC);
          } else if (option == "Pass") {
            int cur_x = chosen->getX();
            int cur_y = chosen->getY();
            menu.clear();
            if ((cur_x > 0)
                && (unit_board[cur_y][cur_x-1] != nullptr)
                && (unit_board[cur_y][cur_x-1]->getTeam() == 0)
                && (unit_board[cur_y][cur_x-1]->getHeld() == nullptr)) {
              loc_des << "(" << (cur_x-1) << ", " << cur_y << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if ((cur_y > 0)
                && (unit_board[cur_y-1][cur_x] != nullptr)
                && (unit_board[cur_y-1][cur_x]->getTeam() == 0)
                && (unit_board[cur_y-1][cur_x]->getHeld() == nullptr)) {
              loc_des << "(" << cur_x << ", " << (cur_y-1) << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if (((cur_x+1) < tiles_x)
                && (unit_board[cur_y][cur_x+1] != nullptr)
                && (unit_board[cur_y][cur_x+1]->getTeam() != 0)
                && (unit_board[cur_y][cur_x+1]->getHeld() == nullptr)) {
              loc_des << "(" << (cur_x+1) << ", " << cur_y << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            if (((cur_y+1) < tiles_y)
                && (unit_board[cur_y+1][cur_x] != nullptr)
                && (unit_board[cur_y+1][cur_x]->getTeam() == 0)
                && (unit_board[cur_y+1][cur_x]->getHeld() == nullptr)) {
              loc_des << "(" << cur_x << ", " << (cur_y+1) << ")";
              menu.push_back(loc_des.str());
              loc_des.str("");
            }
            menu.push_back(cons_menu_options[8]/*->at(8)*/);
            setGameState(EXPECT_PASS);
          } else if (option == "Ability") {
            menu.clear();
            /* Get / calculate max range weapon again */
            unsigned int weapon_ind = 0;
            int maxR = -1;
            int minR = tiles_x+tiles_y;
            while (chosen->getWeapons()[weapon_ind] != nullptr
                   && weapon_ind < WEAPON_CAPACITY) {
              Weapon *nextW =  chosen->getWeapons()[weapon_ind];
              if (maxR < nextW->getMaxRange()) {
                maxR = nextW->getMaxRange();
              }
              if (minR < nextW->getMinRange()) {
                minR = nextW->getMinRange();
              }
              weapon_ind ++;
            }
            
            for (ind=0; ind < in_range.size(); ind ++) {
              std::string nextUName = in_range.at(ind)->toString();
              menu.push_back(nextUName);
            }
            menu.push_back(cons_menu_options[8]/*->at(8)*/);
            /*                find_enemies(maxR, x_ind, y_ind);*/
            /*                show_weapon_range_efficient(maxR, minR, x_ind, y_ind);*/
            setGameState(EXPECT_SPEC_TARGET);
          }/* else {
            
            }*/
        }
        return 0;
      } else {
        /*            return -1;*/
      }
      break;
    }
    case EXPECT_ENEMY_CHOICE: /* ** Enemy selection expected ** */
    {
      /* First, get enemy pointer */
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        /*            printf("Enemy choice ID'd as %d\n", menu_choice);*/
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        } else {
          target = in_range.at(menu_choice);
        }
      } else if ((x_click < terrain_map_width)
                 && (y_click < terrain_map_height)) {
        /* Process unit board click*/
        if (unit_board[y_ind][x_ind] &&
            unit_board[y_ind][x_ind]->getTeam()) {
          bool found = false;
          target = unit_board[y_ind][x_ind];
          for (unsigned int ind = 0; ind < in_range.size(); ind ++) {
            if (in_range.at(ind) == target) {
              found = true;
            }
          }
          if (! found) {
            break;
          }
        } else {
          break;
        }
      }
      unsigned int ind, upper=menu.size()-1;
      menu.clear();
      
      /* Then show weapon list */
      unsigned int weapon_ind = 0;
      int dist = Entity::get_distance(chosen->getX(), chosen->getY(),
                                      target->getX(), target->getY());
      while (chosen->getWeapons()[weapon_ind] != nullptr
             && weapon_ind < WEAPON_CAPACITY) {
        Weapon *nextW = chosen->getWeapons()[weapon_ind];
        if ((dist <= nextW->getMaxRange()) && (dist >= nextW->getMinRange())) {
          std::string next_weap_desc = nextW->toString();
          menu.push_back(next_weap_desc);
        }
        //            printf("weapon ind = %d, range: %d-%d\n", weapon_ind, nextW->getMinRange, nextW->getMaxRange());
        weapon_ind ++;
      }
      menu.push_back(cons_menu_options[7]/*->at(7)*/);
      setGameState(EXPECT_WEAPON_CHOICE);
      break;
    }
    case EXPECT_WEAPON_CHOICE: /* ** Weapon selection expected ** */
    {
      int attack_result;
      /* First, get weapon pointer */
      Weapon *use = nullptr;
      unsigned int menu_choice = 2*y_click / MENU_OPTION_HEIGHT;
      /*          printf("Menu choice identified as %u\n", menu_choice);*/
      /*          printf("Menu size in total identified as %lu\n", menu.size());*/
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        if (menu_choice >= (menu.size() - 1)) {
          /* Reserve the last option for being "Back" */
          unsigned int weapon_ind = 0;
          int maxR = -1;
          int minR = tiles_x+tiles_y;
          while (chosen->getWeapons()[weapon_ind] != nullptr
                 && weapon_ind < WEAPON_CAPACITY) {
            Weapon *nextW = chosen->getWeapons()[weapon_ind];
            if (maxR < nextW->getMaxRange()) {
              maxR = nextW->getMaxRange();
            }
            if (minR > nextW->getMinRange()) {
              minR = nextW->getMinRange();
            }
            weapon_ind ++;
          }
          
          unsigned int ind;
          menu.clear();
          for (ind=0; ind < in_range.size(); ind ++) {
            std::string nextUName = in_range.at(ind)->toString();
            menu.push_back(nextUName);
          }
          menu.push_back(cons_menu_options[8]/*->at(8)*/);
          
          setGameState(EXPECT_ENEMY_CHOICE);
          break;
        } else {
          std::string option = menu.at(menu_choice);
          //                if (option == nullptr) {
          //                  std::cerr << "[!!] ERR - menu option data is nullptr!" << std::endl;
          //                } else {
          /*                  std::cout << "[DEBUG] `option` is " << *option << std::endl;*/
          /*                  std::cout << "[DEBUG] `meneu_choice` is " << menu_choice << std::endl;*/
          /*                  std::cout << "[DEBUG] menu option name's string length is " << option->length() << std::endl;*/
          //                }
          
          unsigned int weapon_ind = 0;
          Weapon *nextW = nullptr;
          while ((nextW = chosen->getWeapons()[weapon_ind]) != nullptr
                 && weapon_ind < WEAPON_CAPACITY) {
            nextW = chosen->getWeapons()[weapon_ind];
            std::string weapon_name = nextW->toString();
            //                  if (weapon_name == nullptr) {
            ///*                    std::cerr << "ERR - weapon name is nullptr!" << std::endl;*/
            //                  } else {
            ///*                    std::cout << "[DEBUG] `weapon_name` is " << *weapon_name << std::endl;*/
            ///*                    std::cout << "[DEBUG] weapon str length is " << weapon_name->length() << std::endl;*/
            //                  }
            if (option == weapon_name) {
              use = nextW;
            }
            weapon_ind ++;
          }
          
          /* Next, process the attack */
          attack_result = TacticalMapBattle::attack(chosen, target, use,
              terrain_board[target->getY()][target->getX()], false);
          if (attack_result) {
            std::cerr << "[ERR] Attack returned error " << attack_result << std::endl;
          }/* else {
            std::cout << "[OK] Attack processed correctly." << std::endl;
          }*/
          if (! target->isUp()) {
            printf("ENEMY UNIT debilitated; ");
            unit_board[target->getY()][target->getX()] = nullptr;
            delete target;
            unsigned int search_ind = 0;
            unsigned int length = enemy_units.size();
            while (search_ind < enemy_units.size()) {
              if (enemy_units.at(search_ind) == target) {
                /* */
                /*                    printf("Enemy unit at (%d, %d) apparently matches... \n", test->getX(), test->getY());*/
                /*                    printf("\tbecause %p equals %p ??? But (%d,  %d) ?= (%d, %d)\n", test, target, test->getX(), test->getY(), target->getX(), target->getY());*/
                enemy_units.erase(enemy_units.begin()+search_ind);
                /*                    delete target/ *enemy_units.at(search_ind)* /;*/
                /*                    printf("test ptr (expected nullptr), got %p\n", test);*/
                search_ind = length;
              }
              search_ind ++;
            }
            printf("enemy unit vector size: %u -> %lu\n", length, enemy_units.size());
            target = nullptr;
          }
          
          unsigned int ind, upper=menu.size()-1;
          menu.clear();
          /* Then end the Unit turn */
          end_unit_turn(0);
        }
      }
      
      break;
    }
//    case 6: /* **  ** */
//    {
//      
//      break;
//    }
    case EXPECT_RESCUED_UNIT: /* expected a choice of WHOM to Rescue */
    {
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        int x, y;
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        /*            printf("Enemy choice ID'd as %d\n", menu_choice);*/
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        } else {
          sscanf(menu.at(menu_choice).c_str(), "(%d, %d)", &x, &y);
          chosen->setHeld(unit_board[y][x]);
          set_moved(unit_board[y][x]);
          /*              if () {*/
          unit_board[y][x]->setX(chosen->getX());
          unit_board[y][x]->setY(chosen->getY());
          chosen->setHeld(unit_board[y][x]);
          set_moved(unit_board[y][x]);
          unit_board[y][x] = nullptr;
          end_unit_turn(0);
          /*              }*/
        }
      } else if ((x_click < terrain_map_width) &&
                 (y_click < terrain_map_height)) {
        /* Process unit board click*/
        if (unit_board[y_ind][x_ind]/* && unit_board[y_ind][x_ind]->getTeam() == 0*/) {
          unit_board[y_ind][x_ind]->setX(chosen->getX());
          unit_board[y_ind][x_ind]->setY(chosen->getY());
          chosen->setHeld(unit_board[y_ind][x_ind]);
          set_moved(unit_board[y_ind][x_ind]);
          unit_board[y_ind][x_ind] = nullptr;
          end_unit_turn(0);
        }
      }
      break;
    }
    case EXPECT_DROP_LOC: /* expected a choice of WHERE TO DROP */
    {
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        int x, y;
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        } else {
          sscanf(menu.at(menu_choice).c_str(), "(%d, %d)", &x, &y);
          unit_board[y][x] = chosen->getHeld();
          unit_board[y][x]->setX(x);
          unit_board[y][x]->setY(y);
          chosen->setHeld(nullptr);
          set_moved(unit_board[y][x]);
        }
      } else if ((x_click < terrain_map_width) &&
                 (y_click < terrain_map_height)) {
        /* Process unit board click */
        if (unit_board[y_ind][x_ind] == nullptr
            && terrain_board[y_ind][x_ind] != 0) {
          unit_board[y_ind][x_ind] = chosen->getHeld();
          unit_board[y_ind][x_ind]->setX(x_ind);
          unit_board[y_ind][x_ind]->setY(y_ind);
          chosen->setHeld(nullptr);
          set_moved(unit_board[y_ind][x_ind]);
        }
      }
      
      /* Then end the Unit turn */
      end_unit_turn(0);
      break;
    }
    case EXPECT_PASS: /* expected a choice of WHOM TO PASS TO */
    {
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        int x, y;
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        /*            printf("Enemy choice ID'd as %d\n", menu_choice);*/
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        } else {
          sscanf(menu.at(menu_choice).c_str(), "(%d, %d)", &x, &y);
          Unit *being_passed = chosen->getHeld();
          set_moved(being_passed);
          unit_board[y][x]->setHeld(being_passed);
          chosen->setHeld(nullptr);
          end_unit_turn(0);
        }
      } else if ((x_click < terrain_map_width) &&
                 (y_click < terrain_map_height)) {
        /* Process unit board click*/
        if (unit_board[y_ind][x_ind]/*
                                     && unit_board[y_ind][x_ind]->getTeam() == 0*/) {
                                       Unit *being_passed = chosen->getHeld();
                                       set_moved(being_passed);
                                       unit_board[y_ind][x_ind]->setHeld(being_passed);
                                       chosen->setHeld(nullptr);
                                       end_unit_turn(0);
                                     }
      }
      break;
    }
    case EXPECT_TRADE_PARTNER:
    {
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        } else {
          target = in_range.at(menu_choice);
        }
      } else if ((x_click < terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process unit board click*/
        if (unit_board[y_ind][x_ind] && (unit_board[y_ind][x_ind]->getTeam() == 0)) {
          target = unit_board[y_ind][x_ind];
        }
      }
      unsigned int ind;
      menu.clear();
      
      /*        menu.push_back(chosen->getName());*/
      menu.push_back(chosen->toString());
      /*        menu.push_back(new std::string("Chosen Unit :"));*/
      for (size_t iter = 0; iter < chosen->getInventory().size(); iter++)
        menu.push_back(chosen->getInventory().at(iter)->toString());
      
      /*        menu.push_back(target->getName());*/
      menu.push_back(target->toString());
      /*          menu.push_back(new std::string("Other Friendly Unit :"));*/
      for (size_t iter = 0; iter < target->getInventory().size(); iter++) {
        menu.push_back(target->getInventory().at(iter)->toString());
      }
      menu.push_back(cons_menu_options[7]/*->at(7)*/);
      
      setGameState(EXPECT_TRADE_ACT);
      break;
    }
    case EXPECT_TRADE_ACT: /* expected a click of rearranging items. */
    {
      /* This one is a doozy, get ready */
      /*        unsigned int iter = 0;*/
      /*          MAX_ITEM_LIMIT;*/
      /* maybe change the latter condition to TMB_SCREEN_HEIGHT if # of items grows large */
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        unsigned int ind;
        Item *to_be_moved = nullptr;
        std::string swap_str;
        if (menu_choice == 0 || menu_choice == chosen->getInventory().size() + 1) {
          break;
        } else if (menu_choice-1 < chosen->getInventory().size()) {
          std::cout << "Chose an item from first half " << std::endl;
          to_be_moved = chosen->getInventory().at(menu_choice-1);
          if ((target->getInventory().size() < MAX_ITEM_LIMIT)
              && target->addItem(to_be_moved)) {
            chosen->deleteItem(menu_choice - 1);
            swap_str = menu.at(menu_choice);
          }
          ind = menu_choice;
          while (ind <= chosen->getInventory().size()+1) {
            menu.at(ind) = menu.at(ind+1);
            std::cout << "Copying from index " << (ind+1) << " to index " << ind << std::endl;
            ind ++;
          }
          menu.at(ind) = swap_str;
        } else if ((menu_choice-2) < (chosen->getInventory().size() +
                                      target->getInventory().size())) {
          to_be_moved = target->getInventory().at(menu_choice-2-chosen->getInventory().size());
          if ((target->getInventory().size() < MAX_ITEM_LIMIT)
              && chosen->addItem(to_be_moved)) {
            target->deleteItem(menu_choice - 2 - chosen->getInventory().size());
            swap_str = menu.at(menu_choice);
          }
          ind = menu_choice;
          while (ind > chosen->getInventory().size()) {
            menu.at(ind) = menu.at(ind-1);
            ind --;
          }
          menu.at(ind) = swap_str;
        } else {
          /* Go back to state 1 */
          end_unit_turn(0);
          break;
        }
        /*          menu.clear();*/
        /*          menu.push_back(chosen->getName());*/
        /*          menu.push_back(chosen->toString());*/
        /*            menu.push_back(new std::string("Chosen Unit :"));*/
        /*          while (iter < chosen->getItems()->size()) {
         menu.push_back(chosen->getItems()->at(iter)->toString());
         iter ++;
         }*/
        /*          menu.push_back(target->getName());*/
        /*          menu.push_back(target->toString());*/
        /*            menu.push_back(new std::string("Other Friendly Unit :"));*/
        /*          for (iter = 0; iter < target->getItems()->size(); iter ++) {
         menu.push_back(target->getItems()->at(iter)->toString());
         }
         menu.push_back(cons_menu_options[7]/ *->at(7)* /);*/
      }
      break;
    }
    case EXPECT_ITEM_USE:
    {
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        }
        StatChange *info = chosen->getInventory().at(menu_choice)->apply();
        chosen->alter(nullptr, info);
        chosen->deleteItem(menu_choice);
      }
      end_unit_turn(0);
      break;
    }
    case EXPECT_SPEC_TARGET:
    { /* Special ability TARGET unit expected */
      if ((x_click > terrain_map_width) && (y_click < terrain_map_height)) {
        /* Process menu click */
        unsigned int menu_choice = (2*y_click / MENU_OPTION_HEIGHT);
        /*            printf("Enemy choice ID'd as %d\n", menu_choice);*/
        if (menu_choice >= (menu.size() - 1)) {
          make_stop_menu();
          break;
        } else {
          target = in_range.at(menu_choice);
        }
      } else if ((x_click < terrain_map_width)
                 && (y_click < terrain_map_height)) {
        /* Process unit board click*/
        if (unit_board[y_ind][x_ind] && unit_board[y_ind][x_ind]->getTeam()) {
          target = unit_board[y_ind][x_ind];
        }
      }
      unsigned int ind, upper=menu.size();
      menu.clear();
      
      /* Then show weapon list */
      size_t weapon_ind = 0;
      int dist = Entity::get_distance(chosen->getX(), chosen->getY(),
                                      target->getX(), target->getY());
      while (chosen->getWeapons()[weapon_ind] != nullptr
             && weapon_ind < WEAPON_CAPACITY) {
        Weapon *nextW = chosen->getWeapons()[weapon_ind];
        if (nextW->getMinRange() <= dist && dist <= nextW->getMaxRange()) {
          std::string next_weap_desc = nextW->toString();
          menu.push_back(next_weap_desc);
        }
        //printf("weapon ind = %d, range: %d-%d\n", weapon_ind, nextW->getMinRange, nextW->getMaxRange());
        weapon_ind ++;
      }
      menu.push_back(cons_menu_options[7]/*->at(7)*/);
      setGameState(EXPECT_SPEC_WEAP);
      break;
    }
    case EXPECT_SPEC_WEAP:
    {
      int attack_result;
      /* First, get weapon pointer */
      Weapon *use = nullptr;
      unsigned int menu_choice = 2*y_click / MENU_OPTION_HEIGHT;
      /*                      std::cout<<"Menu choice identified as "<<menu_choice<<std::endl;*/
      /*                      std::cout<<"Menu size in total identified as "<<menu.size()<<std::endl;*/
      if (menu_choice >= (menu.size() - 1)) {
        /* Reserve the last option for being "Back" */
        unsigned int weapon_ind = 0;
        int maxR = -1;
        int minR = tiles_x+tiles_y;
        while (chosen->getWeapons()[weapon_ind] != nullptr
               && weapon_ind < WEAPON_CAPACITY) {
          Weapon *nextW = chosen->getWeapons()[weapon_ind];
          if (maxR < nextW->getMaxRange()) {
            maxR = nextW->getMaxRange();
          }
          if (minR > nextW->getMinRange()) {
            minR = nextW->getMinRange();
          }
          weapon_ind ++;
        }
        
        unsigned int ind;
        for (ind=0; ind < in_range.size(); ind ++) {
          std::string nextUName = in_range.at(ind)->toString();
          menu.push_back(nextUName);
        }
        menu.push_back(cons_menu_options[8]/*->at(8)*/);
        setGameState(EXPECT_ENEMY_CHOICE);
        
        menu.clear();
        break;
      } else {
        std::string option = menu.at(menu_choice);
        unsigned int ind = 0;
        unsigned int upper=menu.size()-1;
        unsigned int weapon_ind = 0;
        while (chosen->getWeapons()[weapon_ind] != nullptr
               && weapon_ind < WEAPON_CAPACITY) {
          Weapon *nextW = chosen->getWeapons()[weapon_ind];
          std::string weapon_name = nextW->toString();
          if (option == weapon_name) {
            use = nextW;
          }
          weapon_ind ++;
        }
        
        /* Next, process the attack */
        attack_result = TacticalMapBattle::attack(chosen, target, use,
            terrain_board[target->getY()][target->getX()], true);
        if (attack_result) {
          std::cerr << "[ERR] Attack returned error " << attack_result << std::endl;
        }/* else {
          std::cout << "[OK] Attack processed correctly." << std::endl;
        }*/
        
        if (! chosen->isUp()) {
          printf("PLAYER UNIT debilitated\n");
          unit_board[chosen->getY()][chosen->getX()] = nullptr;
          /*              delete chosen;*/
          unsigned int search_ind = 0;
          unsigned int length = player_units.size();
          while (search_ind < player_units.size()) {
            if (player_units.at(search_ind) == chosen) {
              /* */
              /*                  Unit *test = nullptr;*/
              debilitated_players.push_back(player_units.at(search_ind));
              player_units.erase(player_units.begin()+search_ind);
              /*                  test = player_units.at(search_ind);
               printf("test ptr (expected nullptr), got %p\n", test);*/
              
              search_ind = length;
            }
            search_ind ++;
          }
          /*              printf("Player unit vector size went from %u to %u\n", length, player_units.size());*/
          chosen = nullptr;
        }
        if (! target->isUp()) {
          printf("ENEMY UNIT debilitated\n");
          unit_board[target->getY()][target->getX()] = nullptr;
          /*                              delete target;*/
          size_t search_ind = 0;
          size_t length = enemy_units.size();
          while (search_ind < enemy_units.size()) {
            if (enemy_units.at(search_ind) == target) {
              /* */
              /*                                      printf("Enemy unit at (%d, %d) apparently matches... \n", test->getX(), test->getY());*/
              /*                                      printf("\tbecause %p equals %p ??? But (%d,  %d) ?= (%d, %d)\n", test, target, test->getX(), test->getY(), target->getX(), target->getY());*/
              enemy_units.erase(enemy_units.begin()+search_ind);
              /*                  printf("test ptr (expected nullptr), got %p\n", test);*/
              
              search_ind = length;
            }
            search_ind ++;
          }
          printf("Enemy unit vector size went from %lu to %zd\n", length, enemy_units.size());
          target = nullptr;
        }
        
        /* Then end the Unit turn */
        end_unit_turn(0);
      }
      break;
    }
    default:
    {
      
      break;
    }
  }
  return 0;
}

/*
 * ---OUTLINE of setting the contents of the terrain map---
 *  al_create_bitmap of the proper width and height;
 *  al_set_target_bitmap to the newly created map;
 *  go through the same for-loops as below, but use
 *    draw_scaled_bitmap(); FROM the terrain_tiles array TO the target map;
 *  Then reset target_bitmap so that it is the backbuffer of the displayTMB.
 */
/* Preconditions: Now that this has been re-hauled into an Object,
 *   the preconditions are simply that this object has
 *   been fully constructed. */
int TacticalMapBattle::display_terrain_map(int print_verbose) {
  SDL_Surface *pumpkin_surf = IMG_Load_RW(SDL_RWFromFile("res/terrain/halloween/jank-o-lantern.png", "rb"), 1);
  SDL_Texture *pumpkin = SDL_CreateTextureFromSurface(displayTMB, pumpkin_surf);
  if (pumpkin == nullptr) {
    std::cerr<<"ERROR ERROR ERROR PUMPKIN IS nullptr"<<std::endl;
    return (-1);
  }
  int ind_row, ind_col;
  if (print_verbose) {
    for (ind_row=0; ind_row < tiles_y; ind_row ++) {
      /*      int y_interval = (tile_side_length)*(ind_row+1);
       SDL_SetRenderDrawColor(displayTMB, 1, 1, 1), 3, 255);
       SDL_RenderDrawLine(displayTMB, 1, y_interval, tiles_y, y_interval, ;*/
      for (ind_col=0; ind_col < tiles_x; ind_col ++) {
        /*        int x_int = (tile_side_length)*(ind_col+1);
         SDL_SetRenderDrawColor(displayTMB, 1, 1, 1), 3, 255);
         SDL_RenderDrawLine(displayTMB, x_int, 1, x_int, tiles_y, ;*/
        int saved_terrain_type = terrain_board[ind_row][ind_col];
        /*        printf("\t(%d, %d): %d : ", ind_col, ind_row, saved_terrain_type);*/
        int obt_wid=0, obt_height=0;
        SDL_QueryTexture(terrain_tiles[saved_terrain_type], nullptr, nullptr, &obt_wid, &obt_height);
        
        if (ind_row || ind_col) {
          const SDL_Rect dt = {ind_col*tile_side_length, ind_row*tile_side_length, tile_side_length, tile_side_length};
          SDL_RenderCopy(displayTMB, terrain_tiles[saved_terrain_type], nullptr, &dt);
          
          /* Halloween highlight */
          /* randomly spawn jack-o-lanterns! */
          /*              int chance = (generator() % (tiles_x*tiles_y));
           if ((saved_terrain_type != 0) && 
           (unit_board[ind_row][ind_col] == nullptr) && (chance < 8)) {
           float pkin_wid = al_get_bitmap_width(pumpkin);
           float pkin_height = al_get_bitmap_height(pumpkin);*/
          /*                al_draw_scaled_bitmap(pumpkin, 1, 1,
           pkin_wid, pkin_height, 1, 1, tile_side_length, tile_side_length, 0);*/
          /*                al_draw_scaled_bitmap(pumpkin, 1, 1, pkin_wid,
           pkin_height, ind_col*tile_side_length,
           ind_row*tile_side_length, tile_side_length-6, tile_side_length-6, 0);
           }*/
        } else {
          const SDL_Rect dt = {1, 1, tile_side_length, tile_side_length};
          SDL_RenderCopy(displayTMB, terrain_tiles[saved_terrain_type], nullptr, &dt);
          /* Halloween highlight */
          /* randomly spawn jack-o-lanterns! */
          /*              int chance = generator() % (tiles_x*tiles_y);
           if ((saved_terrain_type != 0) && 
           (unit_board[ind_row][ind_col] == nullptr) && (chance < 8)) {
           float pkin_wid = al_get_bitmap_width(pumpkin);
           float pkin_height = al_get_bitmap_height(pumpkin);
           const SDL_Rect dt = {1, 1, tile_side_length, tile_side_length};
           SDL_RenderCopy(displayTMB, pumpkin, nullptr, &dt);
           }*/
        }
        
        printf("\t(%d)", saved_terrain_type);
        
        if (saved_terrain_type == 0) {
          printf("  Ocean   ");
        } else if (saved_terrain_type == 1) {
          printf("  Plains  ");
        } else if (saved_terrain_type == 2) {
          printf("  Forest  ");
        } else if (saved_terrain_type == 3) {
          printf("  Hills   ");
        } else if (saved_terrain_type == 4) {
          printf("  Ice     ");
        } else if (saved_terrain_type == 5) {
          printf("  Sand    ");
        } else if (saved_terrain_type == 6) {
          printf("   Mountains  ");
          
          /*
           } else if (saved_terrain_type == 7) {
           printf("");
           } else if (saved_terrain_type == 8) {
           printf("");
           } else if (saved_terrain_type == 9) {
           printf("");
           */
          
        } else {
          printf(" UNIDENTIFIED ");
        }
        if (ind_col-tiles_x+1) {
          printf("|");
        }
      }
      printf("\n");
    }
  } else {
    for (ind_row=0; ind_row < tiles_y; ind_row ++) {
      for (ind_col=0; ind_col < tiles_x; ind_col ++) {
        int saved_terrain_type = terrain_board[ind_row][ind_col];
        int obt_wid=0, obt_height=0;
        SDL_QueryTexture(terrain_tiles[saved_terrain_type], nullptr,
                         nullptr, &obt_wid, &obt_height);
        /*
         const SDL_Rect dt = {(ind_col*tile_side_length+1), (ind_row*tile_side_length+1), tile_side_length, tile_side_length};
         SDL_RenderCopy(displayTMB, terrain_tiles[saved_terrain_type], nullptr, &dt);
         */
        if (ind_row || ind_col) {
          const SDL_Rect dt = {ind_col*tile_side_length, ind_row*tile_side_length, tile_side_length, tile_side_length};
          SDL_RenderCopy(displayTMB, terrain_tiles[saved_terrain_type], nullptr, &dt);
          
          /* Halloween highlight */
          /* randomly spawn jack-o-lanterns! */
          /*              int chance = generator() % (tiles_x*tiles_y);*/
          /*              printf("chance integer is %d (comparing it to 8)\n", chance);*/
          /*              if ((saved_terrain_type != 0) && 
           (unit_board[ind_row][ind_col] == nullptr) && (chance < 8)) {*/
          /*                printf("\tHEH, Drawing the jack-o-lantern.\n");*/
          /*                float pkin_wid = al_get_bitmap_width(pumpkin);
           float pkin_height = al_get_bitmap_height(pumpkin);*/
          /*                al_draw_scaled_bitmap(pumpkin, 1, 1,
           pkin_wid, pkin_height, 1, 1, tile_side_length, tile_side_length, 0);*/
          /*                al_draw_scaled_bitmap(pumpkin, 1, 1, pkin_wid,
           pkin_height, ind_col*tile_side_length,
           ind_row*tile_side_length, tile_side_length-6, tile_side_length-6, 0);
           }*/
        } else {
          const SDL_Rect dt = {1, 1, tile_side_length, tile_side_length};
          SDL_RenderCopy(displayTMB, terrain_tiles[saved_terrain_type], nullptr, &dt);
          
          /* Halloween highlight */
          /* randomly spawn jack-o-lanterns! */
          /*              int chance = generator() % (tiles_x*tiles_y);*/
          /*              printf("chance integer is %d (comparing it to 8)\n", chance);*/
          /*              if ((saved_terrain_type != 0) && 
           (unit_board[ind_row][ind_col] == nullptr) && (chance < 8)) {*/
          /*                printf("\tHEH, Drawing the jack-o-lantern.\n");*/
          /*                float pkin_wid = al_get_bitmap_width(pumpkin);
           float pkin_height = al_get_bitmap_height(pumpkin);*/
          /*                al_draw_scaled_bitmap(pumpkin, 1, 1,
           pkin_wid, pkin_height, 1, 1, tile_side_length, tile_side_length, 0);*/
          /*                al_draw_scaled_bitmap(pumpkin, 1, 1, pkin_wid,
           pkin_height, ind_col*tile_side_length,
           ind_row*tile_side_length, tile_side_length-6, tile_side_length-6, 0);
           }*/
          
        }
      }
    }
  }
  for (ind_row=1; ind_row <= tiles_y; ind_row ++) { /* Horizonstal lines */
    int y_interval = (tile_side_length*ind_row)-2;
    SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 255);
    SDL_RenderDrawLine(displayTMB, 1, y_interval, tiles_x*tile_side_length, y_interval);
  }
  for (ind_col=1; ind_col <= tiles_x; ind_col ++) { /* Vertical lines */
    int x_int = (tile_side_length*ind_col)-1;
    SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 255);
    SDL_RenderDrawLine(displayTMB, x_int, 1, x_int, tiles_y*tile_side_length);
  }
  return 0;
}

/* Default soundtrack options should be:
 *  - a "default" theme, to play as the TMB begins
 *  - a theme for "only one enemy remaining!" i.e. close to victory
 *  - a theme for "only one player unit remaining!" i.e. close to defeat
 * Other themes may be added in certain subclasses, e.g. the Seize TMB class */
bool TacticalMapBattle::checkForMusicTransition() {
  if (enemy_units.size() == 0) {
    if (musicPhase == VICTORY) {
      return false;
    } else {
      musicPhase = VICTORY;
      return true;
    }
  } else if (player_units.size() == 0) {
    if (musicPhase == DEFEAT) {
      return false;
    } else {
      musicPhase = DEFEAT;
      return true;
    }
  } else if (enemy_units.size() == 1) {
    if (musicPhase == ONE_ENEMY_LEFT) {
      return false;
    } else {
      musicPhase = ONE_ENEMY_LEFT;
      return true;
    }
  } else if (player_units.size() == 1) {
    if (musicPhase == ONE_PLAYER_LEFT) {
      return false;
    } else {
      musicPhase = ONE_PLAYER_LEFT;
      return true;
    }
  } else /*if ()*/ { // can add more custom soundtrack conditions here
    if (musicPhase == DEFAULT) {
      return false;
    } else {
      musicPhase = DEFAULT;
      return true;
    }
  }
}

bool SeizeTMB::checkForMusicTransition() {
  if (enemy_units.size() == 0) {
    if (musicPhase == VICTORY) {
      return false;
    } else {
      musicPhase = VICTORY;
      return true;
    }
  } else if (player_units.size() == 0) {
    if (musicPhase == DEFEAT) {
      return false;
    } else {
      musicPhase = DEFEAT;
      return true;
    }
  } else if (enemy_units.size() == 1) {
    if (musicPhase == ONE_ENEMY_LEFT) {
      return false;
    } else {
      musicPhase = ONE_ENEMY_LEFT;
      return true;
    }
  } else if (enemy_units.size() == 0) {
    if (musicPhase == JUST_SEIZE_IT) {
      return false;
    } else {
      musicPhase = JUST_SEIZE_IT;
      return true;
    }
  } else if (player_units.size() == 1) {
    if (musicPhase == ONE_PLAYER_LEFT) {
      return false;
    } else {
      musicPhase = ONE_PLAYER_LEFT;
      return true;
    }
  } else /*if ()*/ { // can add more custom soundtrack conditions here
    if (musicPhase == DEFAULT) {
      return false;
    } else {
      musicPhase = DEFAULT;
      return true;
    }
  }
}

void TacticalMapBattle::display_chosen_unit_frame(int t_wid, int t_hei, Unit *sel,
      int urx, int ury, bool showHP) {
  Unit *next_ele = sel;
  SDL_Texture *next_icon = next_ele->advanceAnim() /*(displayTMB, next_ele->getIcon())*/;
  int obt_wid=0, obt_height=0;
  SDL_QueryTexture(next_icon, nullptr, nullptr, &obt_wid, &obt_height);
  float scale_factor = 1.0f;
  
  if (obt_wid < obt_height) {
    scale_factor = (float)(((float)t_hei) / obt_height);
    /* Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0` */
    float new_wid = scale_factor*obt_wid;
    float dx = ((t_wid - new_wid) / 2.0);
    const SDL_Rect dt = {urx, ury, static_cast<int>(new_wid), t_hei};
    if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
      std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
      std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
    }
/*    SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
    if (showHP) {
      std::stringstream hpSGen;
      hpSGen << next_ele->getHealth() << " HP";
      std::string hpS = hpSGen.str();
      if (next_ele->getTeam() == 0) {
        FC_Draw(fc_luxirb_player, displayTMB, urx + t_wid/2/* dx*/,
                ury, hpS.c_str());
      } else {
        FC_Draw(fc_luxirb_enemy, displayTMB, urx + t_wid/2/* dx*/,
                ury, hpS.c_str());
      }
    }
  } else {
    scale_factor = (float)(((float)t_wid) / obt_wid);
    float new_height = scale_factor*((float)obt_height);
    float dy = ((t_hei - new_height) / 2.0);
    const SDL_Rect dt = {urx, ury, t_wid, static_cast<int>(new_height)};
    if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
      std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
      std::cerr<<"[ ERR ]    Could not draw the unit icon for "
        <<next_ele->toString()<<"."<<std::endl;
    }
/*    SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
    if (showHP) {
      std::stringstream hpSGen;
      hpSGen << next_ele->getHealth() << " HP";
      std::string hpS = hpSGen.str();
      if (next_ele->getTeam() == 0) {
        FC_Draw(fc_luxirb_player, displayTMB, urx, ury, hpS.c_str());
      } else {
        FC_Draw(fc_luxirb_enemy, displayTMB, urx, ury, hpS.c_str());
      }
    }
  }
}

void TacticalMapBattle::display_chosen_unit(int t_wid, int t_hei, Unit *sel,
      int urx, int ury, bool showHP) {
  Unit *next_ele = sel;
  SDL_Texture *next_icon = SDL_CreateTextureFromSurface(displayTMB, next_ele->getIcon());
  int obt_wid=0, obt_height=0;
  SDL_QueryTexture(next_icon, nullptr, nullptr, &obt_wid, &obt_height);
  float scale_factor = 1.0f;
  
  if (obt_wid < obt_height) {
    scale_factor = (float)(((float)t_hei) / obt_height);
    /* Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0` */
    float new_wid = scale_factor*obt_wid;
    float dx = ((t_wid - new_wid) / 2.0);
    const SDL_Rect dt = {urx, ury, static_cast<int>(new_wid), t_hei};
    if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
      std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
      std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
    }
/*    SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
    if (showHP) {
      std::stringstream hpSGen;
      hpSGen << next_ele->getHealth() << " HP";
      std::string hpS = hpSGen.str();
      if (next_ele->getTeam() == 0) {
        FC_Draw(fc_luxirb_player, displayTMB, urx + t_wid/2/* dx*/,
                ury, hpS.c_str());
      } else {
        FC_Draw(fc_luxirb_enemy, displayTMB, urx + t_wid/2/* dx*/,
                ury, hpS.c_str());
      }
    }
  } else {
    scale_factor = (float)(((float)t_wid) / obt_wid);
    float new_height = scale_factor*((float)obt_height);
    float dy = ((t_hei - new_height) / 2.0);
    const SDL_Rect dt = {urx, ury, t_wid, static_cast<int>(new_height)};
    if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
      std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
      std::cerr<<"[ ERR ]    Could not draw the unit icon for "
        <<next_ele->toString()<<"."<<std::endl;
    }
/*    SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
    if (showHP) {
      std::stringstream hpSGen;
      hpSGen << next_ele->getHealth() << " HP";
      std::string hpS = hpSGen.str();
      if (next_ele->getTeam() == 0) {
        FC_Draw(fc_luxirb_player, displayTMB, urx, ury, hpS.c_str());
      } else {
        FC_Draw(fc_luxirb_enemy, displayTMB, urx, ury, hpS.c_str());
      }
    }
  }
}

void TacticalMapBattle::display_one_unit(int t_wid, int t_hei, int x,
      int y, bool showHP) {
  Unit *next_ele = unit_board[y][x];
  SDL_Texture *next_icon = SDL_CreateTextureFromSurface(displayTMB, next_ele->getIcon());
  int obt_wid=0, obt_height=0;
  SDL_QueryTexture(next_icon, nullptr, nullptr, &obt_wid, &obt_height);
  float scale_factor = 1.0;
  
  if (obt_wid < obt_height) {
    scale_factor = t_hei / obt_height;
    /* Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0` */
    float new_wid = scale_factor*obt_wid;
    float dx = ((t_wid - new_wid) / 2.0);
    const SDL_Rect dt = {x + static_cast<int>(dx), y, static_cast<int>(new_wid), t_hei};
    if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
      std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
      std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
    }
/*    SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
    if (showHP) {
      std::stringstream hpSGen;
      hpSGen << next_ele->getHealth() << " HP";
      std::string hpS = hpSGen.str();
      if (next_ele->getTeam() == 0) {
        FC_Draw(fc_luxirb_player, displayTMB, (x*t_wid) + t_wid/2/* dx*/,
                y*t_hei, hpS.c_str());
      } else {
        FC_Draw(fc_luxirb_enemy, displayTMB, (x*t_wid) + t_wid/2/* dx*/,
                y*t_hei, hpS.c_str());
      }
    }
  } else {
    scale_factor = t_wid / obt_wid;
    float new_height = scale_factor*obt_height;
    float dy = ((t_hei - new_height) / 2.0);
    const SDL_Rect dt = {
        x, y + static_cast<int>(dy),
        t_wid, static_cast<int>(new_height)
    };
    if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
      std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
      std::cerr<<"[ ERR ]    Could not draw the unit icon for "
        <<next_ele->toString()<<"."<<std::endl;
    }
/*    SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
    if (showHP) {
      std::stringstream hpSGen;
      hpSGen << next_ele->getHealth() << " HP";
      std::string hpS = hpSGen.str();
      if (next_ele->getTeam() == 0) {
        FC_Draw(fc_luxirb_player, displayTMB, (x*t_wid),
                y*t_hei+dy, hpS.c_str());
      } else {
        FC_Draw(fc_luxirb_enemy, displayTMB, (x*t_wid),
                y*t_hei+dy, hpS.c_str());
      }
    }
  }
}

/* Preconditions:
 *      * tile_width has been set correctly from map data,
 *         as a valid, positive integer
 *      * tile_height has been set correctly from map data,
 *         as a valid, positive integer
 *      * FC_Fonts fc_luxirb_player and fc_luxirb_enemy are
 *         have been properly initialized
 * Postcondition:
 *      * will draw the units from unit_board onto the screen displayTMB.
 */
/* UPDATE (2017-11-09): The original approach conflicts with the Rescue/Drop
 *  system.
 *  Thus it has been replaced with a O(width*height) solution */
int TacticalMapBattle::display_units(int tile_width, int tile_height) {
  if (verbosity != 0) {
    int row, col;
    for (row = 0; row < tiles_y; row ++) {
      for (col = 0; col < tiles_x; col ++) {
        Unit *next_ele = unit_board[row][col];
        if (next_ele != nullptr) {
          SDL_Texture *next_icon = SDL_CreateTextureFromSurface(displayTMB, next_ele->getIcon());
          int obt_wid=0, obt_height=0;
          SDL_QueryTexture(next_icon, nullptr, nullptr, &obt_wid, &obt_height);
          
          float scale_factor = 1.0;
          char hp_s[7] = "xxx HP";
          sprintf(hp_s, "%d HP", next_ele->getHealth());
          if (obt_wid < obt_height) {
            scale_factor = tile_height / obt_height;
            /* Note: this currently centers the unit in the tile.
             * In the future, if we want to have the unit 
             * up against the bottom of the tile, then simply remove the `/ 2.0`
             */
            float new_wid = scale_factor*obt_wid;
            float dx = ((tile_width - new_wid) / 2.0);
            /*                if (row || col) {*/
            const SDL_Rect dt = {static_cast<int>((col*tile_width) + dx), row*tile_height, static_cast<int>(new_wid), tile_height};
            if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
              std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
              std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
            }
            /*                } else {
             const SDL_Rect dt = {1, 1, new_wid, tile_height};
             SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
             }*/
            /*                SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
            if (next_ele->getTeam() == 0) {
              FC_Draw(fc_luxirb_player, displayTMB, (col*tile_width) + tile_width/2/* dx*/,
                      row*tile_height, hp_s);
            } else {
              FC_Draw(fc_luxirb_enemy, displayTMB, (col*tile_width) + tile_width/2/* dx*/,
                      row*tile_height, hp_s);
            }
          } else {
            scale_factor = tile_width / obt_wid;
            float new_height = scale_factor*obt_height;
            float dy = ((tile_height - new_height) / 2.0);
            /*                if (row || col) {*/
            const SDL_Rect dt = {col*tile_width, static_cast<int>((row*tile_height) + dy), tile_width, static_cast<int>(new_height)};
            if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
              std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
              std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
            }
            /*                  SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
             } else {
             const SDL_Rect dt = {1, 1+dy, tile_width, new_height};
             SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
             }*/
            /*                SDL_SetRenderDrawColor(displayTMB, 1, 1, 1, 255);*/
            if (next_ele->getTeam() == 0) {
              FC_Draw(fc_luxirb_player, displayTMB, (col*tile_width),
                      row*tile_height+dy, hp_s);
            } else {
              FC_Draw(fc_luxirb_enemy, displayTMB, (col*tile_width),
                      row*tile_height+dy, hp_s);
            }
          }
        }/* else {
          printf("unit_board[%d][%d] is nullptr\n", row, col);
          }*/
      }
    }
    /*        printf("row is now %d, obviously greater than or equal to %d???\n", row, tiles_y);*/
    /*
     unsigned int ind;
     for (ind = 0; ind < player_units.size(); ind++) {
     Unit *next_ele = player_units[ind];
     int row = next_ele->getY();
     int col = next_ele->getX();
     SDL_Texture *next_icon = SDL_CreateTextureFromSurface(next_ele->getIcon());
     float obt_wid = al_get_bitmap_width(next_icon);
     float obt_height = al_get_bitmap_height(next_icon);
     float scale_factor = 1.0;
     if (obt_wid < obt_height) {
     scale_factor = tile_height / obt_height;
     / * Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0`
     * /
     float new_wid = scale_factor*obt_wid;
     float dx = ((tile_width - new_wid) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {(col*tile_width) + dx, row*tile_height, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     } else {
     scale_factor = tile_width / obt_wid;;
     float new_height = scale_factor*obt_height;
     float dy = ((tile_height - new_height) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {col*tile_width, (row*tile_height) + dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1+dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     }
     }
     for (ind = 0; ind < enemy_units.size(); ind++) {
     Unit *next_ele = (*enemy_units)[ind];
     int row = next_ele->getY();
     int col = next_ele->getX();
     SDL_Texture *next_icon = SDL_CreateTextureFromSurface(next_ele->getIcon());
     float obt_wid = al_get_bitmap_width(next_icon);
     float obt_height = al_get_bitmap_height(next_icon);
     float scale_factor = 1.0;
     if (obt_wid < obt_height) {
     scale_factor = tile_height / obt_height;
     / * Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0`
     * /
     float new_wid = scale_factor*obt_wid;
     float dx = ((tile_width - new_wid) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {(col*tile_width) + dx, row*tile_height, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     } else {
     scale_factor = tile_width / obt_wid;;
     float new_height = scale_factor*obt_height;
     float dy = ((tile_height - new_height) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {col*tile_width, (row*tile_height) + dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1+dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     }
     }
     */
    verbosity = 0;
  } else {
    int row, col;
    for (row=0; row < tiles_y; row ++) {
      for (col=0; col < tiles_x; col ++) {
        Unit *next_ele = unit_board[row][col];
        if (next_ele != nullptr) {
          /*              printf("\tunit_board[%d][%d] is non-nullptr\n", row, col);*/
          SDL_Texture *next_icon = SDL_CreateTextureFromSurface(displayTMB, next_ele->getIcon());
          int obt_wid=0, obt_height=0;
          SDL_QueryTexture(next_icon, nullptr, nullptr, &obt_wid, &obt_height);
          
          float scale_factor = 1.0;
          char hp_s[7] = "xxx HP";
          sprintf(hp_s, "%d HP", next_ele->getHealth());
          
          if (obt_wid < obt_height) {
            scale_factor = ((float)(tile_height) / obt_height);
            /* Note: this currently centers the unit in the tile.
             * In the future, if we want to have the unit 
             * up against the bottom of the tile, then simply remove
             * the `/ 2.0` */
            float new_wid = scale_factor*obt_wid;
            float dx = ((tile_width - new_wid) / 2.0);
            /*                if (row || col) {*/
            const SDL_Rect dt = {static_cast<int>((col*tile_width) + dx), row*tile_height, static_cast<int>(new_wid), tile_height};
            if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
              std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
              std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
            }
            /*                } else {
             const SDL_Rect dt = {1, 1, new_wid, tile_height};
             SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
             }*/
            /*                SDL_SetRenderDrawColor(displayTMB, 5, 250, 50, 255);
             FC_Draw(font, displayTMB, (col*tile_width) + dx,
             row*tile_height, hp_s);*/
            
            if (next_ele->getTeam() == 0) {
              FC_Draw(fc_luxirb_player, displayTMB, (col*tile_width) + tile_width/2/* dx*/,
                      row*tile_height, hp_s);
            } else {
              FC_Draw(fc_luxirb_enemy, displayTMB, (col*tile_width) + tile_width/2/* dx*/,
                      row*tile_height, hp_s);
            }
          } else {
            scale_factor = ((float) (tile_width)) / obt_wid;;
            float new_height = scale_factor*obt_height;
            float dy = ((tile_height - new_height) / 2.0);
            /*                if (row || col) {*/
            const SDL_Rect dt = {col*tile_width, static_cast<int>((row*tile_height) + dy), tile_width, static_cast<int>(new_height)};
            if (SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt)) {
              std::cerr<<"[ ERR ]    SDL Error: "<<SDL_GetError()<<std::endl;
              std::cerr<<"[ ERR ]    Could not draw the unit icon for "<<next_ele->toString()<<"."<<std::endl;
            }
            /*                } else {
             const SDL_Rect dt = {1, 1+dy, tile_width, new_height};
             SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
             }*/
            /*                SDL_SetRenderDrawColor(displayTMB, 5, 250, 50, 255);
             FC_Draw(font, displayTMB, col*tile_width,
             (row*tile_height) + dy, hp_s);*/
            if (next_ele->getTeam() == 0) {
              FC_Draw(fc_luxirb_player, displayTMB, (col*tile_width),
                      row*tile_height+dy, hp_s);
            } else {
              FC_Draw(fc_luxirb_enemy, displayTMB, (col*tile_width),
                      row*tile_height+dy, hp_s);
            }
          }
        }/* else {
          printf("unit_board[%d][%d] is nullptr\n", row, col);
          }*/
      }
    }
    /* unsigned int ind;
     for (ind = 0; ind < player_units.size(); ind++) {
     Unit *next_ele = player_units.at(ind);
     int row = next_ele->getY();
     int col = next_ele->getX();
     SDL_Texture *next_icon = SDL_CreateTextureFromSurface(next_ele->getIcon());
     float obt_wid = al_get_bitmap_width(next_icon);
     float obt_height = al_get_bitmap_height(next_icon);
     float scale_factor = 1.0;
     if (obt_wid < obt_height) {
     scale_factor = tile_height / obt_height;
     / * Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0`
     * /
     float new_wid = scale_factor*obt_wid;
     float dx = ((tile_width - new_wid) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {(col*tile_width) + dx, row*tile_height, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     } else {
     scale_factor = tile_width / obt_wid;;
     float new_height = scale_factor*obt_height;
     float dy = ((tile_height - new_height) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {col*tile_width, (row*tile_height) + dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1+dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     }
     }
     for (ind = 0; ind < enemy_units.size(); ind++) {
     Unit *next_ele = enemy_units.at(ind);
     int row = next_ele->getY();
     int col = next_ele->getX();
     SDL_Texture *next_icon = SDL_CreateTextureFromSurface(next_ele->getIcon());
     float obt_wid = al_get_bitmap_width(next_icon);
     float obt_height = al_get_bitmap_height(next_icon);
     float scale_factor = 1.0;
     if (obt_wid < obt_height) {
     scale_factor = tile_height / obt_height;
     / * Note: this currently centers the unit in the tile.
     * In the future, if we want to have the unit 
     * up against the bottom of the tile, then simply remove the `/ 2.0`
     * /
     float new_wid = scale_factor*obt_wid;
     float dx = ((tile_width - new_wid) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {(col*tile_width) + dx, row*tile_height, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1, new_wid, tile_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     } else {
     scale_factor = tile_width / obt_wid;;
     float new_height = scale_factor*obt_height;
     float dy = ((tile_height - new_height) / 2.0);
     if (row || col) {
     const SDL_Rect dt = {col*tile_width, (row*tile_height) + dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     } else {
     const SDL_Rect dt = {1, 1+dy, tile_width, new_height};
     SDL_RenderCopy(displayTMB, next_icon, nullptr, &dt);
     
     }
     }
     }
     */
  }
  return 0;
}

/* Precondition: file_name is non-nullptr valid file name with a proper path included.
 * Postcondition: the variables to which width and height are pointers will be
 *    set correctly;
 *   the game board will be properly read from the appropriate file, and
 *    saved in the 2d array to which `board` points.
 *   ALSO allocates memory for the `unit_board` pointer!  */
int TacticalMapBattle::load_map(const char *file_name) {
  FILE *input = fopen(file_name, "r");
  int row_ind = 0;
  int col_ind, ret_val;
  
  /* Look to the port!
   * Look to the STARBOARD!
   * Heh heh. Get it? STAR board? Like: "*board"? Heh.
   * I bet no one but me will ever read that joke.
   * Contact me (chocorho) if you read it.
   * (Seriously, it's getting very lonely in here.)
   *  e a s t e r  e g g  # 1.
   *   5500 lines, 348 days
   * ONE BY ONE THE PENGUINS SLOWLY STEAL MY SANITY.
   */
  ret_val = fscanf(input, "%d %d\n", &tiles_x, &tiles_y);
  if (ret_val != 2) {
    std::cerr<<"ERR: failed to read number of tiles used in"
      <<"the width and height"<<std::endl;
    return -1;
  }
  if (terrain_board != nullptr) {
    std::cerr<<"\tWARNING: THE GAME BOARD IS NON-nullptr, and we are expected"
      <<"to re-load it."<<std::endl;
    std::cerr<<"\tAs a result, we will free the memory and re-allocate it."<<std::endl;
    delete terrain_board;
  }
  terrain_board = new TERRAIN_TYPE*[tiles_x];
  if ((unit_board) != nullptr) {
    std::cerr<<"\tWARNING: THE UNIT BOARD IS NON-nullptr, and we are expected"
      <<"to re-load it."<<std::endl;
    std::cerr<<"\tAs a result, we will free the memory and re-allocate it."
      <<std::endl;
    delete unit_board;
  }
  unit_board = new Unit**[tiles_x];
  
  for (; row_ind < tiles_y; row_ind ++) {
    terrain_board[row_ind] = new TERRAIN_TYPE[tiles_x]();
    unit_board[row_ind] = new Unit*[tiles_x]();
    for (col_ind = 0; col_ind < (tiles_x-1); col_ind ++) {
      /* TODO replace with a simpler C++ implementation! */
      ret_val = fscanf(input, "%d ", (terrain_board[row_ind])+col_ind);
      if (ret_val != 1) {
        std::cerr<<"ERR: failed to read number of tiles used in the width and"
          <<"height"<<std::endl;
        return -1;
      }
      /*          fscanf(input, "%d", ((**board)+((row_ind)*(tiles_y))+(col_ind)));*/
    }
    ret_val = fscanf(input, "%d\n", (terrain_board[row_ind])+col_ind);
    if (ret_val != 1) {
      std::cerr<<"ERR: failed to read number of tiles"
        <<"used in the width and height"<<std::endl;
      return -1;
    }
    /*        fscanf(input, "%d", (*board)[row_ind][col_ind]);*/
    /*        fscanf(input, "%d\n", ((**board)+((row_ind)*(tiles_y))+tiles_x-1));*/
  }
  fclose(input);
  
  /* NOW initialize the graph structure.
   * A new addition for 0.0.4. I've been debating 
   * exactly what structure this should be (as is clear from
   * the commented sections), but I think I will try leaving 
   * the hashing to the COMPUTER. */
  //      graph = new int*[tiles_x*tiles_y];
  /* TODO consider combining some of these loops */
  graph = new std::list<Edge>*[tiles_x*tiles_y];
  for (int ind=0; ind < tiles_x*tiles_y; ind++) {
    graph[ind] = new std::list<Edge>();
  }
  locale = new int*[tiles_y];
  for (int initInd = 0; initInd < tiles_y; initInd ++) {
    locale[initInd] = new int[tiles_x]();
  }
  
  /* NW corner */
  if (canMove(0, 0)) {
//    graph[tiles_x*rowI+colI+1] = MOVE_COST[terrain_board[rowI][colI]];
//    graph[tiles_x*(rowI+1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
    int c = MOVE_COST[terrain_board[0][0]];
    graph[1]->emplace_back(Edge(0, 1, 0, 0, c));
    graph[tiles_x]->emplace_back(Edge(1, 0, 0, 0, c));
  }
  
  /* NE corner */
  if (canMove(tiles_x-1, 0)) {
//    graph[tiles_x*rowI+colI-1] = MOVE_COST[terrain_board[rowI][colI]];
//    graph[tiles_x*(rowI+1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
    int c = MOVE_COST[terrain_board[0][tiles_x-1]];
    graph[tiles_x-2]->emplace_back(Edge(tiles_x-2, 0, tiles_x-1, 0, c));
    graph[(2*tiles_x)-1]->emplace_back(Edge(tiles_x-1, 1, tiles_x-1, 0, c));
  }
  
  /* SW corner */
  if (canMove(0, tiles_y-1)) {
//    graph[tiles_x*rowI+colI-1] = MOVE_COST[terrain_board[rowI][colI]];
//    graph[tiles_x*(rowI-1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
    int c = MOVE_COST[terrain_board[tiles_y-1][0]];
    graph[tiles_x*(tiles_y-2)]->emplace_back(Edge(0, tiles_y-2, 0, tiles_y-1, c));
    graph[tiles_x*(tiles_y-1)+1]->emplace_back(Edge(1, tiles_y-1, 0, tiles_y-1, c));
  }
  
  /* SE corner */
  if (canMove(tiles_x-1, tiles_y-1)) {
//    graph[tiles_x*rowI+colI+1] = MOVE_COST[terrain_board[rowI][colI]];
//    graph[tiles_x*(rowI-1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
    int c = MOVE_COST[terrain_board[tiles_y-1][tiles_x-1]];
    graph[tiles_x*(tiles_y-2)+tiles_x-1]->emplace_back(Edge(tiles_x-1, tiles_y-2, tiles_x-1, tiles_y-1, c));
    graph[tiles_x*(tiles_y-1)+tiles_x-2]->emplace_back(Edge(tiles_x-2, tiles_y-1, tiles_x-1, tiles_y-1, c));
  }
  
  /* Northern row */
  for (int colI=1; colI < tiles_x-1; colI ++) {
    if (canMove(colI, 0)) {
      //          graph[colI-1] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[colI+1] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
      int c = MOVE_COST[terrain_board[0][colI]];
      graph[colI-1]->emplace_back(Edge(colI-1, 0, colI, 0, c));
      graph[colI+1]->emplace_back(Edge(colI+1, 0, colI, 0, c));
      graph[tiles_x+colI]->emplace_back(Edge(colI, 1, colI, 0, c));
    }
  }
  
  /* Southern row */
  for (int colI=1; colI < tiles_x-1; colI ++) {
    if (canMove(colI, tiles_y-1)) {
      //          graph[tiles_x*(tiles_y-1)+colI-1] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(tiles_y-1)+colI+1] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(tiles_y-2)+colI] = MOVE_COST[terrain_board[rowI][colI]];
      int c = MOVE_COST[terrain_board[tiles_y-1][colI]];
      graph[tiles_x*(tiles_y-1)+colI-1]->emplace_back(Edge(colI-1, tiles_y-1, colI, tiles_y-1, c));
      graph[tiles_x*(tiles_y-1)+colI+1]->emplace_back(Edge(colI+1, tiles_y-1, colI, tiles_y-1, c));
      graph[tiles_x*(tiles_y-2)+colI]->emplace_back(Edge(colI, tiles_y-2, colI, tiles_y-1, c));
    }
  }
  
  /* Left column */
  for (int rowI=1; rowI < tiles_y-1; rowI ++) {
    if (canMove(0, rowI)) {
      //          /* Add this vertex to the graph AND
      //           * */
      //          graph[tiles_x*rowI+1] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(rowI-1)] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(rowI+1)] = MOVE_COST[terrain_board[rowI][colI]];
      int c = MOVE_COST[terrain_board[rowI][0]];
      graph[tiles_x*rowI+1]->emplace_back(Edge(1, rowI, 0, rowI, c));
      graph[tiles_x*(rowI-1)]->emplace_back(Edge(0, rowI-1, 0, rowI, c));
      graph[tiles_x*(rowI+1)]->emplace_back(Edge(0, rowI+1, 0, rowI, c));
    }
  }
  
  /* Right column */
  for (int rowI=1; rowI < tiles_y-1; rowI ++) {
    if (canMove(tiles_x-1, rowI)) {
      /* Add this vertex to the graph AND
       * */
      //          graph[tiles_x*rowI+tiles_x-2] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(rowI-1)+tiles_x-1] = MOVE_COST[terrain_board[rowI][colI]];
      //          graph[tiles_x*(rowI+1)+tiles_x-1] = MOVE_COST[terrain_board[rowI][colI]];
      int c = MOVE_COST[terrain_board[rowI][tiles_x-1]];
      graph[tiles_x*rowI+tiles_x-2]->emplace_back(Edge(tiles_x-2, rowI, tiles_x-1, rowI, c));
      graph[tiles_x*(rowI-1)+tiles_x-1]->emplace_back(Edge(tiles_x-1, rowI-1, tiles_x-1, rowI, c));
      graph[tiles_x*(rowI+1)+tiles_x-1]->emplace_back(Edge(tiles_x-1, rowI+1, tiles_x-1, rowI, c));
    }
  }
  
  /* Finally, all the rest of the vertices,
   * a.k.a. the elegant part :D */
  for (int rowI=1; rowI < tiles_y-1; rowI ++) {
    for (int colI=1; colI < tiles_x-1; colI ++) {
      if (canMove(colI, rowI)/*terrain_board[][] != 0*/) {
        //            graph[tiles_x*rowI+colI-1] = MOVE_COST[terrain_board[rowI][colI]];
        //            graph[tiles_x*rowI+colI+1] = MOVE_COST[terrain_board[rowI][colI]];
        //            graph[tiles_x*(rowI-1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
        //            graph[tiles_x*(rowI+1)+colI] = MOVE_COST[terrain_board[rowI][colI]];
        int c = MOVE_COST[terrain_board[rowI][colI]];
        graph[tiles_x*rowI+colI-1]->emplace_back(Edge(colI-1, rowI, colI, rowI, c));
        graph[tiles_x*rowI+colI+1]->emplace_back(Edge(colI+1, rowI, colI, rowI, c));
        graph[tiles_x*(rowI-1)+colI]->emplace_back(Edge(colI, rowI-1, colI, rowI, c));
        graph[tiles_x*(rowI+1)+colI]->emplace_back(Edge(colI, rowI+1, colI, rowI, c));
      }
    }
  }
  return 0;
}

int TacticalMapBattle::load_units(std::vector<Unit *> &side0_units,
                 std::vector<Unit *> &side1_units) {
  for (/*const */Unit *nextSide0U : side0_units) {
    int x_com = nextSide0U->getX();
    int y_com = nextSide0U->getY();
    unit_board[y_com][x_com] = nextSide0U;
  }
  
  for (/*const */Unit *nextSide1U : side1_units) {
    int x_com = nextSide1U->getX();
    int y_com = nextSide1U->getY();
    unit_board[y_com][x_com] = nextSide1U;
  }
  return 0;
}

int TacticalMapBattle::load_terrain_tiles(const char *name) {
  FILE *input = fopen(name, "r");
  int ind, exit;
  char file_name_container[50];
  /*      terrain_tiles = (SDL_Texture*)[NUM_TERRAIN_TYPES];*/
  /*      terrain_tiles = (SDL_Texture *[NUM_TERRAIN_TYPES]) malloc(NUM_TERRAIN_TYPES * sizeof(SDL_Texture *));*/
  for (ind=0; ind < NUM_TERRAIN_TYPES; ind ++) {
    exit = fscanf(input, "%s\n", file_name_container);
    if (exit != 1) {
      std::cerr<<"ERR: failed to read number of tiles used in"
        <<"the width and height"<<std::endl;
      return -1;
    }
    SDL_Surface *terrain_surf = IMG_Load_RW(SDL_RWFromFile(file_name_container, "rb"), 1);
    terrain_tiles[ind] = SDL_CreateTextureFromSurface(displayTMB, terrain_surf);
  }
  fclose(input);
  return 0;
}

enum ATTACK_RESULT {
  OK=0, FRIENDLY_FIRE=-1, OUT_OF_ARROWS=-2, OUT_OF_USES=-3, DODGED=-4, UC_NOT_FOUND=-5
};

/* As of the JULY 2019 REFACTOR,
 * This method is now a static member of the TMB class,
 * incorporating all the individual Unit subclass attack()
 * algorithms, all in one place. */
int TacticalMapBattle::attack(Unit *c, Unit *target, Weapon *w,
                              int terrain, bool mustHit) {
  switch (c->getUC()) {
    case ARCHER:
      {
        /* Key for attack() return values: (copied from Archer class)
         *  -1: tried to attack a unit on the same team
         *  -2: Quiver out of arrows
         *  -3: ability not available, already used
         *  -4: DODGED */
        /* There are two important checks here:
         * whether the quiver of arrows has enough (is now empty),
         * and a check on whether the archer is not trying to 
         *    shoot a teammate.
         * (I suspect the team check is unnecessary but 
         *  I will keep it in this version as a consistency check) */
        if (mustHit) {
          if (c->getCanAbility()) {
            if (target->getTeam() != c->getTeam() && w->fire()) {
              int armourDifference = (w->getAP())-(target->getArmour());
              if (armourDifference > 0) {
                armourDifference = 0;
              }
              int dmg = (((generator())%(w->getMaxDamage() - w->getMinDamage()))
                  + w->getMinDamage() - armourDifference);
              target->dealDamage(dmg,c,c->getStrength(), w->getMaxDamage(), 0);
              c->addExp(9*dmg);
            } else if ((target->getTeam() == c->getTeam())) {
/*            std::cerr << "Inconsistency -- unit "+c->toString()+" is trying to target a teammate!" << std::endl;*/
              return FRIENDLY_FIRE;
            } else {
/*            std::cerr << "Quiver of arrows is empty!" << std::endl;*/
              return OUT_OF_ARROWS;
            }
            c->setCanUseAbility(false);
            return 0;
          } else {
            return OUT_OF_USES;
          }
        } else {
          if (target->getTeam() != c->getTeam() && w->fire()) {
            if (c->accuracyTest(target, w->getAccuracy(), terrain, 0) == 0) {
              int armourDifference = w->getAP() - target->getArmour();
              if (armourDifference > 0) {
                armourDifference = 0;
              }
              int dmg = (((generator())%(w->getMaxDamage()-w->getMinDamage()))
                  + w->getMinDamage() - armourDifference);
              target->dealDamage(dmg, c, c->getStrength(), w->getMaxDamage(), 0);
              c->addExp(9*dmg);
            } else {
              std::string internal_name = c->toString();
              std::cout << "[MISS] " << internal_name << " shot an arrow and missed." << std::endl;
            }
          } else if ((target->getTeam() == c->getTeam())) {
/*            std::cerr << "[ERR] teams match!" << std::endl;*/
            return FRIENDLY_FIRE;
          } else {
/*            std::cerr << "Quiver of arrows is empty!" << std::endl;*/
            return OUT_OF_ARROWS;
          }
          return OK;
        }
      }
    case SOLDIER:
      {
        if (c->checkAdjacent(target->getX(), target->getY()) &&
            (target->getTeam() != c->getTeam())) {
/*        if (!canDodge(this, target)) {*/
          int armourDifference = w->getAP() - target->getArmour();
          if (armourDifference > 0) {
            armourDifference = 0;
          }
/*          int denom = generator() % 100;
          int numer = generator() % denom;
          target->dealDamage(
            (int)(((numer*(w->getMaxDamage()-
            w->getMinDamage())) / denom)
            + w->getMinDamage()
            - armourDifference),
          c, c->getStrength(), w->getMaxDamage(), 0);*/
          int dmg = ((generator() % (w->getMaxDamage() - w->getMinDamage()))
              + w->getMinDamage() - armourDifference);
          target->dealDamage(dmg, c, c->getStrength(), w->getMaxDamage(), 0);
          c->addExp(9*dmg);
          return OK;
/*          } else {
            return -4;
          }*/
        } else {
          return FRIENDLY_FIRE;
        }
        break;
      }
    default:
      {
        return UC_NOT_FOUND;
      }
  }
}

void TacticalMapBattle::setSettlementName(const char *settlement) {
  loc_name = settlement;
}

void TacticalMapBattle::setSettlementName(const std::string &settlement) {
  loc_name = settlement;
}

const std::string &TacticalMapBattle::getSettlementName() const {
  return loc_name;
}

bool TacticalMapBattle::isProvinceBattle() const {
  return is_province;
}
void TacticalMapBattle::setProvince(bool prov_b) {
  is_province = prov_b;
}

/* Preconditions: This TMB object has been fully constructed, with
 *  a valid terrain map and unit board, 
 *  non-nullptr objects to contain sets of units,
 *  and a valid non-nullptr displayTMB. */
/* Note to self, don't worry, this function is already 
 * declared as `virtual` in the header file. */
TMB_RESULT/*int*/ TacticalMapBattle::launch() {
  /* "Just-in-time Walk-sprite Actualization":
   * Here, run through all relevant player units,
   * and actually allocate memory for their Textures. */
  for (Unit *unit : player_units) {
    unit->actualizeAllWalkAnims(displayTMB);
  }
  SDL_SetRenderTarget(displayTMB, layer2);
  /*      SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 56);*/
  SDL_RenderClear(displayTMB);
  
  SDL_SetRenderTarget(displayTMB, layer2);
  SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 2);
  SDL_RenderClear(displayTMB);
  
  SDL_SetRenderTarget(displayTMB, terrain_base);
  display_terrain_map(verbosity);
  SDL_SetRenderTarget(displayTMB, menu_img);
  
  SDL_SetRenderDrawColor(displayTMB, 10, 70, 131, 121);
  SDL_RenderFillRect(displayTMB, nullptr);
  /* Halloween color change:  */
  SDL_SetRenderDrawColor(displayTMB, 250, 120, 80, 60);
  SDL_SetRenderDrawColor(displayTMB, 10, 20, 181, 181);
  SDL_SetRenderTarget(displayTMB, nullptr);
/*  SDL_Texture *img = nullptr;*/
/*  SDL_Texture *img_displayTMB = nullptr;*/
/*  int visual_type = 0;*/
  TMB_RESULT/*int*/ rtn_result = ERR/*ENEMY_VICTORY*/;
  
  /*** INIT TIMERS ***/
  /*      tmbTicker = SDL_AddTimer(1000.0 / FPS, process_tick, this);
   if (tmbTicker == 0) {
   std::cerr<<"ERR: failed to load FPS timer :-("<<std::endl;
   SDL_DestroyRenderer(displayTMB);
   return ERR;
   }*/
  tmbEnemyAI = SDL_AddTimer(msPerAICheck, process_ai, this);
  if (tmbEnemyAI == 0) {
    std::cerr<<"[ ERR ]    Failed to load the enemy AI timer :-("<<std::endl;
    SDL_DestroyRenderer(displayTMB);
    SDL_RemoveTimer(tmbTicker);
    return ERR;
  }
  
  animation_timer = SDL_AddTimer(4000.0 / FPS, toggle_terrain_animation,
                                 this);
  if (animation_timer == 0) {
    std::cerr<<"ERR: failed to load start-animation timer :-("<<std::endl;
    SDL_DestroyRenderer(displayTMB);
    SDL_RemoveTimer(tmbTicker);
    SDL_RemoveTimer(tmbEnemyAI);
    return ERR;
  }
  /*** INIT FONTS ***/
  /* original font files were in /usr/share/fonts/TTF/ */
  TTF_Font *font_amok = TTF_OpenFont("./res/fonts/ComputerAmok.ttf", 54);
  if (font_amok == nullptr) {
    fprintf(stderr, "Failed to load the font ComputerAmok.ttf\n");
    SDL_DestroyRenderer(displayTMB);
    SDL_RemoveTimer(tmbTicker);
    SDL_RemoveTimer(tmbEnemyAI);
    return ERR;
  }
  TTF_Font *font_luxirb = TTF_OpenFont("./res/fonts/luxirb.ttf", 54);
  if (font_luxirb == nullptr) {
    fprintf(stderr, "Failed to load the font luxirb.ttf\n");
    SDL_DestroyRenderer(displayTMB);
    SDL_RemoveTimer(tmbTicker);
    SDL_RemoveTimer(tmbEnemyAI);
    return ERR;
  }
  //      TTF_Font *font_luxirb_small = TTF_OpenFont("./res/fonts/luxirb.ttf", 20);
  //      if (font_luxirb_small == nullptr) {
  //        fprintf(stderr, "Failed to load the font luxirb.ttf in small size\n");
  //        SDL_DestroyRenderer(displayTMB);
  //        SDL_RemoveTimer(tmbTicker);
  //        SDL_RemoveTimer(tmbEnemyAI);
  //        return ERR;
  //      }
  FC_Font *tmb_fc_luxirb_small = FC_CreateFont();
  FC_LoadFont(tmb_fc_luxirb_small, displayTMB, "res/fonts/luxirb.ttf", 20, FC_MakeColor(1,1,1,255), TTF_STYLE_NORMAL);
  
  TTF_Font *font_luxirb_vsmall = TTF_OpenFont("./res/fonts/luxirb.ttf", 16);
  if (font_luxirb_vsmall == nullptr) {
    fprintf(stderr, "Failed to load the font luxirb.ttf in VERY small size\n");
    SDL_DestroyRenderer(displayTMB);
    SDL_RemoveTimer(tmbTicker);
    SDL_RemoveTimer(tmbEnemyAI);
    return ERR;
  }
  
  /* Allegro colors, for reference:
               (font, al_map_rgb(250, 5, 50), ...);
               (font, al_map_rgb(5, 250, 50), ...); */
  fc_luxirb_player = FC_CreateFont();
  fc_luxirb_enemy = FC_CreateFont();
  FC_LoadFont(fc_luxirb_player, displayTMB, "res/fonts/luxirb.ttf", 16, FC_MakeColor(5,250,50,255), TTF_STYLE_NORMAL);
  FC_LoadFont(fc_luxirb_enemy, displayTMB, "res/fonts/luxirb.ttf", 16, FC_MakeColor(250,5,50,255), TTF_STYLE_NORMAL);
  
  TTF_Font *font_luxirr = TTF_OpenFont("res/fonts/luxirr.ttf", 54);
  if (font_luxirr == nullptr) {
    fprintf(stderr, "Failed to load the font luxirir.ttf\n");
    SDL_DestroyRenderer(displayTMB);
    SDL_RemoveTimer(tmbTicker);
    SDL_RemoveTimer(tmbEnemyAI);
    return ERR;
  }
  
  /*  chosen_province last_selected = {-1, -1};*/
  /*  img = IMG_Load_RW(SDL_RWFromFile("res/world_map.png", "rb"), 1);*/
  /*  img_displayTMB = IMG_Load_RW(SDL_RWFromFile("res/world_map.png", "rb"), 1);*/
  
  /*      int paused = 1;*/
  /*      bool global_redraw = false;*/
  SDL_LockMutex(redraw_mut);
  redraw = false;
  SDL_UnlockMutex(redraw_mut);
  
  /* Initial game state has the background map */
  SDL_QueryTexture(terrain_base, nullptr, nullptr, &(dframe->w), &(dframe->h));
  dframe->x = 1;
  dframe->y = 1;
  SDL_RenderCopy(displayTMB, terrain_base, nullptr, dframe);
  display_units(tile_side_length, tile_side_length);
  SDL_RenderPresent(displayTMB);
  
  const SDL_MessageBoxData cancel_box = {SDL_MESSAGEBOX_INFORMATION, tmb_wind,
    "Skip the TMB?", "Are you sure you want to skip? There is no "\
    "guarantee that you'll be able to fight this battle again.",
    3, cancel_buttons, nullptr};

  /* Load music */
  Mix_Music *tmbTheme0 = Mix_LoadMUS("res/music/Lobo_Loco_-_11_-_Snowmelt_Yukkon_River.ogg");
  if (tmbTheme0 == nullptr) {
    std::cerr<<"[ ERR ]    Could not load music theme #0 "<<std::endl;
  }
  
  Mix_Music *tmbTheme1 = Mix_LoadMUS("res/music/Meydn_-_02_-_Pure_Water.ogg");
  if (tmbTheme1 == nullptr) {
    std::cerr<<"[ ERR ]    Could not load music theme #1 "<<std::endl;
  }
  
  Mix_Music *tmbTheme2 = Mix_LoadMUS("res/music/Destructing_Own_Kingdom.ogg");
  if (tmbTheme2 == nullptr) {
    std::cerr<<"[ ERR ]    Could not load music theme #2 "<<std::endl;
  }
/*  themes.insert(DEFAULT, tmbTheme0);*/
  themes.emplace(DEFAULT, tmbTheme0);
/*  themes.insert(ONE_PLAYER_LEFT, tmbTheme1);*/
  themes.emplace(ONE_PLAYER_LEFT, tmbTheme1);
/*  themes.insert(ONE_ENEMY_LEFT, tmbTheme2);*/
  themes.emplace(ONE_ENEMY_LEFT, tmbTheme2);
/*  themes.insert(JUST_SEIZE_IT, tmbTheme2);*/
  themes.emplace(JUST_SEIZE_IT, tmbTheme2);
  themes.emplace(VICTORY, tmbTheme2);
  themes.emplace(DEFEAT, tmbTheme2);
  musicPhase = DEFAULT;
  checkForMusicTransition();
  
  int stat = Mix_PlayMusic(themes.at(musicPhase), 1);
  if (stat != 0) {
    std::cerr << "[ ERR ]    Music would not be played." << std::endl;
    std::cerr << SDL_GetError() << std::endl;
  }
  
  cheat[0] = false;
  cheat[1] = false;
  cheat[2] = false;
  showAnimation = true;
  
  /* MAIN GAME LOOP (MGL) (mgl) */
  while (game_state > 0) {
    /* Get next event and respond to it */
    SDL_Event ev;
    while (SDL_PollEvent(&ev)) {
      if (ev.type == SDL_KEYDOWN) {
        if (ev.key.keysym.sym == SDLK_q) {
          int result = 0;
          SDL_ShowMessageBox(&cancel_box, &result);
          if (result == 1) {
            setGameState(-1);
          } else if (result == 2) {
            setGameState(-2);
          } /*else { }*/
        } else if (ev.key.keysym.sym == SDLK_SPACE) {
        } else if (ev.key.keysym.sym == SDLK_g) {
          cheat[0] = true;
        } else if (ev.key.keysym.sym == SDLK_o) {
          if (cheat[0]) {
            cheat[1] = true;
          }
        } else if (ev.key.keysym.sym == SDLK_d) {
          if (cheat[0] && cheat[1]) {
            cheat[2] = true;
          }
        } else if (ev.key.keysym.sym == SDLK_n || 
                   ev.key.keysym.sym == SDLK_c) {
          cheat[2] = false;
        } else if (ev.key.keysym.sym == SDLK_m) {
          if (musicOn) {
            Mix_FadeOutMusic(500);
          } else {
/*            try {
                Mix_FadeInMusic(themes.at(musicPhase), -1, 500);*/
              Mix_Volume(-1, MIX_MAX_VOLUME/3);
              Mix_FadeInMusic(themes.at(musicPhase), -1, 500);
/*            } catch () {
              
            }*/
          }
          musicOn = !musicOn;
        } else if (ev.key.keysym.sym == SDLK_a) {
          showAnimation = !showAnimation;
        }
      } else if (ev.type == SDL_KEYUP) {
      } else if ((ev.type == SDL_WINDOWEVENT) &&
                 (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
        SDL_LockMutex(redraw_mut);
        redraw = true;
        SDL_UnlockMutex(redraw_mut);
      } else if (ev.type == SDL_WINDOWEVENT &&
                 ev.window.event == SDL_WINDOWEVENT_CLOSE) {
        int result = 0;
        SDL_ShowMessageBox(&cancel_box, &result);
        if (result == 1) {
          setGameState(-1);
        } else if (result == 2) {
          setGameState(-2);
        } /*else { }*/
      } else if ((ev.type == SDL_MOUSEBUTTONDOWN) &&
                 (ev.button.windowID == SDL_GetWindowID(tmb_wind))) {
      } else if ((ev.type == SDL_MOUSEBUTTONUP) &&
                 (ev.button.windowID == SDL_GetWindowID(tmb_wind))) {
        /*          menu.clear();*/
        /* In the future, perhaps spawn new threads to deal with this?
         * This could improve efficiency and also make the system (the main thread)
         * focus more on responding to user input */
        /*          printf("%d, %d: mouse click UP detected\n", ev.button.x, ev.button.y);*/
        if (getTurnPhase() == 0) {
          /*              std::cout<<"[DEBUG]    Now processing a click at "<<ev.button.x<<
           ","<<ev.button.y<<std::endl;*/
          answer_click(ev.button.x, ev.button.y);
          update_menu_img();
          repaint();
        }
      } else {
        
      }
    }
    /* Now, if necessary, redraw the map */
    SDL_LockMutex(redraw_mut);
    if (redraw) {
      redraw = false;
      
      /*          SDL_SetRenderTarget(displayTMB, layer2);*/
      SDL_SetRenderTarget(displayTMB, nullptr);
      /*          display_terrain_map(0);*/
      SDL_SetRenderDrawColor(displayTMB, 255, 255, 255, 255);
      SDL_RenderClear(displayTMB);
      
      /* First (under everything else) draw the terrain map */
      SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 255);
      SDL_QueryTexture(terrain_base, nullptr, nullptr, &(dframe->w), &(dframe->h));
      dframe->x = 1;
      dframe->y = 1;
      SDL_RenderCopy(displayTMB, terrain_base, nullptr, dframe);
      
      /* Then draw the menu image on the right column */
      SDL_QueryTexture(menu_img, nullptr, nullptr, &(dframe->w), &(dframe->h));
      dframe->x = terrain_map_width+1;
      dframe->y = 1;
      SDL_RenderCopy(displayTMB, menu_img, nullptr, dframe);
      
      //          /* Finally, draw the secondary layer of units on the map. */
      //          SDL_SetRenderTarget(displayTMB, layer2);
      display_units(tile_side_length, tile_side_length);
      SDL_SetRenderTarget(displayTMB, nullptr);
      SDL_QueryTexture(layer2, nullptr, nullptr, &(dframe->w), &(dframe->h));
      dframe->x = 0;
      dframe->y = 0;
      SDL_RenderCopy(displayTMB, layer2, nullptr, dframe);
      dframe->x = 5;
      dframe->y = 5;
      dframe->w = 2*tile_side_length;
      dframe->h = tile_side_length/3;
      
      /* Setting font draw color may be unnecessary here: */
      if (TMB_SCREEN_WIDTH < 1000) {
        if (getTurnPhase() == 1) {
          SDL_RenderCopy(displayTMB, enemy_phase_text, nullptr, dframe);
        } else {
          SDL_RenderCopy(displayTMB, player_phase_text, nullptr, dframe);
        }
      } else /*if ()*/ {
        if (getTurnPhase() == 1) {
          SDL_RenderCopy(displayTMB, enemy_phase_text, nullptr, dframe);
        } else {
          SDL_RenderCopy(displayTMB, player_phase_text, nullptr, dframe);
        }
      }
      SDL_RenderPresent(displayTMB);
    }
    SDL_UnlockMutex(redraw_mut);
  }
  
  /* Finally - display the victory or defeat overlay. */
  int final_state = getGameState();
  if (final_state == (-1)) { // player victory
    SDL_SetRenderDrawColor(displayTMB, 233, 233, 233, 112);
    dframe->x = 20;
    dframe->y = 20;
    dframe->w = TMB_SCREEN_WIDTH-40;
    dframe->h = TMB_SCREEN_HEIGHT-60;
    SDL_RenderFillRect(displayTMB, dframe);
    SDL_SetRenderDrawColor(displayTMB, 20, 250, 20, 255);
    dframe->x = 3.8f*TMB_SCREEN_WIDTH/16;
    dframe->y = 1.5f*TMB_SCREEN_HEIGHT/8;
    dframe->w = (8.4f*TMB_SCREEN_WIDTH)/16;
    dframe->h = (TMB_SCREEN_HEIGHT/10);
    SDL_RenderDrawRect(displayTMB, dframe);
    SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 255);
    SDL_RenderCopy(displayTMB, victory_text, nullptr, dframe);
    
    unsigned int unit_ind = 0;
    unsigned int padding = player_units.size();
    while (unit_ind < padding) {
      Unit *next_pl = player_units.at(unit_ind);
      std::string next_name = next_pl->toString();
      SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 255);
      FC_Draw(tmb_fc_luxirb_small, displayTMB,
              TMB_SCREEN_WIDTH/4, 3.0f*TMB_SCREEN_HEIGHT/8+
              (MENU_OPTION_HEIGHT*(unit_ind+1)), next_name.c_str());
      unit_ind ++;
    }
    unit_ind = 0;
    /*                while (unit_ind < debilitated_players.size()) {
     Unit *next_pl = debilitated_players.at(unit_ind);
     SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 255);
     FC_Draw(tmb_fc_luxirb_small, ,
     TMB_SCREEN_WIDTH/4, 3.0f*TMB_SCREEN_HEIGHT/8+(MENU_OPTION_HEIGHT*(unit_ind+padding+1)), next_pl->toString().c_str());
     unit_ind ++;
     }*/
    
    SDL_RenderPresent(displayTMB);
    while (game_state) {
      /* Get next event and respond to it */
      SDL_Event ev;
      while (SDL_PollEvent(&ev)) {
        if (ev.type == SDL_KEYDOWN) {
          setGameState(0);
        } else if ((ev.type == SDL_WINDOWEVENT) &&
                   (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
          SDL_LockMutex(redraw_mut);
          redraw = true;
          SDL_UnlockMutex(redraw_mut);
        } else if (ev.type == SDL_WINDOWEVENT) {
          if (ev.window.event == SDL_WINDOWEVENT_CLOSE) {
            setGameState(0);
          }
        } else if ((ev.type == SDL_MOUSEBUTTONDOWN) &&
                   (ev.button.windowID == SDL_GetWindowID(tmb_wind))) {
          setGameState(0);
        }
      }
      SDL_LockMutex(redraw_mut);
      if (redraw) {
        redraw = false;
        SDL_RenderPresent(displayTMB);
      }
      SDL_UnlockMutex(redraw_mut);
    }
    rtn_result = PLAYER_VICTORY;
  } else if (final_state == (-2)) { // player defeat
    SDL_SetRenderDrawColor(displayTMB, 233, 233, 233, 112);
    dframe->x = 20;
    dframe->y = 20;
    dframe->w = (TMB_SCREEN_WIDTH-20)-(20);
    dframe->h = (TMB_SCREEN_HEIGHT-40)-(20);
    SDL_RenderFillRect(displayTMB, dframe);
    SDL_SetRenderDrawColor(displayTMB, 250, 20, 20, 255);
    dframe->x = 3.8f*TMB_SCREEN_WIDTH/16;
    dframe->y = 3.0f*TMB_SCREEN_HEIGHT/8;
    dframe->w = (8.4f*TMB_SCREEN_WIDTH/16);
    dframe->h = TMB_SCREEN_HEIGHT/10;
    SDL_RenderDrawRect(displayTMB, dframe);
    SDL_RenderCopy(displayTMB, defeat_text, nullptr, dframe);
    
    SDL_RenderPresent(displayTMB);
    while (game_state) {
      /* Get next event and respond to it */
      SDL_Event ev;
      while (SDL_PollEvent(&ev)) {
        if (ev.type == SDL_KEYDOWN) {
          setGameState(0);
        } else if ((ev.type == SDL_WINDOWEVENT) &&
                   (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
          SDL_LockMutex(redraw_mut);
          redraw = true;
          SDL_UnlockMutex(redraw_mut);
        } else if ((ev.type == SDL_WINDOWEVENT) &&
                   (ev.window.event == SDL_WINDOWEVENT_CLOSE)) {
          setGameState(0);
        } else if ((ev.type == SDL_MOUSEBUTTONDOWN) &&
                   (ev.window.windowID == SDL_GetWindowID(tmb_wind))) {
          setGameState(0);
        }
        SDL_LockMutex(redraw_mut);
        if (redraw) {
          redraw = false;
          SDL_RenderPresent(displayTMB);
        }
        SDL_UnlockMutex(redraw_mut);
      }
    }
    rtn_result = ENEMY_VICTORY;
  }
//  int r; // used only in free-ing. So... probably obselete now.
//  for (r = 0; r < tiles_y; r ++) {
//    delete[] terrain_board[r];
//    delete[] unit_board[r];
//  }
  delete[] terrain_board;
  delete[] unit_board;
  SDL_DestroyTexture(terrain_base);
  SDL_DestroyTexture(layer2);
  SDL_DestroyTexture(menu_img);
  
  /* Stop / delete timers */
  SDL_RemoveTimer(tmbTicker);
  SDL_RemoveTimer(tmbEnemyAI);
  SDL_RemoveTimer(animation_timer);
  
  /* Delete fonts */
  if (fc_luxirb_player != nullptr) {
    FC_FreeFont(fc_luxirb_player);
  }
  if (fc_luxirb_enemy != nullptr) {
    FC_FreeFont(fc_luxirb_enemy);
  }
  
  /* Delete most vital graphics displayTMB structs */
  SDL_DestroyRenderer(displayTMB);
  SDL_DestroyWindow(tmb_wind);
  living_players = player_units;

  /* TODO "de-actualize" the Player Unit Textures,
   * and do it cleanly using a separate TMB function call.*/
  for (Unit *u : living_players) {
    u->deActualizeWalkAnims();
  }
  for (Unit *u : debilitated_players) {
    u->deActualizeWalkAnims();
  }
  return rtn_result;
}

/* just some public accessor methods for after the battle ends */
const std::vector<Unit *> &TacticalMapBattle::getDebilPlayers() const {
  return debilitated_players;
}

const std::vector<Unit *> &TacticalMapBattle::getLivingPlayers() const {
  return living_players;
}

const std::vector<Unit *> &TacticalMapBattle::getDebilEnemies() const {
  return debilitated_enemies;
}


/*** FUNCTIONS ***/
/* First, put the timer functions. */
/*static*/ Uint32 TacticalMapBattle::process_tick(Uint32 step, void *argv) {
  TacticalMapBattle *instance = (TacticalMapBattle *) argv;
  instance->repaint();
  return step;
}

/*static*/ Uint32 TacticalMapBattle::process_ai(Uint32 step, void *args) {
  TacticalMapBattle *instance = (TacticalMapBattle *) args;
  int phase;
  if ((phase = instance->getTurnPhase())) {
    instance->post_turnphase();
    /*        instance->moved_set.clear();*/
    instance->clearMovedSet();
    instance->process_one_ai(phase/*instance->getTurnPhase()*/);
    
    /* Now set the text to announce a phase change */
    instance->repaint();
  } else if (instance->isCheating()/*cheat[2]*/) {
    instance->post_turnphase();
    instance->clearMovedSet();
    instance->process_one_ai(phase);
    instance->repaint();
  }
  bool enemy_defeat = instance->check_victory_condition();
  /*            bool enemy_defeat = true;*/
  bool player_defeat = instance->check_loss_condition();
  /* Finally, check for any  combination of victory conditions */
  /*            if ((condition & 1) == 1) { // rout
   if (enemy_units.size() == 0) {
   enemy_defeat = true;
   } else {
   enemy_defeat = false;
   }
   } else if ((condition & 2) == 2 ) { */// boss enemies
  /* Primitive O(|enemy_list|) solution:
   * make sure each living enemy is not a boss.
   */
  /* TODO make this more efficient.
   * Ideas: 
   *    Provide subclasses for the TacticalMapBattle class itself?
   *
   *    Advantages:
   *    _Perhaps SOME subclasses (and not others) can contain a list of BOSS enemies,
   *    reducing the computation time here.
   *    _This would also remove the need for a "is_boss()" flag in the Unit class.
   *    _It would remove the need for the `condition` variable.
   *
   *    Drawbacks:
   *    _But maybe this wouldn't be worth the effort if the
   *    total number of enemy units will always be low.
   *
   *//*
      unsigned int ind = 0;
      while (ind < enemy_units.size()) {
      if (enemy_units.at(ind)->is_boss()) { enemy_defeat = false;
      }
      ind ++;
      }
      }*/
  /* Seizing a point would trigger victory when the PLAYER
   * chooses "Seize", not after the enemy moves.
   if (condition & 4 == 4) { // seize a point
   if (enemy_defeat) {
   
   }
   }
   */
  if (player_defeat) {
    instance->setGameState(-2);
  } else if (enemy_defeat) {
    instance->setGameState(-1);
  }
  return step;
}

Uint32 TacticalMapBattle::toggle_terrain_animation(Uint32 step, void *args) {
  /* Uhhh.
   * how did I do this again? TODO figure out the terrain image swapping. */
  return step;
}

Uint32 TacticalMapBattle::toggle_unit_animation(Uint32 step, void *args) {
  /* TODO implement this, probably by:
   *   obtaining a (so far non-existence) mutex/lock on the unit data;
   *   forwarding each unit's frame to the next in the sequence;
   *   releasing the lock. */
  return step;
}

/* Now -- to include all the helper functions (there are several,
 * may want to move them to a separate structure later).
 * But at least they're all in one place.  */
/* TODO Figure out a more elaborate spawning scheme for each map */
int TacticalMapBattle::spawn_locate_units() {
  for (size_t ind = 0; ind < player_units.size(); ind ++) {
    Unit *next_unit = player_units.at(ind);
    if (next_unit == nullptr) {
      printf("This is really bad, the PLAYER UNITS vector has a"
             " nullptr element at %zd\n", ind);
    }
    bool can_place = false;
    int row_scan = 0;
    int col_scan = 0;
    for (; row_scan < tiles_y; row_scan ++) {
      for (col_scan=0; col_scan < tiles_x; col_scan ++) {
        if ((terrain_board[row_scan][col_scan] != 0)
            && (unit_board[row_scan][col_scan] == nullptr)) {
          /* This tile CAN be used as a spawning tile.*/
          /*              int old_y = next_unit->getY();
           int old_x = next_unit->getX();
           unit_board[old_y][old_x] = nullptr;
           printf("Moving player unit from (%d,%d) to (%d,%d)\n",
           old_x, old_y, col_scan, row_scan);*/
          next_unit->setX(col_scan);
          next_unit->setY(row_scan);
          unit_board[row_scan][col_scan] = next_unit;
          can_place = true;
          col_scan = tiles_x;
          row_scan = tiles_y;
        }
      }
    }
    if (!can_place) {
      std::cerr << "[ERR] -- not enough spaces to realloc" << std::endl;
      std::cerr << "ate the units on the board!!" << std::endl;
      return -1;
    }
  }
  
/*  std::cout << "enemy unit vector size is " << enemy_units.size() << std::endl;*/
  for (size_t ind = 0; ind < enemy_units.size(); ind ++) {
    Unit *next_unit = enemy_units.at(ind);
    if (next_unit == nullptr) {
      std::cerr << "[CRITICAL ERR] ENEMY UNITS vector has a nullptr element at " << ind << std::endl;
    }
    bool can_place = false;
    for (int row_scan = tiles_y-1; row_scan >= 0; row_scan --) {
      for (int col_scan=tiles_x-1; col_scan >= 0; col_scan --) {
        if ((terrain_board[row_scan][col_scan] != 0)
            && (unit_board[row_scan][col_scan] == nullptr)) {
          /* This tile CAN be used as a spawning tile.*/
          /*int old_y = next_unit->getY();
           int old_x = next_unit->getX();
           if (old_y < tiles_y && old_x < tiles_x &&
           old_y >= 0 && old_x >= 0) {
           unit_board[old_y][old_x] = nullptr;
           }*/
          next_unit->setX(col_scan);
          next_unit->setY(row_scan);
          unit_board[row_scan][col_scan] = next_unit;
          can_place = true;
          col_scan = (-1);
          row_scan = (-1);
        }
      }
    }
    if (!can_place) {
      fprintf(stderr, "[ERR] -- not enough spaces to realloc");
      fprintf(stderr, "ate the units on the board!!\n");
      return -1;
    }
  }
  return 0;
}

bool TacticalMapBattle::has_moved(Unit *ele) const {
  return (moved_set.find(ele) != moved_set.end());
}

/* Precondition: turn has value 0 or 1.
 * Postcondition: if it is the player turn: make one enemy unit move in a
 *        random, valid direction.
 *   if it is the enemy turn: have the enemy make a judgement about the
 *   number of player units
 *   (encapsulated in the EnemyAI class) and then move accordingly. */
void TacticalMapBattle::process_one_ai(int turn) {
  /*      int choice = rand() % enemy_units.size();*/
  if (turn == 0) {
    for (size_t e_ind=0; e_ind < player_units.size(); e_ind ++) {
      Unit *active_unit = player_units.at(e_ind);
      /* call the prng ... */
      int move_choice = (generator() % 4);
      /*        int move_choice = (rand() % 4);*/
      if (move_choice == 0) {
        int y_ind = (active_unit->getY() - 1);
        int x_ind = active_unit->getX();
        if (y_ind < 0) {
          /* not a valid move */
        } else if (terrain_board[y_ind][x_ind] == 0) {
          /* no need to process it */
        } else if (unit_board[y_ind][x_ind] == nullptr) {
          /*              SDL_SetRenderTarget(displayTMB, layer2);*/
          
          SDL_SetRenderTarget(displayTMB, layer2);
          SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
          SDL_RenderClear(displayTMB);
          SDL_SetRenderTarget(displayTMB, nullptr);
          
          unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
          unit_board[y_ind][x_ind] = active_unit;
          active_unit->setX(x_ind);
          active_unit->setY(y_ind);
        }
      } else if (move_choice == 1) {
        int y_ind = active_unit->getY();
        int x_ind = active_unit->getX()+1;
        if ((x_ind >= tiles_x) || (terrain_board[y_ind][x_ind] == 0)) {
          /* not a valid move */
          /* no need to process it */
        } else if (unit_board[y_ind][x_ind] == nullptr) {
          /*              SDL_SetRenderTarget(displayTMB, layer2);*/
          
          SDL_SetRenderTarget(displayTMB, layer2);
          SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
          SDL_RenderFillRect(displayTMB, nullptr);
          SDL_SetRenderTarget(displayTMB, nullptr);
          
          unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
          unit_board[y_ind][x_ind] = active_unit;
          active_unit->setX(x_ind);
          active_unit->setY(y_ind);
        }
      } else if (move_choice == 2) {
        int y_ind = active_unit->getY() + 1;
        int x_ind = active_unit->getX();
        if ((y_ind >= tiles_y) || (terrain_board[y_ind][x_ind] == 0)) {
          /* not a valid move */
          /* no need to process it */
        } else if (unit_board[y_ind][x_ind] == nullptr) {
          SDL_SetRenderTarget(displayTMB, layer2);
          SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
          SDL_RenderFillRect(displayTMB, nullptr); // ;
          SDL_SetRenderTarget(displayTMB, nullptr);
          
          unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
          unit_board[y_ind][x_ind] = active_unit;
          active_unit->setX(x_ind);
          active_unit->setY(y_ind);
        }
      } else if (move_choice == 3) {
        int y_ind = active_unit->getY();
        int x_ind = active_unit->getX()-1;
        if ((x_ind < 0) || terrain_board[y_ind][x_ind] == 0) {
          /* no need to process it */
        } else if (unit_board[y_ind][x_ind] == nullptr) {
          SDL_SetRenderTarget(displayTMB, layer2);
          SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
          SDL_RenderFillRect(displayTMB, nullptr); // ;
          SDL_SetRenderTarget(displayTMB, nullptr);
          
          unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
          unit_board[y_ind][x_ind] = active_unit;
          active_unit->setX(x_ind);
          active_unit->setY(y_ind);
        }
      } /*else { }*/
      
      if (can_attack(active_unit)) {
        /* randomly attack a player unit */
        int choice = generator() % (in_range.size());
        Unit *tar = in_range.at(choice);
        int return_val = TacticalMapBattle::attack(active_unit, tar,
            active_unit->getWeapons()[0],
            terrain_board[tar->getY()][tar->getX()], false);
        if (return_val) {
          std::cerr << "[ERR] Attack returned error " << return_val << std::endl;
        }/* else {
          std::cout << "[OK] Attack processed correctly." << std::endl;;
        }*/
        
        if (! tar->isUp()) {
          std::cout << "ENEMY unit debilitated" << std::endl;
          unit_board[tar->getY()][tar->getX()] = nullptr;
          unsigned int search_ind = 0;
          unsigned int length = enemy_units.size();
          while (search_ind < enemy_units.size()) {
            if (enemy_units.at(search_ind) == tar) {
              /*                  Unit *test = nullptr;*/
              debilitated_enemies.push_back(enemy_units.at(search_ind));
              enemy_units.erase(enemy_units.begin()+search_ind);
              /* TODO also erase from the Settlement army list,
               * IF applicable. */
              
              /*                  test = player_units.at(search_ind);
               printf("test ptr (expected nullptr), got %p\n", test);*/
              if (checkForMusicTransition()) {
                Mix_FadeOutMusic(50);
                Mix_FadeInMusic(themes.at(musicPhase), -1, 50);
              }
              search_ind = length;
            }
            search_ind ++;
          }
          /*              printf("Player unit vector size went from %u to %u\n", length, player_units.size());*/
          /*              delete tar;*/
          tar = nullptr;
        }
      }/* else {
        std::cout << "Player Unit cannot attack." << std::endl;
        }*/
    }
    setTurnPhase(1);
  } else if (turn == 1) {
    for (size_t e_ind=0; e_ind < enemy_units.size(); e_ind ++) {
      /* TODO work on enemy AI */
      Unit *active_unit = enemy_units.at(e_ind);
      EnemyAI *mind = new EnemyAI(unit_board, terrain_board, active_unit, tiles_x, tiles_y);
      EnemyMove *res = mind->decide();
      /*          localLogger.log(res.toString(), true);*/
      /*          Entity *recvDest = res->getDestination();
       Entity *recvTar = res->getTarget();*/
      int recv_act = res->getAction();
      if (res->getDestination() == nullptr) {
        /* Now execute the EnemyMove action */
        /*          Entity *recvDest = res->getDestination();
         Entity *recvTar = res->getTarget();*/
        std::cerr << "Error -- destination Point is null. Treating this as \"no movement\"" << std::endl;
      } else {
        int recv_x = (int)(res->getDestination()->getX());
        int recv_y = (int)(res->getDestination()->getY());
        bool legal_move = false;/* = moveEnemy(active_unit, recvX, recvY);*/
        
        if (recv_x < tiles_x && recv_y < tiles_y && recv_x >= 0 &&
            recv_y >= 0 && terrain_board[recv_y][recv_x]) {
          if (unit_board[recv_y][recv_x] == nullptr) {
            legal_move = true;
          } else if (unit_board[recv_y][recv_x] == active_unit) {
            legal_move = true;
          }
          if (legal_move) {
            // alters the characters position
            unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
            active_unit->move(recv_x, recv_y);
            unit_board[recv_y][recv_x] = active_unit;
          }
          //  private bool moveTest(Unit selected, int x, int y) {
          //    if(x >= tilesX || y >= tilesY || x < 0 || y < 0) {
          //      return false;
          //    }
          //    if (gameBoard[y][x] != null) {
          //      if (gameBoard[y][x] != highlighted) {
          //        return false;
          //      }
          //    }
          //    if ((0 == terrainMap[y][x]) ||
          //        (((terrainImage[1].getRGB(x*TILE_SIZE, y*TILE_SIZE)) + 1387614848) != 0)) {
          //      return false;
          //    }
          //    return true;
          //  }
          /* BACK TO C++ */
          /*              fprintf(stdout, "Just added the ENEMY unit to the moved array\n");*/
        }/* else {
          localLogger.log("ERROR IN MOVING ENEMY "
          + active_unit.toString() + " TO THE LOCATION (" +
          recvX + ", " + recvY + ")", true);
          }*/
      }
      delete res;
      delete mind;
      /*          set_moved(active_unit);*/
      /* ***
       * Leaving some space here...
       * so that we remember to deal with more complex actions,
       * like giving / using an item, etc. */
      switch (recv_act) {
        case 0:
          /*              localLogger.log("Enemy unit " + enemyInd + " will wait.", true);*/
          break;
        case 1:
          
          break;
        case 2:
          
          break;
        case 3:
          
          break;
        case 4:
          
          break;
        case 5:
          
          break;
        case 6:
          
          break;
        case 7:
          
          break;
        default:
          
          break;
      }
      /* primitive approach */
      /* Move in one direction, one unit. Yea pretty pathetic, I know.
       * call the prng ... */
      /*          int move_choice = (generator() % 4);
       if (move_choice == 0) {
       int y_ind = (active_unit->getY() - 1);
       int x_ind = active_unit->getX();
       if (y_ind < 0) {
       / * not a valid move * /
       } else if (terrain_board[y_ind][x_ind] == 0) {
       / * no need to process it * /
       } else if (unit_board[y_ind][x_ind] == nullptr) {
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_RenderFillRect(displayTMB, nullptr); // ;
       SDL_SetRenderTarget(displayTMB, nullptr);
       
       unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
       unit_board[y_ind][x_ind] = active_unit;
       active_unit->setX(x_ind);
       active_unit->setY(y_ind);
       }
       } else if (move_choice == 1) {
       int y_ind = active_unit->getY();
       int x_ind = active_unit->getX()+1;
       if ((x_ind >= tiles_x) || (terrain_board[y_ind][x_ind] == 0)) {
       / * not a valid move - no need to process it * /
       } else if (unit_board[y_ind][x_ind] == nullptr) {
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_RenderFillRect(displayTMB, nullptr); // ;
       SDL_SetRenderTarget(displayTMB, nullptr);
       
       unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
       unit_board[y_ind][x_ind] = active_unit;
       active_unit->setX(x_ind);
       active_unit->setY(y_ind);
       }
       } else if (move_choice == 2) {
       int y_ind = active_unit->getY() + 1;
       int x_ind = active_unit->getX();
       if ((y_ind >= tiles_y) || (terrain_board[y_ind][x_ind] == 0)) {
       / * not a valid move, no need to process it * /
       } else if (unit_board[y_ind][x_ind] == nullptr) {
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_RenderFillRect(displayTMB, nullptr); // ;
       SDL_SetRenderTarget(displayTMB, nullptr);
       
       unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
       unit_board[y_ind][x_ind] = active_unit;
       active_unit->setX(x_ind);
       active_unit->setY(y_ind);
       }
       } else if (move_choice == 3) {
       int y_ind = active_unit->getY();
       int x_ind = active_unit->getX()-1;
       if ((x_ind < 0) || terrain_board[y_ind][x_ind] == 0) {
       / * no need to process it * /
       } else if (unit_board[y_ind][x_ind] == nullptr) {
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderTarget(displayTMB, layer2);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_SetRenderDrawColor(displayTMB, 2, 2, 2, 1);
       SDL_RenderFillRect(displayTMB, nullptr); // ;
       SDL_SetRenderTarget(displayTMB, nullptr);
       
       unit_board[active_unit->getY()][active_unit->getX()] = nullptr;
       unit_board[y_ind][x_ind] = active_unit;
       active_unit->setX(x_ind);
       active_unit->setY(y_ind);
       }
       } / *else { }*/
      /* The rest I can keep: if the enemy unit can attack,
       * why not try simply attacking for now? ;) */
      
      if (can_attack(active_unit)) {
        /* randomly attack a player unit */
        int choice = (generator() % (in_range.size()));
        Unit *tar = in_range.at(choice);
        int return_val = TacticalMapBattle::attack(active_unit, tar,
            active_unit->getWeapons()[0],
            terrain_board[tar->getY()][tar->getX()], false);
        if (return_val) {
          std::cerr << "[ERR] Attack returned error " << return_val << std::endl;
        }/* else {
          std::cout << "[OK] Attack processed correctly." << std::endl;;
          }*/
        
        if (! tar->isUp()) {
          std::cout << "PLAYER UNIT debilitated" << std::endl;
          unit_board[tar->getY()][tar->getX()] = nullptr;
          /*            delete chosen;*/
          unsigned int search_ind = 0;
          unsigned int length = player_units.size();
          while (search_ind < player_units.size()) {
            if (player_units.at(search_ind) == tar) {
              /*                Unit *test = nullptr;*/
              debilitated_players.push_back(player_units.at(search_ind));
              player_units.erase(player_units.begin()+search_ind);
              /*                test = player_units.at(search_ind);
               printf("test ptr (expected nullptr), got %p\n", test);*/
              search_ind = length;
            }
            search_ind ++;
          }
          /*              printf("Player unit vector size went from %u to %u\n", length, player_units.size());*/
          /*            chosen = nullptr;*/
        }
      }/* else {
        std::cout << "Enemy Unit cannot attack." << std::endl;
        }*/
    }
    setTurnPhase(0);
  }
}

void TacticalMapBattle::getFriendliesInRange(int xCoord, int yCoord, const std::array<Weapon*, WEAPON_CAPACITY> &weapons) {
  for (size_t friend_ind = 0; friend_ind < player_units.size(); friend_ind++) {
    Unit *next_player = player_units.at(friend_ind);
    for (size_t weapoInd = 0; weapoInd < WEAPON_CAPACITY; weapoInd++) {
      int delta = Entity::get_distance(xCoord, yCoord, next_player->getX(), next_player->getY());
      if (weapons[weapoInd] != nullptr) {
        if (weapons[weapoInd]->getMinRange() <= delta && delta <= weapons[weapoInd]->getMaxRange()) {
          in_range.push_back(next_player);
        }
      }
    }
  }
}

void TacticalMapBattle::getEnemiesInRange(/*int maxR, int minR, */int xCoord, int yCoord,
                                          const std::array<Weapon*, WEAPON_CAPACITY> &weapons/*, int xSrc, int ySrc*/) {
  /* TODO divide into cases
   * case 1: if range == 1, just make four comparisons and leave.
   * case 2: if range > 1 but range is small relative to the number of enemies,
   *        use the region-sweep check commented below.
   * case 3: otherwise, simply check each enemy and check if it is in range.
   *        (use the approach that isn't commented :D)
   */
  /* If there are fewer enemies compared to the range,
   * this method is more efficient:
   * */
  
  for (size_t enemy_ind = 0; enemy_ind < enemy_units.size(); enemy_ind++) {
    Unit *next_enemy = enemy_units.at(enemy_ind);
    for (size_t weapoInd = 0; weapoInd < WEAPON_CAPACITY; weapoInd++) {
      int delta = Entity::get_distance(xCoord, yCoord, next_enemy->getX(), next_enemy->getY());
      if (weapons[weapoInd] != nullptr) {
        if (weapons[weapoInd]->getMinRange() <= delta &&
            delta <= weapons[weapoInd]->getMaxRange()) {
          in_range.push_back(next_enemy);
        }
      }
    }
  }
  /*
   int ind = 1;
   //      NE
   printf("\tNORTHEAST\n");
   while (ind <= range) {
   int i = 0;
   while (i < ind) {
   //           consider moving RIGHT `i` times and UP `n-i` times
   printf("\t\tValue of `i`: %d\n", i);
   int x_ind = (xCoord+i);
   int y_ind = (yCoord-ind+i);
   printf("\tNow checking (%d, %d)\n", x_ind, y_ind);
   if ((y_ind > 0) && (y_ind < tiles_y)
   && (x_ind > 0) && (x_ind < tiles_x)
   && (unit_board[y_ind][x_ind] != nullptr)
   && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
   //            int ulx = xCoord*tile_side_length;
   //            int uly = yCoord*tile_side_length;
   //            SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
   //            dframe->x = ulx;
   //            dframe->y = uly;
   //            dframe->w = (tile_side_length-1);
   //            dframe->h = (tile_side_length-1);
   //            SDL_RenderFillRect(displayTMB, dframe);
   in_range.push_back(unit_board[y_ind][x_ind]);
   }
   i ++;
   }
   ind ++;
   }
   ind = 1;
   
   //      SE
   printf("\tSOUTHEAST\n");
   while (ind <= range) {
   int i = 0;
   while (i < ind) {
   //           consider moving RIGHT `i` times and DOWN `n-i` times
   printf("\t\tValue of `i`: %d\n", i);
   int y_ind = (xCoord+i);
   int x_ind = (yCoord+ind-i);
   printf("\tNow checking (%d, %d)", x_ind, y_ind);
   if ((y_ind > 0) && (y_ind < tiles_y)
   && (x_ind > 0) && (x_ind < tiles_x)
   && (unit_board[y_ind][x_ind] != nullptr)
   && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
   //            int ulx = xCoord*tile_side_length;
   //            int uly = yCoord*tile_side_length;
   //            SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
   //            dframe->x = ulx;
   //            dframe->y = uly;
   //            dframe->w = (tile_side_length-1);
   //            dframe->h = (tile_side_length-1);
   //            SDL_RenderFillRect(displayTMB, dframe);
   in_range.push_back(unit_board[y_ind][x_ind]);
   printf(" - enemy unit detected!");
   }
   printf("\n");
   i ++;
   }
   ind ++;
   }
   ind = 1;
   
   //      SW
   printf("\tSOUTHWEST\n");
   while (ind <= range) {
   int i = 0;
   while (i < ind) {
   //           consider moving LEFT `i` times and DOWN `n-i` times
   printf("\t\tValue of `i`: %d\n", i);
   int x_ind = (xCoord-i);
   int y_ind = (yCoord+ind-i);
   printf("\tNow checking (%d, %d)", x_ind, y_ind);
   if ((y_ind > 0) && (y_ind < tiles_y)
   && (x_ind > 0) && (x_ind < tiles_x)
   && (unit_board[y_ind][x_ind] != nullptr)
   && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
   //            int ulx = xCoord*tile_side_length;
   //            int uly = yCoord*tile_side_length;
   //            SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
   //            dframe->x = ulx;
   //            dframe->y = uly;
   //            dframe->w = (tile_side_length-1);
   //            dframe->h = (tile_side_length-1);
   //            SDL_RenderFillRect(displayTMB, dframe);
   in_range.push_back(unit_board[y_ind][x_ind]);
   printf(" - enemy unit detected!");
   }
   printf("\n");
   i ++;
   }
   ind ++;
   }
   ind = 1;
   
   //      NW
   printf("\tNORTHWEST\n");
   while (ind <= range) {
   int i = 0;
   while (i < ind) {
   //           consider moving LEFT `i` times and UP `n-i` times
   printf("\t\tValue of `i`: %d\n", i);
   int y_ind = (yCoord-i);
   int x_ind = (xCoord-ind+i);
   //          printf("\tNow checking (%d, %d)\n", x_ind, y_ind);
   if ((y_ind > 0) && (y_ind < tiles_y)
   && (x_ind > 0) && (x_ind < tiles_x)
   && (unit_board[y_ind][x_ind] != nullptr)
   && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
   //            int ulx = xCoord*tile_side_length;
   //            int uly = yCoord*tile_side_length;
   //            SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
   //            dframe->x = ulx;
   //            dframe->y = uly;
   //            dframe->w = (tile_side_length-1);
   //            dframe->h = (tile_side_length-1);
   //            SDL_RenderFillRect(displayTMB, dframe);
   in_range.push_back(unit_board[y_ind][x_ind]);
   printf(" - enemy unit detected!");
   }
   printf("\n");
   i ++;
   }
   ind ++;
   }
   */
}

/***
 * A recursive method to identify all enemy units in range
 * and determine which, if any, weapons can reach the enemy from the player unit's 
 * current position.
 * *//*
void getEnemiesInRange(int range, int xCoord, int yCoord, Weapon **weapons,
      int xSrc, int ySrc) {
  if ((xCoord >= 0) && (xCoord < tiles_x) &&
  (yCoord >= 0) && (yCoord < tiles_y)) {
    Unit *next_unit = unit_board[yCoord][xCoord];
    if (range == 0) {
    / *          if (terrain_board[yCoord][xCoord] == 0) {
    } else* /
      if (next_unit == nullptr) {
    / * no-op... * /
    } else if (next_unit->getTeam() == 1) {
    / *            int ind;* /
        in_range.push_back(next_unit);
      }
    } else {
    / *          if (terrain_board[yCoord][xCoord] == 0) {
    // No-op
    } else * /
      if (next_unit == nullptr) {
        getEnemiesInRange(range-1, xCoord-1, yCoord, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord+1, yCoord, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord, yCoord-1, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord, yCoord+1, weapons, xSrc, ySrc);
      } else if (next_unit->getTeam() == 1) {
        in_range.push_back(next_unit);
        getEnemiesInRange(range-1, xCoord-1, yCoord, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord+1, yCoord, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord, yCoord-1, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord, yCoord+1, weapons, xSrc, ySrc);
      } else { 
        / *            localLogger.log("\tFunky case -- the unit is not nullptr, but its team, "+next_unit.getTeam()+" is not 1 [range="+range+"]", true);* /
        getEnemiesInRange(range-1, xCoord-1, yCoord, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord+1, yCoord, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord, yCoord-1, weapons, xSrc, ySrc);
        getEnemiesInRange(range-1, xCoord, yCoord+1, weapons, xSrc, ySrc);
      }
    }
  }
}*/

/* Postcondition: Returns a boolean,
 *  defining whether the GIVEN unit can travel to the 
 *  GIVEN terrain_board address.
 * For now, follow the  K.I.S.S. methodology. */
inline bool TacticalMapBattle::canMove(Unit *subject, int dx, int dy) const {
  return (unit_board[dy][dx] == nullptr || unit_board[dy][dx] == subject) &&
         (terrain_board[dy][dx] != 0);
}
inline bool TacticalMapBattle::canMove(int dx, int dy) const {
  return (unit_board[dy][dx] == nullptr) && (terrain_board[dy][dx] != 0);
}

/* A method to check whether the selected Player unit 
 * can attack from its current position...
 * Precondition: `o` (obj) represents a non-nullptr player Unit
 * Postcondition: 
 *   Updates the list of Units that are within attack range
 *   and then returns a bool representing whether that
 *   list is non-empty.  */
inline bool TacticalMapBattle::can_attack(Unit *o) {
  int xVal = o->getX();
  int yVal = o->getY();
  int curMaxRange = -1;
  int curMinRange = tiles_x+tiles_y;
  int numWeapons = 0;
  auto const& allWeaponChoices = o->getWeapons(); 
  
  /*      Weapon *allWeaponChoices[WEAPON_CAPACITY];*/
  /* Finding max range */
  /* In a future release,
   * this processing may be done just once per unit and stored somewhere in memory */
  for (int choice = 0; choice < WEAPON_CAPACITY; choice ++) {
    Weapon *nextWeap = allWeaponChoices[choice];
    if (nextWeap != nullptr) {
      numWeapons ++;
      int nextWeapRange = nextWeap->getMaxRange();
      if (nextWeapRange > curMaxRange) {
        curMaxRange = nextWeapRange;
      }
      nextWeapRange = nextWeap->getMinRange();
      if (curMinRange > nextWeapRange) {
        curMinRange = nextWeapRange;
      }
    }
  }
  in_range.clear();
  /*      validWeaponsPerUnit.clear();*/
  /*      fprintf(stdout, ("numWeapons == "+numWeapons+"; maxRange == "+curMaxRange, true);*/
  if (numWeapons < 1) {
    printf("Unit has no weapons with which to attack!\n");
    return false;
  } else {/*if (numWeapons > 1) { */// oh boy. have to process all these possible weapons.
    in_range.clear();
    if (o->getTeam() == 0) {
      /*in_range = */getEnemiesInRange(/*curMaxRange, curMinRange, */xVal, yVal, allWeaponChoices/*, o->getX(), o->getY()*/);
      /*        fprintf(stdout, "in_range: "+in_range.toString());*/
      /*        fprintf(stdout, "hashMap for each enemy in range: "+validWeaponsPerUnit.toString());*/
      return (in_range.size() > 0);
      /*      } else { */// only one weapon! yay
      /*        fprintf(stdout, ("weapon chosen: "+(allWeaponChoices[0]!=nullptr), true);*/
      /*in_range = *//*getEnemiesInRangeOneWeapon(curMaxRange, xVal, yVal, allWeaponChoices[0], o->getX(), o->getY());*/
      /*        fprintf(stdout, ("Should have justed printed out a bunch of indented info about surrounding units", true);*/
      /*        fprintf(stdout, ("in_range: "+in_range.toString(), true);*/
      /*        fprintf(stdout, ("hashMap for each enemy in range: "+validWeaponsPerUnit.toString(), true);*/
      /*        return (in_range.size() > 0);*/
    } else if (o->getTeam() == 1) {
      /* */
      getFriendliesInRange(xVal, yVal, allWeaponChoices);
      return (in_range.size() > 0);
    } else {
      return false;
    }
  }
}

bool TacticalMapBattle::can_rescue(Unit *o) const {
  if (o->getHeld() != nullptr) {
    return false;
  }
  int xVal = o->getX();
  int yVal = o->getY();
  if ((xVal > 0) &&
      (unit_board[yVal][xVal-1] != nullptr) &&
      (unit_board[yVal][xVal-1]->getTeam() == 0) &&
      (unit_board[yVal][xVal-1]->getHeld() == nullptr)) {
    return true;
  }
  if ((xVal < (tiles_x-1)) &&
      (unit_board[yVal][xVal+1] != nullptr) &&
      (unit_board[yVal][xVal+1]->getTeam() == 0) &&
      (unit_board[yVal][xVal+1]->getHeld() == nullptr)) {
    return true;
  }
  if ((yVal > 0) &&
      (unit_board[yVal-1][xVal] != nullptr) &&
      (unit_board[yVal-1][xVal]->getTeam() == 0) &&
      (unit_board[yVal-1][xVal]->getHeld() == nullptr)) {
    return true;
  }
  if ((yVal < (tiles_y-1)) &&
      (unit_board[yVal+1][xVal] != nullptr) &&
      (unit_board[yVal+1][xVal]->getTeam() == 0) &&
      (unit_board[yVal+1][xVal]->getHeld() == nullptr)) {
    return true;
  }
  return false;
}

bool TacticalMapBattle::can_drop(Unit *o) const {
  int xVal = o->getX();
  int yVal = o->getY();
  if (o->getHeld() == nullptr) {
    return false;
  }
  if ((xVal > 0) &&
      (unit_board[yVal][xVal-1] == nullptr) &&
      (terrain_board[yVal][xVal-1] != 0)) {
    return true;
  }
  if ((xVal < (tiles_x-1)) &&
      (unit_board[yVal][xVal+1] == nullptr) &&
      (terrain_board[yVal][xVal+1] != 0)) {
    return true;
  }
  if ((yVal > 0) &&
      (unit_board[yVal-1][xVal] == nullptr) &&
      (terrain_board[yVal-1][xVal] != 0)) {
    return true;
  }
  if ((yVal < (tiles_y-1)) &&
      (unit_board[yVal+1][xVal] == nullptr) &&
      (terrain_board[yVal+1][xVal] != 0)) {
    return true;
  }
  return false;
}

bool TacticalMapBattle::can_trade(Unit *o) const {
  int xVal = o->getX();
  int yVal = o->getY();
  if ((xVal > 0) &&
      (unit_board[yVal][xVal-1] != nullptr) &&
      (unit_board[yVal][xVal-1]->getTeam() == 0)) {
    return true;
  }
  if ((xVal < (tiles_x-1)) &&
      (unit_board[yVal][xVal+1] != nullptr) &&
      (unit_board[yVal][xVal+1]->getTeam() == 0)) {
    return true;
  }
  if ((yVal > 0) &&
      (unit_board[yVal-1][xVal] != nullptr) &&
      (unit_board[yVal-1][xVal]->getTeam() == 0)) {
    return true;
  }
  if ((yVal < (tiles_y-1)) &&
      (unit_board[yVal+1][xVal] != nullptr) &&
      (unit_board[yVal+1][xVal]->getTeam() == 0)) {
    return true;
  }
  return false;
}

bool TacticalMapBattle::can_pass(Unit *o) const {
  int xVal = o->getX();
  int yVal = o->getY();
  if (o->getHeld() == nullptr) {
    return false;
  }
  if ((xVal > 0) &&
      (unit_board[yVal][xVal-1] != nullptr) &&
      (unit_board[yVal][xVal-1]->getTeam() == 0) &&
      (unit_board[yVal][xVal-1]->getHeld() == nullptr)) {
    return true;
  }
  if ((xVal < (tiles_x-1)) &&
      (unit_board[yVal][xVal+1] != nullptr) &&
      (unit_board[yVal][xVal+1]->getTeam() == 0) &&
      ((unit_board[yVal][xVal+1]->getHeld() == nullptr))) {
    return true;
  }
  if ((yVal > 0) &&
      (unit_board[yVal-1][xVal] != nullptr) &&
      (unit_board[yVal-1][xVal]->getTeam() == 0) &&
      ((unit_board[yVal-1][xVal]->getHeld() == nullptr))) {
    return true;
  }
  if ((yVal < (tiles_y-1)) &&
      (unit_board[yVal+1][xVal] != nullptr) &&
      (unit_board[yVal+1][xVal]->getTeam() == 0) &&
      (unit_board[yVal+1][xVal]->getHeld() == nullptr)) {
    return true;
  }
  return false;
}

/* TODO -- METHOD UNDER CONSTRUCTION...
 * MANY ERRORS HERE
 * Especially with the calculation of upper_x etc. */
//int TacticalMapBattle::show_move_range_flying(int speed, int src_x, int src_y) {
//  int ind_y;
//  int ind_x;
//  int start_y;
//  int upper_y;
//  /* Initialize ind_y */
//  if (src_y < speed) {
//    ind_y = 0;
//  } else {
//    ind_y = src_y - speed;
//  }
//  
//  /* Initialize the upper bound for y */
//  if ((src_y+speed+1) > tiles_y) {
//    upper_y = tiles_y-1;
//  } else {
//    upper_y = src_y+speed;
//  }
//  
//  while (ind_y <= upper_y) {
//    int upper_x;
//    /* Initialize ind_x */
//    if (src_x < speed) {
//      ind_x = 0;
//    } else {
//      ind_x = src_x-speed;
//    }
//    if (src_x+speed-ind_y+1 > tiles_x) {
//      /*          upper_x = ;*/
//      upper_x = tiles_x-1;
//    } else {
//      /*          upper_x = src_x+speed;*/
//      upper_x = src_x - ind_y + start_y;
//    }
//    
//    while (ind_x <= upper_x) {
//      /* Visit the terrain board at [ind_y][ind_x]
//       * this would be doing many things,
//       * but it should be O(1) time.  */
//      if (terrain_board[ind_y][ind_x] == 0) {
//        // No-op
//      } else if (unit_board[ind_y][ind_x] == nullptr) {
//        int ulx = ind_x * tile_side_length;
//        int uly = ind_y * tile_side_length;
//        SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
//        dframe->x = ulx;
//        dframe->y = uly;
//        dframe->w = tile_side_length-1;
//        dframe->h = tile_side_length-1;
//        SDL_RenderFillRect(displayTMB, dframe);
//      }
//      ind_x ++;
//    }
//    ind_y ++;
//  }
//  return 0;
//}

/* Note: this current algorithm is perfectly accurate, but 
 * unfortunately inefficient (can take multiple seconds to finish
 * on maps with large tiles).
 *
 * Update: this is only noticeable when the tile size is large
 *  (so, when the map size is large relative to the number of tiles).
 *
 * So developer(s): be sure to look for something better
 *
 * One attempted solution is given above, in show_move_range_flying,
 * but that is still in development and assumes the unit can
 * fly over bodies of water.
 **/
/* TODO -- UNSTABLE -- IN TESTING -- implementing dynamic programming
 *  approach; this should be a more efficient approach,
 *  in that it studies each map tile AT MOST ONCE;
 *  the primitive recursive method checks the same
 *  tile many times. */
void TacticalMapBattle::show_move_range_dyn(int speed, int xCoord, int yCoord) {
  /*        std::cout << "SPEED: " << speed << std::endl;*/
  int *store = nullptr;
  if (xCoord+1 < tiles_x &&
      terrain_board[yCoord][xCoord+1] && 
      ((unit_board[yCoord][xCoord+1] == nullptr) ||
       (unit_board[yCoord][xCoord+1]->getTeam() == 0))) {
        int y_bound = (yCoord - speed) < 0 ? yCoord+1 : speed;
        int x_bound = (xCoord + speed) >= tiles_x ? tiles_x-xCoord-1 : speed;
        store = (int *) calloc(y_bound, x_bound*sizeof(int));
        int cache = y_bound-1;
        /* Visit this tile: in this case,
         * color the square here, and set the first `store` flag */
        int ulx = (xCoord+1)*tile_side_length;
        int uly = yCoord*tile_side_length;
        if (unit_board[yCoord][xCoord+1] == nullptr) {
          /*                printf("[DEBUG]\tunit_board is nullptr at %d, %d so we're painting that green!\n", xCoord+1, yCoord);*/
          SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
          dframe->x = ulx;
          dframe->y = uly;
          dframe->w = (ulx+tile_side_length-1)-(ulx);
          dframe->h = (uly+tile_side_length-1)-(uly);
          SDL_RenderFillRect(displayTMB, dframe);
        }
        store[x_bound*(y_bound-1)] = 1;
        
        /* Consider all tiles DUE NORTH and DUE EAST of this tile,
         * to use as base cases. */
        int lastSpd = speed-1;
        for (int incr = y_bound-2; incr >= 0; incr --) {
          int terra = terrain_board[yCoord+incr-y_bound+1][xCoord+1]; // NORTH
          /*                printf("[DEBUG1] yCoord+incr-y_bound+1 = %d + %d - %d + 1 = %d"\
           *                "and terrain is %d\n", yCoord, incr, y_bound, yCoord+incr-y_bound+1, terra);*/
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord+incr-y_bound+1][xCoord+1] == nullptr) {
              ulx = (xCoord+1)*tile_side_length;
              uly = tile_side_length*(yCoord+incr-y_bound+1);
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = (ulx+tile_side_length-1)-(ulx);
              dframe->h = (uly+tile_side_length-1)-(uly);
              SDL_RenderFillRect(displayTMB, dframe);
              /*                        printf("[DEBUG NE 1]\t Now coloring %d, %d green...\n",
               xCoord+1, yCoord+incr-y_bound+1);*/
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else if (unit_board[yCoord+incr-y_bound+1][xCoord+1]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else {
              lastSpd = 0;
              store[x_bound*incr] = 0;
              incr = -1;
            }
          } else {
            lastSpd = 0;
            store[x_bound*incr] = 0;
            incr = -1;
          }
        }
        lastSpd = speed;
        for (int incr = 1; incr < x_bound; incr ++) {
          int terra = terrain_board[yCoord][xCoord+incr+1]; // EAST
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord][xCoord+incr+1] == nullptr) {
              ulx = (xCoord+incr+1)*tile_side_length;
              uly = tile_side_length*yCoord;
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = (ulx+tile_side_length-1)-(ulx);
              dframe->h = (uly+tile_side_length-1)-(uly);
              SDL_RenderFillRect(displayTMB, dframe);
              /*                        printf("[DEBUG NE 2]\t Now coloring %d, %d green...\n",
               xCoord+incr+1, yCoord);*/
              
              lastSpd -= MOVE_COST[terra];
              store[x_bound*cache+incr] = lastSpd; // DO NOT DELETE!!
            } else if (unit_board[yCoord][xCoord+incr+1]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[x_bound*cache+incr] = lastSpd; // DO NOT DELETE!!
            } else {
              lastSpd = 0;
              store[x_bound*cache+incr] = 0;
              incr = x_bound;
            }
          } else {
            lastSpd = 0;
            store[x_bound*cache+incr] = 0;
            incr = x_bound;
          }
        }
        
        /* And then consider all tiles northeast of this tile,
         * using dynamic programming */
        for (int row = y_bound-2; row >= 0; row --) { // North-
          for (int col=1; col < x_bound; col ++) {  // east
            int terra = terrain_board[yCoord+row-y_bound+1][xCoord+col+1];
            /*                    int terra1 = terrain_board[yCoord+row-y_bound+1][xCoord+1];*/
            /*                    int terra2 = terrain_board[yCoord][xCoord+col];*/
            int st1 = store[x_bound*(row+1)+col];
            int st2 = store[x_bound*(row)+col-1];
            /*                    printf("[DEBUG]\tx_bound*(row+1)+col=%d*%d+%d=%d store[%d]=%d\n",
             x_bound, row+1, col, x_bound*(row+1)+col, x_bound*(row+1)+col, st1);
             printf("[DEBUG]\tx_bound*(row)+col-1=%d*%d+%d=%d store[%d]=%d\n",
             x_bound, row, col-1, (x_bound*row)+col-1, (x_bound*row)+col-1, st2);*/
            if (terra>0 && ((st1 >= MOVE_COST[terra]) ||
                            (st2 >= MOVE_COST[terra]))) {
              if (unit_board[yCoord+row-y_bound+1][xCoord+col+1] == nullptr) {
                if (st1 > st2) {
                  store[(x_bound*row)+col] = st1-MOVE_COST[terra];
                } else {
                  store[(x_bound*row)+col] = st2-MOVE_COST[terra];
                }
                /*                            printf("[DEBUG NE 3]\t Now coloring %d, %d green...\n",
                 xCoord+col+1, yCoord+row-y_bound+1);*/
                ulx = (xCoord+col+1)*tile_side_length;
                uly = (yCoord+row-y_bound+1)*tile_side_length;
                SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
                dframe->x = ulx;
                dframe->y = uly;
                dframe->w = (ulx+tile_side_length-1)-(ulx);
                dframe->h = (uly+tile_side_length-1)-(uly);
                SDL_RenderFillRect(displayTMB, dframe);
              } else if (unit_board[yCoord+row-y_bound+1][xCoord+col+1]->getTeam() == 0) {
                if (st1 > st2) {
                  store[(x_bound*row)+col] = st1 - MOVE_COST[terra];
                } else {
                  store[(x_bound*row)+col] = st2 - MOVE_COST[terra];
                }
              } else {
                store[(x_bound*row)+col] = 0;
              }
            } else {
              store[(x_bound*row)+col] = 0;
            }
          }
        }
        free(store);
        store = nullptr;
      }
  if (yCoord+1 < tiles_y &&
      terrain_board[yCoord+1][xCoord] &&
      ((unit_board[yCoord+1][xCoord] == nullptr) ||
       (unit_board[yCoord+1][xCoord]->getTeam() == 0))) {
        int y_bound = (yCoord + speed) >= tiles_y ? tiles_y-yCoord : speed;
        int x_bound = (xCoord + speed) >= tiles_x ? tiles_x-xCoord : speed;
        int lastSpd = speed-1;
        int cache = y_bound-1;
        /* Visit this tile: in this case, color the square here */
        int ulx = xCoord*tile_side_length;
        int uly = (yCoord+1)*tile_side_length;
        SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
        dframe->x = ulx;
        dframe->y = uly;
        dframe->w = (ulx+tile_side_length-1)-(ulx);
        dframe->h = (uly+tile_side_length-1)-(uly);
        SDL_RenderFillRect(displayTMB, dframe);
        
        /* Consider all tiles DUE SOUTH and DUE EAST of this tile,
         * to use as base cases.  */
        store = (int *) calloc(y_bound, x_bound*sizeof(int));
        store[0] = 1;
        for (int incr = 1; incr < x_bound; incr ++) { // EAST
          int terra = terrain_board[yCoord+1][xCoord+incr];
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord+1][xCoord+incr] == nullptr) {
              ulx = (xCoord+incr)*tile_side_length;
              uly = (yCoord+1)*tile_side_length;
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = tile_side_length-1;
              dframe->h = tile_side_length-1;
              SDL_RenderFillRect(displayTMB, dframe);
              /*                        printf("[DEBUG SE 1]\t Now coloring %d, %d green...\n", xCoord+incr, yCoord+1);*/
              
              lastSpd -= MOVE_COST[terra];
              store[incr] = lastSpd; // DO NOT DELETE!!!!
            } else if (unit_board[yCoord+1][xCoord+incr]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[incr] = lastSpd; // DO NOT DELETE!!!!
            } else {
              lastSpd = 0;
              store[incr] = 0;
              incr = x_bound;
            }
          } else {
            lastSpd = 0;
            store[incr] = 0;
            incr = x_bound;
          }
        }
        lastSpd = speed;
        for (int incr = 2; incr < y_bound-1; incr ++) {
          int terra = terrain_board[yCoord+incr][xCoord];
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord+incr][xCoord] == nullptr) {
              ulx = (xCoord)*tile_side_length;
              uly = (yCoord+incr)*tile_side_length;
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = tile_side_length-1;
              dframe->h = tile_side_length-1;
              SDL_RenderFillRect(displayTMB, dframe);
              /*                        printf("[DEBUG SE 2]\t Now coloring %d, %d green...\n", xCoord, yCoord+incr);*/
              
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else if (unit_board[yCoord+incr][xCoord]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else {
              lastSpd = 0;
              store[x_bound*cache+incr] = 0;
              incr = x_bound;
            }
          } else {
            lastSpd = 0;
            store[x_bound*cache+incr] = 0;
            incr = x_bound;
          }
        }
        lastSpd = speed;
        //            /* And then consider all tiles southeast of this tile,
        //             * using dynamic programming */
        //            for (int row = 1; row < y_bound; row ++) {
        //                for (int col = 1; col < x_bound; col ++) {
        //                    if (terrain_board[yCoord+row][xCoord+col] && store[x_bound*(row-1)+col] & store[x_bound*(row+1)+col]) {
        //                        ulx = (xCoord+col)*tile_side_length;
        //                        uly = (yCoord+row)*tile_side_length;
        //                        SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
        //                        dframe->x = ulx;
        //                        dframe->y = uly;
        //                        dframe->w = (tile_side_length-1);
        //                        dframe->h = (tile_side_length-1);
        //                        SDL_RenderFillRect(displayTMB, dframe);
        //                        store[x_bound*row+col] = 1;
        //                    }
        //                }
        //            }
        free(store);
        store = nullptr;
      }
  if (xCoord >= 1 &&
      terrain_board[yCoord][xCoord-1] &&
      ((unit_board[yCoord][xCoord-1] == nullptr) ||
       (unit_board[yCoord][xCoord-1]->getTeam() == 0))) {
        int y_bound = (yCoord + speed) >= tiles_y ? tiles_y-yCoord : speed;
        int x_bound = (xCoord - speed) < 0 ? xCoord : speed;
        store = (int *) calloc(y_bound, x_bound*sizeof(int));
        /*            printf("[DEBUG] store is %dx%d\n", y_bound, x_bound);*/
        /* Visit this tile: in this case,
         * color the square here, and set the first `store` flag */
        int ulx = (xCoord-1)*tile_side_length;
        int uly = yCoord*tile_side_length;
        if (unit_board[yCoord][xCoord-1] == nullptr) {
          printf("[DEBUG SW 0]\tunit_board is nullptr at %d, %d so we're painting that green!\n", xCoord+1, yCoord);
          SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
          dframe->x = ulx;
          dframe->y = uly;
          dframe->w = tile_side_length-1;
          dframe->h = tile_side_length-1;
          SDL_RenderFillRect(displayTMB, dframe);
        }
        store[x_bound-1] = 1;
        int lastSpd = speed-1;
        for (int incr = 1; incr < y_bound-1; incr ++) { // SOUTH
          int terra = terrain_board[yCoord+incr][xCoord-1];
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord+incr][xCoord-1] == nullptr) {
              ulx = (xCoord-1)*tile_side_length;
              uly = (yCoord+incr)*tile_side_length;
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = tile_side_length-1;
              dframe->h = tile_side_length-1;
              SDL_RenderFillRect(displayTMB, dframe);
              printf("[DEBUG SW 1]\t Now coloring %d, %d green...\n", xCoord-1, yCoord+incr);
              
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else if (unit_board[yCoord+incr][xCoord-1]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[x_bound*(incr-1)+x_bound-1] = lastSpd; // DO NOT DELETE!!!!
            } else {
              lastSpd = 0;
              store[x_bound*(incr-1)+x_bound-1] = 0;
              incr = x_bound;
            }
          } else {
            lastSpd = 0;
            store[x_bound*(incr-1)+x_bound-1] = 0;
            incr = y_bound;
          }
        }
        lastSpd = speed;
        for (int incr = 2; incr < x_bound; incr ++) { // WEST
          int terra = terrain_board[yCoord][xCoord-incr];
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord][xCoord-incr] == nullptr) {
              ulx = (xCoord-incr)*tile_side_length;
              uly = (yCoord)*tile_side_length;
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = tile_side_length-1;
              dframe->h = tile_side_length-1;
              SDL_RenderFillRect(displayTMB, dframe);
              printf("[DEBUG SW 2]\t Now coloring %d, %d green...\n", xCoord-incr, yCoord);
              
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else if (unit_board[yCoord][xCoord-incr]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[x_bound*(incr-1)+x_bound-1] = lastSpd; // DO NOT DELETE!!!!
            } else {
              lastSpd = 0;
              store[x_bound*(incr-1)+x_bound-1] = 0;
              incr = x_bound;
            }
          } else {
            lastSpd = 0;
            store[x_bound*(incr-1)+x_bound-1] = 0;
            incr = y_bound;
          }
        }
        free(store);
        store = nullptr;
      }
  if (yCoord >= 1 &&
      terrain_board[yCoord][xCoord-1] &&
      ((unit_board[yCoord][xCoord-1] == nullptr) ||
       (unit_board[yCoord][xCoord-1]->getTeam() == 0))) {
        int y_bound = (yCoord - speed) < 0 ? yCoord : speed;
        int x_bound = (xCoord - speed) < 0 ? xCoord : speed;
        /*            printf("[DEBUG] store is %dx%d\n", y_bound, x_bound);*/
        store = (int *) calloc(y_bound, x_bound*sizeof(int));
        /*            int cache = y_bound-1;*/
        /* Visit this tile: in this case,
         * color the square here, and set the first `store` flag */
        int ulx = xCoord*tile_side_length;
        int uly = (yCoord-1)*tile_side_length;
        if (unit_board[yCoord-1][xCoord] == nullptr) {
          printf("[DEBUG NW 0]\tunit_board is nullptr at %d, %d so we're painting that green!\n", xCoord+1, yCoord);
          SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
          dframe->x = ulx;
          dframe->y = uly;
          dframe->w = tile_side_length-1;
          dframe->h = tile_side_length-1;
          SDL_RenderFillRect(displayTMB, dframe);
        }
        store[x_bound*(y_bound-1) + (x_bound-1)] = 1;
        int lastSpd = speed-1;
        for (int incr = y_bound-2; incr >= 0; incr --) {
          int terra = terrain_board[yCoord+incr-y_bound+1][xCoord]; // NORTH
          /*                printf("[DEBUG1] yCoord+incr-y_bound+1 = %d + %d - %d + 1 = %d"\
           *                "and terrain is %d\n", yCoord, incr, y_bound, yCoord+incr-y_bound+1, terra);*/
          if (terra>0 && lastSpd>=MOVE_COST[terra]) {
            if (unit_board[yCoord+incr-y_bound+1][xCoord] == nullptr) {
              ulx = xCoord*tile_side_length;
              uly = tile_side_length*(yCoord+incr-y_bound+1);
              SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
              dframe->x = ulx;
              dframe->y = uly;
              dframe->w = tile_side_length-1;
              dframe->h = tile_side_length-1;
              SDL_RenderFillRect(displayTMB, dframe);
              printf("[DEBUG NW 1]\t Now coloring %d, %d green...\n",
                     xCoord+1, yCoord+incr-y_bound+1);
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else if (unit_board[yCoord+incr-y_bound+1][xCoord]->getTeam() == 0) {
              lastSpd -= MOVE_COST[terra];
              store[x_bound*incr] = lastSpd; // DO NOT DELETE!!!!
            } else {
              lastSpd = 0;
              store[x_bound*incr] = 0;
              incr = -1;
            }
          } else {
            lastSpd = 0;
            store[x_bound*incr] = 0;
            incr = -1;
          }
        }
        lastSpd = speed;
        
        free(store);
        store = nullptr;
      }
  printf("\t[DEBUG] EXITING the show_move_range_dyn function\n");
}

/* Decoupled movement range DISPLAY from internal computation;
 * i.e. one could have an internal array of ints or bools,
 * simply representing whether a unit CAN travel there,
 * and LATER write to the layer2 texture based on the finished array values.
 * The reason why this is superior is because it avoids the need for
 * a SINGLE read-write texture.
 * In order to use LockTexture() and GetRGBA(), the texture must
 * be STREAMING. But in order to actually use it as a target for rendering
 * things on top of it, it must be TARGET.
 * It CANNOT be both at the same time. */
void TacticalMapBattle::show_move_range(const int speed, const int xCoord, const int yCoord) {
  int lowerCol=xCoord - speed;
  if (lowerCol<0) lowerCol=0;
  int upperCol=xCoord+speed;
  if (upperCol >= tiles_x) upperCol=tiles_x-1;
  int lowerRow = yCoord-speed;
  if (lowerRow<0) lowerRow=0;
  int upperRow = yCoord+speed;
  if (upperRow >= tiles_y) upperRow=tiles_y-1;
  
  /* Allocate memory for the internal movement map.
   * a 2d array keeping track of WHERE the selected 
   * Unit can move within its neighborhood or "locale." */
  /* TODO see if you can do a memcpy call here
   * to make this step more efficient. */
  for (int r=0; r < tiles_y; r++) {
    for (int c=0; c < tiles_x; c++) {
      locale[r][c] = INFINITE_DIST;
    }
  }
  previous.clear();
  path.clear();
  
  /* New approach: Dijkstra's algorithm
   * on the adjacency-list graph structure. */
  /*      std::list<Entity> queue = std::list<Entity>();*/
  
  /* Step 0: init tile heap */
  Heap *tileHeap = new Heap();
  for (int c=lowerCol; c <= upperCol; c++) {
    for (int r=lowerRow; r <= upperRow; r++) {
      /* TOSS OUT ALL THE NODES THAT DO NOT SATISFY 
       * AN OVERALL REQUIREMENT!!!
       * This is really cool. I can
       * just filter ALL the nodes right here in this loop!
       * (i.e., only INCLUDE empty tiles and Player Unit-occupied tiles)
       * I sure hope it works... */
      if ((unit_board[r][c] == nullptr ||
           unit_board[r][c]->getTeam() == 0)) {
        tileHeap->insert(Node(c, r));
      }
    }
  }
  
  /* Step 1: set the src node distance to zero. */
  locale[yCoord][xCoord] = 0;
  if (tileHeap->replace(Node(xCoord, yCoord, -1), 0, Entity(xCoord, yCoord))) {
    
  } else {
    std::cerr<<"[ ERR ] Could not find the (x_0, y_0) src-node in the heap!!! :-("<<std::endl;
  }
  
  /* Step 2: set correct initial values for the distance
   * of nearest neighbors to the source node,
   * which are equal to movement costs! */
  for (Edge &iter : (*graph[tiles_x*yCoord+xCoord])) {
    /*        iter.print();*/
    tileHeap->replace(Node(iter.getX(), iter.getY()), iter.getWeight(),
      Entity(xCoord, yCoord));
  }
  int maxDist = 0;
  
  /* Step 3: Do the loop! Check through all other nodes. */
  /* Consider uncommenting the second condition...
   * It MIGHT theoretically be useful to stop this loop short 
   * after a neighborhood around the src node has been sufficiently
   * computed. But this is ... not easy to determine,
   * and could be error-prone. So don't uncomment it yet. */
  while ((! tileHeap->isEmpty())/* && (maxDist <= speed)*/) {
    Node nextClosest = tileHeap->extractMin();
    
    /* If the destination node has NOT been visited,
     * then we do indeed want to continue. */
    std::list<Edge> *destNeighbors = graph[nextClosest.hash(tiles_x)];
    /* For each neighbor, what are we doing...? */
    for (auto &iter : (*destNeighbors)) {
      int interX = iter.getX();
      int interY = iter.getY();
      Node nextNbr = Node(interX, interY);
      
      /* TODO check if nextNbr is not occupied by an enemy??? */
      if (tileHeap->contains(nextNbr)) {
        int altPath = iter.getWeight() + nextClosest.getDistance();
        
        /* The "LESS THAN?" check is done INSIDE the 
         * function call.
         * Why? Because I'm a f***ing madman. ;) */
        tileHeap->replace(nextNbr, altPath, Entity(nextClosest.getX(), nextClosest.getY()));
      }
    }
    locale[nextClosest.getY()][nextClosest.getX()] = nextClosest.getDistance();
    previous.emplace(nextClosest, nextClosest.getPrevious()); //[nextClosest] = nextClosest.getPrevious();
    if (maxDist < nextClosest.getDistance()) {
      maxDist = nextClosest.getDistance();
    }
  }
  
  SDL_SetRenderTarget(displayTMB, layer2);
  for (int r=lowerRow; r <= upperRow; r++) {
    for (int c=lowerCol; c <= upperCol; c++) {
      /*          std::cout <<locale[r][c]<<"\t|";*/
      if (locale[r][c] <= speed && unit_board[r][c] == nullptr) {
        /* Color this tile */
        int ulx = c*tile_side_length;
        int uly = r*tile_side_length;
        SDL_SetRenderDrawColor(displayTMB, 2, 180, 2, 156);
        dframe->x = ulx;
        dframe->y = uly;
        dframe->w = tile_side_length;
        dframe->h = tile_side_length;
        SDL_RenderFillRect(displayTMB, dframe);
      }
    }
    /*        std::cout << std::endl;*/
  }
  SDL_SetRenderTarget(displayTMB, nullptr);
  
  delete tileHeap;
}

void TacticalMapBattle::show_weapon_range_efficient(int upper_range, int lower_range,
                                 int xCoord, int yCoord) {
  int ind = lower_range;
  /* NW */
  while (ind < upper_range) {
    int i = 0;
    while (i < upper_range) {
      /* consider moving RIGHT `i` times and UP `n-i` times*/
      int x_ind = (xCoord+i);
      int y_ind = (yCoord-upper_range+i);
      if ((y_ind > 0) && (y_ind < tiles_y)
          && (x_ind > 0) && (x_ind < tiles_x)
          && (unit_board[y_ind][x_ind] != nullptr)
          && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
        int ulx = xCoord*tile_side_length;
        int uly = yCoord*tile_side_length;
        SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
        dframe->x = ulx;
        dframe->y = uly;
        dframe->w = tile_side_length-1;
        dframe->h = tile_side_length-1;
        SDL_RenderFillRect(displayTMB, dframe);
      }
      i ++;
    }
    ind ++;
  }
  ind = 0;
  
  /* NE */
  while (ind < upper_range) {
    int i = 0;
    while (i < upper_range) {
      /* consider moving RIGHT `i` times and DOWN `n-i` times*/
      int y_ind = (xCoord+i);
      int x_ind = (yCoord+upper_range-i);
      if ((y_ind > 0) && (y_ind < tiles_y)
          && (x_ind > 0) && (x_ind < tiles_x)
          && (unit_board[y_ind][x_ind] != nullptr)
          && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
        int ulx = xCoord*tile_side_length;
        int uly = yCoord*tile_side_length;
        SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
        dframe->x = ulx;
        dframe->y = uly;
        dframe->w = tile_side_length-1;
        dframe->h = tile_side_length-1;
        SDL_RenderFillRect(displayTMB, dframe);
      }
      i ++;
    }
    ind ++;
  }
  ind = 0;
  
  /* SW */
  while (ind < upper_range) {
    int i = 0;
    while (i < upper_range) {
      /* consider moving LEFT `i` times and DOWN `n-i` times*/
      int x_ind = (xCoord-i);
      int y_ind = (yCoord+upper_range-i);
      if ((y_ind > 0) && (y_ind < tiles_y)
          && (x_ind > 0) && (x_ind < tiles_x)
          && (unit_board[y_ind][x_ind] != nullptr)
          && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
        int ulx = xCoord*tile_side_length;
        int uly = yCoord*tile_side_length;
        SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
        dframe->x = ulx;
        dframe->y = uly;
        dframe->w = tile_side_length-1;
        dframe->h = tile_side_length-1;
        SDL_RenderFillRect(displayTMB, dframe);
      }
      i ++;
    }
    ind ++;
  }
  ind = 0;
  
  /* SE */
  while (ind < upper_range) {
    int i = 0;
    while (i < upper_range) {
      /* consider moving LEFT `i` times and UP `n-i` times*/
      int y_ind = (yCoord-i);
      int x_ind = (xCoord-upper_range+i);
      if ((y_ind > 0) && (y_ind < tiles_y)
          && (x_ind > 0) && (x_ind < tiles_x)
          && (unit_board[y_ind][x_ind] != nullptr)
          && (unit_board[y_ind][x_ind]->getTeam() == 1)) {
        int ulx = xCoord*tile_side_length;
        int uly = yCoord*tile_side_length;
        SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
        dframe->x = ulx;
        dframe->y = uly;
        dframe->w = tile_side_length-1;
        dframe->h = tile_side_length-1;
        SDL_RenderFillRect(displayTMB, dframe);
      }
      i ++;
    }
    ind ++;
  }
}

/* Precondition: xCoord and yCoord are valid (nonnegative and less than the
 *  width and height of the TMB map).
 * 
 * Postcondition: Displays maximum weapon range by drawing rectangles
 *  on the TMB screen.
 * 
 * TODO Fix this for min range and max range
 *   probably with a simple combination of nested loops
 *   ...  the recursive approach here does not work */
//void TacticalMapBattle::show_weapon_range(int range, int base, int xCoord, int yCoord) {
//  if ((xCoord == 0) && (yCoord == 0)) {
//    if (range == base) {
//      if (terrain_board[yCoord][xCoord] == 0) {
//        // No-op
//      } else if (unit_board[yCoord][xCoord] == nullptr) {
//        // No-op
//      } else if (unit_board[yCoord][xCoord]->getTeam() == 1) {
//        SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
//        dframe->x = 1;
//        dframe->y = 1;
//        dframe->w = tile_side_length-1;
//        dframe->h = tile_side_length-1;
//        SDL_RenderFillRect(displayTMB, dframe);
//      }
//    } else {
//      if (terrain_board[0][0] == 0) {
//        // No-op
//      } else if (unit_board[0][0] == nullptr) {
//        show_weapon_range(range-1, base, 1, 0);
//        show_weapon_range(range-1, base, 0, 1);
//      } else if (unit_board[0][0]->getTeam() == 1) {
//        /*            printf("[red is %u, alpha is %u] ", r, alpha);
//         if (alpha < 150) {*/
//        SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
//        dframe->x = 1;
//        dframe->y = 1;
//        dframe->w = (tile_side_length)-(1);
//        dframe->h = (tile_side_length)-(1);
//        SDL_RenderFillRect(displayTMB, dframe);
//        /*              printf("Now drawing red at (%d, %d)\n", yCoord, xCoord);*/
//        /*            }*/
//        show_weapon_range(range-1, base, 1, 0);
//        show_weapon_range(range-1, base, 0, 1);
//      } else {
//        show_weapon_range(range-1, base, 1, 0);
//        show_weapon_range(range-1, base, 0, 1);
//      }
//    }
//  } else if ((xCoord >= 0) && (xCoord < tiles_x) &&
//             (yCoord >= 0) && (yCoord < tiles_y)) {
//    int ulx = xCoord*tile_side_length;
//    int uly = yCoord*tile_side_length;
//    SDL_Color current_color;
//    //!        void *pixel = calloc(1, sizeof(Uint32));
//    //!       int pitch_hash = 0;
//    //!       const SDL_Rect clicked = {ulx+(tile_side_length/2), uly+(tile_side_length/2), 1, 1};
//    //!       SDL_LockTexture(layer2, &clicked, &pixel, &pitch_hash);
//    Uint8 /*r, g, b, */alpha;
//    //!       SDL_GetRGBA(((Uint32 *)pixel)[0], SDL_Formats::rgba_format, &r, &g, &b, &alpha);
//    if (range == base) {
//      if (terrain_board[yCoord][xCoord] == 0) {
//        // No-op
//      } else if ((unit_board[yCoord][xCoord] != nullptr)
//                 && (unit_board[yCoord][xCoord]->getTeam() == 1)) {
//        if (alpha < 120) {
//          SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
//          dframe->x = ulx;
//          dframe->y = uly;
//          dframe->w = tile_side_length-1;
//          dframe->h = tile_side_length-1;
//          SDL_RenderFillRect(displayTMB, dframe);
//        }
//      }
//    } else {
//      if (terrain_board[yCoord][xCoord] == 0) {
//        // No-op
//      } else /*if (alpha < 3)*/ {
//        /*            printf("tile (%d, %d) is not an ocean tiles\n", yCoord, xCoord);*/
//        if (unit_board[yCoord][xCoord] == nullptr) {
//          show_weapon_range(range-1, base, xCoord-1, yCoord);
//          show_weapon_range(range-1, base, xCoord+1, yCoord);
//          show_weapon_range(range-1, base, xCoord, yCoord-1);
//          show_weapon_range(range-1, base, xCoord, yCoord+1);
//        } else if (unit_board[yCoord][xCoord]->getTeam() == 1) {
//          /*              printf("Unit clicked is %s\n",
//           unit_board[yCoord][xCoord]->toString()
//           c_str());*/
//          /*              printf("\tIts Team Code = %d\n",
//           unit_board[yCoord][xCoord]->getTeam());*/
//          /*              printf("[red is %u, alpha is %u] ", r, alpha);*/
//          if (alpha < 120) {
//            SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
//            dframe->x = ulx;
//            dframe->y = uly;
//            dframe->w = tile_side_length-1;
//            dframe->h = tile_side_length-1;
//            SDL_RenderFillRect(displayTMB, dframe);
//            /*                printf("Now drawing red at (%d, %d)\n", yCoord, xCoord);*/
//          }
//          show_weapon_range(range-1, base, xCoord-1, yCoord);
//          show_weapon_range(range-1, base, xCoord+1, yCoord);
//          show_weapon_range(range-1, base, xCoord, yCoord-1);
//          show_weapon_range(range-1, base, xCoord, yCoord+1);
//        } else {
//          show_weapon_range(range-1, base, xCoord-1, yCoord);
//          show_weapon_range(range-1, base, xCoord+1, yCoord);
//          show_weapon_range(range-1, base, xCoord, yCoord-1);
//          show_weapon_range(range-1, base, xCoord, yCoord+1);
//        }
//      }
//    }
//  }
//}

/* Precondition: next_u is a valid, non-null pointer to a player Unit.
 * Postcondition: if next_u has already moved, no-op.
 *  Otherwise, add next_u to the list of moved units.
 *  TODO make this a HashSet to provide O(1) operations,
 *  rather than the O(n) vector implementation provided here. */
void TacticalMapBattle::set_moved(Unit *next_u) {
  if ((next_u != nullptr) &&
      (moved_set.find(next_u) == moved_set.end())) {
    moved_set.insert(next_u);
  }
}

/* Precondition: start_ind is some natural number whose value is less
 *  than the current size of the menu.
 * Postcondition: Add a player unit to the "has-moved-this-turn" structure.
 * Also free memory associated with the menu. */
void TacticalMapBattle::end_unit_turn(int start_ind) {
  if (chosen) {
    set_moved(chosen->getHeld());
  }
  set_moved(chosen);
  /*
   for (int index=0; index < friendlies.size(); index ++) {
   Unit nextPlayerUnit = friendlies->at(index);
   if (nextPlayerUnit.getHeld() != nullptr) {
   moved.add(nextPlayerUnit.getHeld());
   }
   }*/
  /*      fprintf(stdout, "Just added "+highlighted.toString()+" to the moved array\n");*/
  /*      if (allUnitsMoved()) {*/
  if (moved_set.size() == player_units.size()) {
    /*        setState(0);*/
    /*        localLogger.log("[state change]   -> 0", true);*/
    moved_set.clear();
    setTurnPhase((getTurnPhase() + 1) % NUM_SIDES);
    /*        currentTeam ++;
     currentTeam %= NUM_SIDES;*/
  } else {
    /*        localLogger.log("Not all player units have moved, apparently.", true);*/
  }
  setGameState(IDLE);
  chosen = nullptr;
  unsigned int upper_bound = menu.size();
  if (upper_bound > 0) {
    /* KEEP THIS HERE */
    /* We DON'T want to delete the last string in the menu,
     * because this will be (in all cases currently covered)
     * a "stock option", i.e. one of the standard constant menu options
     * that we only deallocate at the end. */
    upper_bound --;
    
    /* Now, for all other menu options, they will be temporary
     * strings. So free their memory normally. */
  }
  /* Now clear the vectors. Don't want any dangling pointers, after all :P */
  menu.clear();
  in_range.clear();
}

/* void find_enemies(int range, int xCoord, int yCoord) {
    if ((xCoord == 0) && (yCoord == 0)) {
      if (range == 0) {
        if (terrain_board[yCoord][xCoord] == 0) {
 // No-op
        } else if (unit_board[yCoord][xCoord] == nullptr) {
 // No-op
        } else if (unit_board[yCoord][xCoord]->getTeam() == 1) {
 / *          SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
         dframe->x = 1;
         dframe->y = 1;
         dframe->w = (tile_side_length)-(1);
         dframe->h = (tile_side_length)-(1);
         SDL_RenderFillRect(displayTMB, dframe);* /
        }
      } else {
        if (terrain_board[0][0] == 0) {
         // No-op
         } else if (unit_board[0][0] == nullptr) {
           find_enemies(range-1, 1, 0);
           find_enemies(range-1, 0, 1);
         } else if (unit_board[0][0]->getTeam() == 1) {
 / *            printf("[red is %u, alpha is %u] ", r, alpha);
            if (alpha < 150) {* /
 / *            SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
             dframe->x = 1;
             dframe->y = 1;
             dframe->w = (tile_side_length)-(1);
             dframe->h = (tile_side_length)-(1);
             SDL_RenderFillRect(displayTMB, dframe);* /
 
 / *              printf("Now drawing red at (%d, %d)\n", yCoord, xCoord);* /
 / *            }* /
             find_enemies(range-1, 1, 0);
             find_enemies(range-1, 0, 1);
           } else {
             find_enemies(range-1, 1, 0);
             find_enemies(range-1, 0, 1);
          }
        }
      } else if ((xCoord >= 0) && (xCoord < tiles_x) &&
                  (yCoord >= 0) && (yCoord < tiles_y)) {
          int ulx = xCoord*tile_side_length;
          int uly = yCoord*tile_side_length;
         SDL_Color current_color;
         current_color = SDL_LockTexture(layer2, ulx+(tile_side_length/2),
         uly+(tile_side_length/2));
         Uint8 r, g, b, alpha;
         SDL_GetRGBA(pixel[0], SDL_Formats::rgba_format, &r, &g, &b, &alpha);
         if (range == 0) {
            if (terrain_board[yCoord][xCoord] == 0) {
           // No-op
           } else if ((unit_board[yCoord][xCoord] != nullptr)
                      && (unit_board[yCoord][xCoord]->getTeam() == 1)) {
              if (alpha < 120) {
                SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
                 dframe->x = ulx;
                 dframe->y = uly;
                 dframe->w = (ulx+tile_side_length-1)-(ulx);
                 dframe->h = (uly+tile_side_length-1)-(uly);
                 SDL_RenderFillRect(displayTMB, dframe);
               }
            }
          } else {
              if (terrain_board[yCoord][xCoord] == 0) {
                  // No-op
              } else / *if (alpha < 3)* / {
               if (unit_board[yCoord][xCoord] == nullptr) {
                 find_enemies(range-1, xCoord-1, yCoord);
                 find_enemies(range-1, xCoord+1, yCoord);
                 find_enemies(range-1, xCoord, yCoord-1);
                 find_enemies(range-1, xCoord, yCoord+1);
               } else if (unit_board[yCoord][xCoord]->getTeam() == 1) {
                 if (alpha < 120) {
                   SDL_SetRenderDrawColor(displayTMB, 180, 2, 2, 126);
                   dframe->x = ulx;
                   dframe->y = uly;
                   dframe->w = (ulx+tile_side_length-1)-(ulx);
                   dframe->h = (uly+tile_side_length-1)-(uly);
                   SDL_RenderFillRect(displayTMB, dframe);
                 }
                 find_enemies(range-1, xCoord-1, yCoord);
                 find_enemies(range-1, xCoord+1, yCoord);
                 find_enemies(range-1, xCoord, yCoord-1);
                 find_enemies(range-1, xCoord, yCoord+1);
               } else {
                 find_enemies(range-1, xCoord-1, yCoord);
                 find_enemies(range-1, xCoord+1, yCoord);
                 finnd_enemiesrange-1, xCoord, yCoord-1);
                 find_enemies(range-1, xCoord, yCoord+1);
               }
            }
        }
    }
 }
 */




RoutTMB::RoutTMB(const char *map_file, const char *terrain_types,
        /*ALLEGRO_DISPLAY *render_displayTMB, */std::vector<Unit *> &players,
        std::vector<Unit *> &enemies, int verbose_copy)
  : TacticalMapBattle(map_file, terrain_types,/* render_displayTMB, */players, enemies, verbose_copy)
{  
}

void RoutTMB::post_turnphase() { }

bool RoutTMB::check_victory_condition() const {
  return (enemy_units.size() == 0);
}

bool RoutTMB::check_loss_condition() const {
  return (player_units.size() == 0);
}

TMB_TYPE RoutTMB::getType() { return ROUT; }


BossTMB::BossTMB(const char *map_file, const char *terrain_types,
        /*  ALLEGRO_DISPLAY *render_displayTMB, */std::vector<Unit *> &players,
        std::vector<Unit *> &enemies, std::vector<Unit*> &given_boss,
        int verbose_copy)
  : TacticalMapBattle (map_file, terrain_types,/* render_displayTMB,*/ players, enemies, verbose_copy)
{
  bosses = given_boss;
}

void BossTMB::post_turnphase() { }

bool BossTMB::check_victory_condition() const {
  return (bosses.size() > 0);
}
bool BossTMB::check_loss_condition() const {
  if (player_units.size() == 0) {
    return true;
  }
  return false;
}

TMB_TYPE BossTMB::getType() { return BOSS; }


SeizeTMB::SeizeTMB(const char *map_file, const char *terrain_types,
         /*  ALLEGRO_DISPLAY *render_displayTMB,*/ std::vector<Unit *> &players,
         std::vector<Unit *> &enemies, int x, int y, int verbose_copy)
  : TacticalMapBattle (map_file, terrain_types,/* render_displayTMB, */players, enemies, verbose_copy)
{
  dest_x = x;
  dest_y = y;
}

void SeizeTMB::post_turnphase() { }

bool SeizeTMB::check_victory_condition() const {
  if ((unit_board[dest_y][dest_x] != nullptr) &&
      (unit_board[dest_y][dest_x]->getTeam() == 0)) {
    return true;
  }
  return false;
}

bool SeizeTMB::check_loss_condition() const {
  if (player_units.size() == 0) {
    return true;
  }
  return false;
}

TMB_TYPE SeizeTMB::getType() { return SEIZE; }

DefendLocTMB::DefendLocTMB(const char *map_file, const char *terrain_types,
             /*  ALLEGRO_DISPLAY *render_displayTMB,*/ std::vector<Unit *> &players,
             std::vector<Unit *> &enemies, size_t turn_count, int verbose_copy)
  : TacticalMapBattle(map_file, terrain_types,/* render_displayTMB,*/ players, enemies, verbose_copy)
{
  turns_left = turn_count;
}

void DefendLocTMB::post_turnphase() {
  turns_left --;
}
bool DefendLocTMB::check_victory_condition() const {
  return ((player_units.size() > 0) && (turns_left == 0));
}
bool DefendLocTMB::check_loss_condition() const {
  /* */
  return ((turns_left == 0) || ((player_units.size() == 0)));
}

TMB_TYPE DefendLocTMB::getType() { return DEFEND_LOC; }

DefendUnitTMB::DefendUnitTMB(const char *map_file, const char *terrain_types,
              /*  ALLEGRO_DISPLAY *render_displayTMB,*/ std::vector<Unit *> &players,
              std::vector<Unit *> &enemies, std::vector<Unit*> &to_protect,
              int turn_count, int verbose_copy)
  : TacticalMapBattle (map_file, terrain_types,/* render_displayTMB,*/ players, enemies, verbose_copy)
{
  protect_set = to_protect;
  turns_left = turn_count;
}

void DefendUnitTMB::post_turnphase() {
  turns_left --;
}

bool DefendUnitTMB::check_victory_condition() const {
  return ((player_units.size() > 0) && (turns_left == 0));
}

bool DefendUnitTMB::check_loss_condition() const {
  if (player_units.size() == 0) {
    return true;
  }
  unsigned int iterator = 0;
  while (iterator < protect_set.size()) {
    if (protect_set.at(iterator)->isUp()) {
      /* ok */
    } else {
      /* A protected unit is debilitated -- game over. */
      return true;
    }
    iterator ++;
  }
  return false;
}

TMB_TYPE DefendUnitTMB::getType() { return DEFEND_UNIT; }

