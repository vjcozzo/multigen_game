#pragma once
#define MAX_ITEM_LIMIT 10
#define NUM_STATS 14 /* 15 would include `team`, skill, AND MAX_HP */

#include <sstream>
#include <array>

enum STAT {
  MAX_HP=0, HP, STR, MAGIC_STR, HIT_SPEED, SPEED, LUCK, DODGE, ARMOUR, ACC, CONS,
  INIT, MAGIC_RES, SKILL
};

/* Offers a nice front-end to deal with stat changes,
 * including damage, etc.  */
class StatChange {
  public:
    StatChange(int maxH, int health, int str, int speed, int agl, int armo,
        int acc, int initi,/* int team, */int luck, int constitution,
        int magStr, int hitSpd, int magRes) {
      intRep[MAX_HP] = maxH;
      intRep[HP] = health;
      intRep[STR] = str;
      intRep[SPEED] = speed;
      intRep[DODGE] = agl;
      intRep[ARMOUR] = armo;
      intRep[ACC] = acc;
      intRep[INIT] = initi;
      intRep[LUCK] = luck;
      intRep[CONS] = constitution;
      intRep[MAGIC_STR] = magStr;
      intRep[HIT_SPEED] = hitSpd;
      intRep[MAGIC_RES] = magRes;
    }
    StatChange(int health, int str, int speed, int agl, int armo) {
      intRep[HP] = health;
      intRep[STR] = str;
      intRep[SPEED] = speed;
      intRep[DODGE] = agl;
      intRep[ARMOUR] = armo;
      intRep[ACC] = 0;
      intRep[INIT] = 0;
      intRep[LUCK] = 0;
      intRep[CONS] = 0;
      intRep[MAGIC_STR] = 0;
      intRep[HIT_SPEED] = 0;
      intRep[MAGIC_RES] = 0;
    }
    StatChange(std::array<int, NUM_STATS> p) {
      intRep = p;
    }
    ~StatChange() {
    }

    const std::array<int, NUM_STATS> arrRep() const {
      return intRep;
    }

    /*            health = h;
                  strength = s;
                  speed = sp;
                  dodge = ag;
                  armour = ar;

                  accuracy = ac;
                  initiative = i;
                  team = t;
                  luck = l;
                  constitution = cs;

                  magicalStrength = ms;
                  hitSpeed =  hs;
                  magicalResistance = mR;*/
  
    std::string toString() const {
      std::stringstream descrSetup;
      /* Health */
      if (intRep[0] > 0) {
        descrSetup << "+" << intRep[0] << " ";
      } else if (intRep[0] < 0) {
        descrSetup << intRep[0] << " ";
      }
      /* */
      if (intRep[1] > 0) {
        descrSetup << "STR+" << intRep[1] << " ";
      } else if (intRep[1] < 0) {
        descrSetup << "STR" << intRep[1] << " ";
      }

      if (intRep[2] > 0) {
        descrSetup << "SPD+" << intRep[2] << " ";
      } else if (intRep[2] < 0) {
        descrSetup << "SPD" << intRep[2] << " ";
      }

      if (intRep[3] > 0) {
        descrSetup << "Agility+" << intRep[3] << " ";
      } else if (intRep[3] < 0) {
        descrSetup << "Agility" << intRep[3] << " ";
      }

      if (intRep[4] > 0) {
        descrSetup << "Armour+" << intRep[4] << " ";
      } else if (intRep[4] < 0) {
        descrSetup << "Armour" << intRep[4] << " ";
      }

      if (intRep[5] > 0) {
        descrSetup << "ACC+" << intRep[5] << " ";
      } else if (intRep[5] < 0) {
        descrSetup << "ACC" << intRep[5] << " ";
      }

      if (intRep[6] > 0) {
        descrSetup << "INIT+" << intRep[6] << " ";
      } else if (intRep[6] < 0) {
        descrSetup << "INIT" << intRep[6] << " ";
      }

      if (intRep[7] > 0) {
        descrSetup << "Team " << intRep[7] << " ";
      } else if (intRep[7] < 0) {
        descrSetup << "Team " << intRep[7] << " ";
      }

      if (intRep[8] > 0) {
        descrSetup << "Luck+" << intRep[8] << " ";
      } else if (intRep[8] < 0) {
        descrSetup << "Luck" << intRep[8] << " ";
      }

      if (intRep[9] > 0) {
        descrSetup << "CONST+" << intRep[9] << " ";
      } else if (intRep[9] < 0) {
        descrSetup << "CONST" << intRep[9] << " ";
      }

      if (intRep[10] > 0) {
        descrSetup << "MagicalSTR+" << intRep[10] << " ";
      } else if (intRep[10] < 0) {
        descrSetup << "MagicalSTR" << intRep[10] << " ";
      }

      if (intRep[11] > 0) {
        descrSetup << "HitSPD+" << intRep[11] << " ";
      } else if (intRep[11] < 0) {
        descrSetup << "HitSPD" << intRep[11] << " ";
      }

      if (intRep[12] > 0) {
        descrSetup << "MagicalRES+" << intRep[12] << " ";
      } else if (intRep[12] < 0) {
        descrSetup << "MagicalRES" << intRep[12] << " ";
      }

      return descrSetup.str();
    }
  
  protected:
    std::array<int, NUM_STATS> intRep;
};

/* Item class - still needs to be fleshed out of course */
class Item {
  public:
    Item(const std::string &t_name) : name(t_name) {
      name = t_name;
    }
    virtual ~Item() { }

    const std::string& toString() const {
      return name;
    }

    void setName(const std::string &gn) {
      name = gn;
    }

    virtual StatChange *apply() const = 0;
  
  protected:
    std::string name;
};

class Potion : public Item {
  public:
    Potion(int givenH) : Item("Potion") {
      health = givenH;
      std::stringstream nameSetup;
      if (health < 0) {
        nameSetup << "Potion: " << health << "HP";
      } else {
        nameSetup << "Potion: +" << health << "HP";
      }
      setName(nameSetup.str());
      data = new StatChange(health, 0, 0, 0, 0);
    }
    ~Potion() {
      delete data;
    }

    int getHealth() const {
      return health;
    }

    StatChange *apply() const {
      return data;
    }
  
  protected:
    int health;
    StatChange *data;
};
