/*
 *    The MultiGen Game -- an elaborate, customizable 2D strategy game.
 *    Copyright (C) 2019 and GNU GPL'd by Vincent Cozzo & TrebledJ
 *    Thanks to all contributors, including but not limited to:
 *        TrebledJ (C++ design & development)
 *        MrMcApple (Java code design & implemenetation)
 *    A minor thanks to David Holmes for answering C++ questions.
 *
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License along
 *    with this program; if not, write to the Free Software Foundation, Inc.,
 *    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
/*
 * TODO make more documentation
 * 
 * This file (obviously) holds the value definitions & utils needed at the WMSL level.
 * 
 * Note: the WMSL is represented by game-loop.hpp and Game.*pp
 */

#include "global.hpp"
#include "unit.hpp"
#include "tmb.hpp"

#include "SDL_FontCache.hpp"

/* HALF SIZE */
#define SCREEN_WIDTH 960
#define SCREEN_HEIGHT 540

namespace global {
  extern size_t screen_width;
  extern size_t screen_height;
}

/* 3/4 SIZE */
/*#define SCREEN_WIDTH 1440
#define SCREEN_HEIGHT 810*/

/* Here, define constants used exclusively in the game loop hpp file... */
#define LOOP_ITER 1600
#define BASE 200
#define OPTION_MENU_DIAL_HEIGHT 30
#define NUM_SETTLEMENTS_PER_PROVINCE 5
#define DAYS_PER_MONTH 10
#define MONTHS_PER_YEAR 18
/* Unit leger constants */
#define NUM_OPTIONS 5
#define TABLE_ROW_HEIGHT 20
#define NUM_UNITS_PER_PAGE 20
#define RETIREMENT 60
#define OLD_AGE 80

/* TODO Change this to a different constant,
 * or make it adjustable in game settings...?
 * NOTE TO SELF this value is not used within this file,
 * but it deinifitely is used in Game.cpp !!!
 * So maybe relocate the #define to there? Idk. */
#ifdef SCALE
#else
  #define SCALE 10
#endif

/* Halloween color */
#define HL_OJ_R 230
#define HL_OJ_G 98
#define HL_OJ_B 10

#define VS1_BG_R 204
#define VS1_BG_G 204
#define VS1_BG_B 204
#define VS1_BG_A 254

#define VS2_BG_R 55
#define VS2_BG_G 240
#define VS2_BG_B 1
#define VS2_BG_A 20

#define MID_FONT_SIZE 22

#ifndef UNORDERED_MAP
  #include <unordered_map>
#endif

/* FULL size, presumably: */
/*#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080*/

const int leftPanelBound = (17.72*global::screen_width)/100;
const int rightPanelBound = (82.28*global::screen_width)/100;
const int dy = (global::screen_height)/8;

typedef struct selected {
  int x;
  int y;
} highlighted_province;

//namespace WMSL_Utils {
//    SDL_Surface *mapTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "MAP", black_text);
//    SDL_Texture *mapText = SDL_CreateTextureFromSurface(display, mapTextSurf);
//    
//    SDL_Surface *armyTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "ARMY", black_text);
//    SDL_Texture *armyText = SDL_CreateTextureFromSurface(display, armyTextSurf);
//    
//    SDL_Surface *researchTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "RESEARCH", black_text);
//    SDL_Texture *researchText = SDL_CreateTextureFromSurface(display, researchTextSurf);
//    
//    SDL_Surface *homefrontTextSurf = TTF_RenderUTF8_Solid(font_luxirb_med, "HOMEFRONT", black_text);
//    SDL_Texture *homefrontText = SDL_CreateTextureFromSurface(display, homefrontTextSurf);
//};

/*
   int show_next_tick(unsigned int *time, unsigned int margin) {
 *time = (*time)+margin;
 return 0;
 }
 */

class DecayFunc {
  public:
    DecayFunc(double coef, double stretch, double floor, double year)
      : c(coef), k(stretch), f(floor), t_0(year) {

    }

    double apply(int current_y) {
      if (current_y > t_0) {
        return f+exp(k*(t_0 - current_y));
      } else {
        return f;
      }
    }
  protected:
    double c;
    double k;
    double f;
    double t_0;
};

class FuncSum {
  public:
    FuncSum() {
      components = new std::vector<DecayFunc *>();
      DecayFunc *init = new DecayFunc(1, 0.5, 12, 1);
      components->push_back(init);
    }
    ~FuncSum() {
      for (DecayFunc *g : *components) {
        delete g;
      }
      delete components;
    }

    void addFunc(DecayFunc *f) {
      components->push_back(f);
    }

    double eval(int current_year) {
      double result = 0;
/*      unsigned int limit = components->size();
      for (unsigned int ind=0; ind < limit; ind ++) {*/
      for (DecayFunc *f : *components) {
        /* TODO make a comparison to check if the component function
         * is even "STILL VALID" ...
         * and if it's not, then remove it from the list.
         * */
        result += f->apply(current_year);
      }
      return result;
    }
  protected:
    std::vector<DecayFunc *> *components;
};

/* An enum for the status of the visual type (what is being displayed 
 * in the lower-right quadrant) */
enum visual_code {
  MAP=0, ARMY=1, RESEARCH=2, HOMEFRONT=3, ESIONAGE=4
};

/* TODO separate Settlement Status from Province Status
 *  (a province can be CONTESTED, but a Settlement cannot be,
 *   unless it is a Field Settlement... hmmm might need a refactor. */
enum SETTLE_STATUS {
  PLAYER_OWNED, ENEMY_OWNED, CONTESTED/*, DEFAULT */
};

/*enum PROVINCE_STATUS {
  PLAYER_OWNED, ENEMY_OWNED, CONTESTED/ *, DEFAULT * /
};*/

class Area {
  public:
    Area(const std::string &title, const char *pr_name, int init_st) {
      std::stringstream nameSetup;
      nameSetup << title << ", " << pr_name;
      nameSetup.flush();
      name = nameSetup.str();
      switch (init_st) {
        case 0:
          status = PLAYER_OWNED;
          break;
        case 1:
          status = ENEMY_OWNED;
          break;
        case 2:
          status = CONTESTED;
          break;
        default:
          status = CONTESTED/* DEFAULT */;
          break;
      }
    }
    Area(const std::string &title, const std::string &pr_name, int init_st) {
      std::stringstream nameSetup;
      nameSetup << title << ", " << pr_name;
      nameSetup.flush();
      name = nameSetup.str();
      switch (init_st) {
        case 0:
          status = PLAYER_OWNED;
          break;
        case 1:
          status = ENEMY_OWNED;
          break;
        case 2:
          status = CONTESTED;
          break;
        default:
          status = CONTESTED/* DEFAULT */;
          break;
      }
    }
    
    virtual ~Area() {
/*      delete name;*/ // don't do it any more, I guess
    }
    
    SETTLE_STATUS getOccupierEnum() {
      return status;
    }
    int getOccupier() {
      return status;
    }
    void setStatus(SETTLE_STATUS new_own) {
      status = new_own;
    }
    void setStatus(int new_own) {
      switch (new_own) {
        case 0:
          status = PLAYER_OWNED;
          break;
        case 1:
          status = ENEMY_OWNED;
          break;
        case 2:
          status = CONTESTED;
          break;
        default:
          status = CONTESTED/*DEFAULT*/;
          break;
      }
/*      status = new_own;*/
    }

    const std::string& getName() const {
      return name;
    }

    virtual void addUnit(Unit *ap_u) = 0;

    void addUnits(const std::vector<Unit *> &ins) {
      for (Unit *iter : ins) {
        addUnit(iter);
      }
    }
    
  protected:
    std::string name;
    SETTLE_STATUS/* int*/ status;
};

class Settlement : public Area {
  public:
/*    Settlement(const char *title, SETTLE_STATUS init_st) {
      name = new std::string(title);
      status = init_st;
      army = new std::vector<Unit *>();
    }*/
    Settlement(const std::string &title, const std::string &p_n, int init_st)
                : Area(title, p_n, init_st) {
/*      status = init_st;*/
/*      army = new std::vector<Unit>();*/
      std::stringstream nameSetup;
      nameSetup << title;
      nameSetup.flush();
      stored_name = nameSetup.str();
    }
    
    ~Settlement() {
      unsigned int army_ind;
      for (army_ind=0; army_ind < army.size(); army_ind++) {
/*        std::cout << "[DEBUG]    Army ind is now " << army_ind
              << " out of size " << army.size() << std::endl;*/
        if (army.at(army_ind)->isUp()) {
#ifdef DEBUG_MSG
          std::cout << "[DEBUG]    Now deleting " << army.at(army_ind)->toString()
              << " in Settlement " << stored_name << std::endl;
#endif
          delete army.at(army_ind);
        } else {
          std::cerr<<"WARNING - a dead unit lingers in settlement "<<name<<"."
              <<std::endl;
        }
      }
      army.clear(); // clear out all the unnecessary dangling pointers
    }
    
    std::vector<Unit*> &units() {
      return army;
    }
    
    void addUnit(/*const */Unit *next_u) {
      if (next_u->getTeam() == status) {
        bool found = false;
        unsigned int check_exists = 0;
        /* Check if the current army roster CONTAINS this unit */
        /* TODO maybe this can be made more efficient, by using
         *  a hashed structure - `std::unordered_set` perhaps? */
        for (; check_exists < army.size(); check_exists ++) {
          if (next_u == army.at(check_exists)) {
            found = true;
          }
        }
        if (!found) {
          army.push_back(next_u);
        }
      }
    }

    void replaceUnits(std::vector<Unit *> &invaders) {
      army.assign(invaders.begin(), invaders.end());
    }

    /* Postcondition: Removes a Unit from this
     *     settlement's army structure. */
    void remove(/*const */Unit *tbd) {
      unsigned int ind, size=army.size();
/*      std::cout << "[DEBUG]    army size is " << size << std::endl;*/
      for (ind=0; ind < size; ind ++) {
/*        std::cout << "[DEBUG]    now about to check index " << ind << std::endl;*/
        if (army.at(ind) == tbd) {
          army.erase(army.begin()+ind);
          size --;
          ind --;
        }
      }
    }

  protected:
    std::vector<Unit *> army;
    std::string stored_name;
    /* DO NOT UNCOMMENT unless you are a masochist and want dependency hell
     * (which means, if any of you runs gentoo, go ahead and uncomment): 
     * Actually... maybe with a forward declaration of Province, 
     * it would work. 
     * Yo dawg we heard you like header files... */
    /*    Province *host_province;*/ 
};

/* This is the equivalent of NoSettlement,
 * i.e. the Field inside the Province */
class Field : public Area/*Settlement*/ {
    /* Field class refactor:
     * include both unit armies in one array,
     * indexed by which Team (Player or Enemy) controls it.
     * This avoids ambiguities involving:
     * "Does the units() vector's team depend on who controls the Field?"
     * "What if the Field is contested?" Then you need to keep track of which 
     * side is attacking and which is defending at a given time,
     * and store that within the Field class, wasting resources, etc.
     * While this is of course doable, it would require repeated checks
     * (if and else branches determining which army method to call).
     * Consolidating them into one array is much simpler. */
 
  public:
    /* Only uncomment if you're SURE we need a default constructor.
     * At the moment, no such need is known. */
/*    Field() : Settlement("Field", 2) {
      
    }*/
    Field(const std::string &p_n, SETTLE_STATUS s) : Area("Field", p_n, s) {
    }
    Field(const std::string &p_n, int s) : Area("Field", p_n, s) {
    }
    ~Field() {
    }
    
    std::vector<Unit*> &player_units() {
      return forces[0];
    }
    std::vector<Unit*> &enemy_units() {
      return forces[1];
    }
    
    void addUnit(Unit *next_t) {
      if (next_t->getTeam() >= 2) {
        std::cerr << "[ERR] unit's team, " << next_t->getTeam()
          << ", is unclear." << std::endl;
        ; /* throw exception? Print an error? TODO */
        return;
      }
      /* TODO replace with a std::unordered_set */
      std::vector<Unit *> &army = forces[next_t->getTeam()];
      /* ...so that this check (whether the next unit already
       * exists in the structure) is more efficient: */
      for (Unit *iter : army) {
        if (iter == next_t) {
          return;
        }
      }
      army.push_back(next_t);
    }
    
/*    std::vector<Unit *> *opposing_units() {
      return ;
    }*/
    
    /* TODO Use "remove_if"??? "Erase-remove idiom"?? Something? */
    void removePlayer(/*const */Unit *tbd) {
      unsigned int ind, size=forces[0].size();
      for (ind=0; ind < size; ind ++) {
        if (forces[0].at(ind) == tbd) {
          forces[0].erase(forces[0].begin()+ind);
          size --;
          ind --;
        }
      }
    }
  protected:
    std::vector<Unit*> forces[2];
};

class Province {
  public:
    Province(char *title, int init_st, char own) {
      std::stringstream nameSetup;
      nameSetup << title;
      nameSetup.flush();
      name = nameSetup.str();
      status = init_st;
      owner = own;
      houses = new std::vector<Settlement *>();
/*      houses->reserve(5);*/
/*      f = new Field(init_st);
      houses->push_back(f);*/
      f = nullptr;
      for (unsigned int ind=0; ind < NUM_STATS; ind ++) {
        stat_mods.emplace(ind, new FuncSum());
      }
    }
    Province(const std::string &title, int init_st, char own)
      : Province((char *)title.c_str(), init_st, own)
    {
    }
    ~Province() {
      unsigned int house_ind, upper;
      upper = houses->size();
/*      delete name;*/ // not anymore, as we move from std::string ptr to std::string
      for (house_ind=0; house_ind < upper; house_ind ++) {
/*        std::cout << "[DEBUG]    Now deleting the settlement #" << house_ind << " of name " << *houses->at(house_ind)->getName() << std::endl;*/
        delete houses->at(house_ind);
      }
      delete houses;
/*      std::cout << "[DEBUG]    Now deleting the FIELD settlement of name " << *f->getName() << std::endl;*/
      delete f;
    }

    /* accessor */
    const std::string &getName() const {
      return name;
    }
    int getStatus() const {
      return status;
    }
    std::vector<Settlement *> *getSettlements() const {
      return houses;
    }
    Field *getField() const {
      return f;
    }

    /* Modifier */
    /* TODO Sort this out.
     *  I am trying to port this to MODERN C++ practices,
     *  but how do I manage this return type, with the knowledge that
     *  the thing being returned MAY OR MAY NOT be added into the houses
     *  vector?
     *  Any help would be appreciated.
     *  I've been told I should not be using naked pointers,
     *  but what option does that leave me with here?
     *  A Field is (probably rightfully) separated from the vector-list
     *  of Settlements. 
     *  So is there a way to return a REFERENCE to the Field,
     *  without simply invoking `new` and dereferencing it?
     * */
    Area &addSettlement(const std::string &s_name, int ownership) {
      if (s_name == "Field") {
        f = new Field(name, ownership);
        return *f;
      } else {
        Settlement *next_s = new Settlement(s_name, name.c_str(), ownership);
        houses->push_back(next_s);
/*        houses->emplace_back(Settlement(s_name, name.c_str(), ownership));*/
        return *next_s;
      }
    }

/*    void addSettlement(Settlement *s) {
      if (s == nullptr) {
      } else if (s->getName()->compare("Field") == 0) {
        f = new Field(s->getOccupierEnum());
      } else {
        houses->push_back(s);
      }
    }*/
    void setStatus(int code) {
      status = code;
    }

    void remove(/*const */Unit *troop) {
/*      std::string *troop_loc = troop->getLoc();*/
      int /*house_ind, upper,*/ set = troop->getSettlId();

      if (set + 1) {
        houses->at(set)->remove(troop);
      } else {
        /* TODO maybe partition on troop's getTeam() value */
        f->removePlayer(troop);
      }
      /* Old approach, using string comparisons. (Yea. I know.) */
/*      std::string s_name = troop_loc->substr(0, troop_loc->find(","));
      upper = houses->size();
      for (house_ind=0; house_ind < upper; house_ind ++) {
        Settlement *next = houses->at(house_ind);
        if (next->getName()->compare(s_name) == 0) {
          houses->at(house_ind)->remove(troop);
        }
      }*/
    }

    /* Precondition: m is a non-null Trait object
     * Postcondition: updates Trait Modifier functions
     *    in the Province.
     * TODO
     */
    void addTraitMod(Trait *m) {
      
    }
    
    /* TODO in the future, have a more elaborate system in place
     * i.e., a Province could be more likely to generate
     * Units of a particular UNIT CLASS...
     * But for now, make it uniformly random */
    /* Eventually, the method will take a random value as input
     * and then compare it successively to various THRESHOLD VALUES
     * (of which there is one PER Unit Class).
     * 
     * So each Province will store an array of integer thresholds.
     * If arr[i] < input < arr[i+1],
     * then that particular UC is chosen and the method will 
     * return the appropriate INDEX of the array. */
    unsigned int getNextUC(unsigned int input) {
      return (input % 100) / 50;
    }

    void applyStatMods(Unit *warrior, int y) {
      std::array<int, NUM_STATS> wrap;
      for (int ind=0; ind < NUM_STATS; ind ++) {
        int result = stat_mods.at(ind)->eval(y);
        wrap[ind] = result;
      }
      StatChange changes = StatChange(wrap);
      warrior->alter(nullptr, &changes);
    }
  protected:
    std::string name;
    int status;
    char owner;
    std::vector<Settlement *> *houses;
    /* Field (there should be ONE AND ONLY ONE per Province): */
    Field *f;
    std::unordered_map<int/*STAT_NAME*/, FuncSum*> stat_mods;
};

class Capitol : public Settlement {
  public:
    Capitol(const char *p_n, int init_st) : Settlement("DefaultCapitol", p_n, init_st) {

    }
};

class Fort : public Settlement {
  public:
    Fort(const char *p_n, int init_st) : Settlement("DefaultFort", p_n, init_st) {

    }
};

/* Stores public utility functions...
 * Basically, all public methods used in a WMSL environment.  */
/* TODO sort out a better design for this class...
 * what is the best approach -- a single global array? Probably not.
 * A class that only contains static functions and needs not be instantiated?
 * This would probably save heap memory (no need to keep instantiating the
 * same class simply to do the same thing in different places) ...
 * Can I use an initializer list for a private static variable? I bet I can.
 * Look into this. */
/* PLAN FOR NOW:
 * Just keep them as static C strings... */
namespace Util {
  static SDL_Color white_text = {255, 255, 255, 255};
  
/*  std::string *stat_msgs[4] = { new std::string("Status: PLAYER"),
                                  new std::string("Status: OPPONENT"),
                                  new std::string("Status: CONTESTED"),
                                  new std::string("Status: UNKNOWN")};*/
  static const char *stat_msgs[4] = { "Status: PLAYER",
                         "Status: OPPONENT",
                         "Status: CONTESTED",
                         "Status: UNKNOWN" };
  
/*  static const char* getFormat(SDL_Surface *surf) {
    return SDL_GetPixelFormatName(surf->format->format);
  }*/
  
  static Uint32 getPixel(SDL_Surface *surface, int x, int y) {
    int lockResult = SDL_LockSurface(surface);
    if (lockResult < 0) {
      std::cerr << "[ ERR ]    Internal SURFACE LOCKING error: "
        << SDL_GetError() << std::endl;
    }
    
    /* *** NOTE *** */
    /* The following code segment
     * is heavily inspired by (but not identical to) the example at
     * http://sdl.beuc.net/sdl.wiki/Pixel_Access 
     * How does the license work? TODO check it. */
    Uint8 *pxlCopy = (Uint8 *)surface->pixels;
    Uint8 *selPixl = pxlCopy + (surface->format->BytesPerPixel*x)
                             + (y*(surface->pitch));
    Uint32 aggregateColVal = selPixl[0]/* | (selPixl[1] << 8) | (selPixl[2] << 16)*/;
    aggregateColVal |= (selPixl[1] << 8);
    aggregateColVal |= (selPixl[2] << 16);
    
    if (surface->format->BytesPerPixel != 4 &&
        surface->format->BytesPerPixel != 3) {
      std::cerr << "[ ERR ] Inconsistency detected." << std::endl;
      std::cerr << "[ ERR ]   Bytes per pixel: expected 3 or 4, got " << surface->format->BytesPerPixel << std::endl;
    }
    SDL_UnlockSurface(surface);
    return aggregateColVal;
  }

  /* TODO debug this, make sure that a Uint overflow
   * is not possible */
  static unsigned int hash(Uint8 r, Uint8 g, Uint8 b, Uint8 alpha) {
    unsigned int result;
    result = (r + g + b + alpha - 100);
    return result;
  }

  static Trait *getRandomTrait() {
    Trait *result = new Trait(0, 2);

    return result;
  }
}
/*
class Util {
  private:
    std::string *stat_msgs[4];
  
  public:
    Util() {
      stat_msgs[0] = new std::string("Status: PLAYER");
      stat_msgs[1] = new std::string("Status: OPPONENT");
      stat_msgs[2] = new std::string("Status: CONTESTED");
      stat_msgs[3] = new std::string("Status: UNKNOWN");
    }
    ~Util() {
      delete stat_msgs[0];
      delete stat_msgs[1];
      delete stat_msgs[2];
      delete stat_msgs[3];
    }
    
    / * Given a status CODE, fills in the STRING with the appropriate province status.
     * Given its own method because it will be used so frequently.
     * /
    static int get_status_text(char *reci, int st) {
      int res;
      if (st == 0) {
        res = sprintf(reci, "Status: PLAYER");
      } else if (st == 1) {
        res = sprintf(reci, "Status: OPPONENT");
      } else if (st == 2) {
        res = sprintf(reci, "Status: CONTESTED");
      } else {
        res = sprintf(reci, "Status: UNKNOWN");
      }
      return res;
    }
    
    / * The C++, Object-oriented version of the previous function:
     * one that actually outputs strings. * /
    std::string *get_status_str(/ *unsigned * /int status) {
      if (status > 4 || status < 0) {
        return stat_msgs[3];
      } else {
        return stat_msgs[status];
      }
    }
};*/

class MapDisplay {
  public:
    MapDisplay(/*SDL_Renderer *disp_p, SDL_Window *win, */SDL_Window *wm_win, int del,
        SDL_Surface *world_img, SDL_Surface *internal, TTF_Font *text_font) {
      /* In the constructor, I need to set a NEW scale factor,
       * based on the width/height of the image and 
       * the values for total_width - del_x and total_height
       * (the dimensions of this temporary display struct). */
/*      disp = disp_p;
      window = win;*/
      window = SDL_CreateWindow("Map Display", global::screen_width, 0,
        TMB_SCREEN_WIDTH, TMB_SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
      disp = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
      wmsl_win = wm_win;
      SDL_SetRenderTarget(disp, nullptr);
      del_x = del;

      /* TODO is this necessary? Can I just replace total_width with 
       * TMB_SCREEN_WIDTH and total_height with TMB_SCREEN_HEIGHT?
       * This note is a "Porting to SDL" vestige. */
      total_width = TMB_SCREEN_WIDTH;
      total_height = TMB_SCREEN_HEIGHT;
      
      left_half = SDL_CreateTexture(disp, SDL_PIXELFORMAT_RGBA8888,
          SDL_TEXTUREACCESS_TARGET, del_x, total_height-dy);
      right_half = SDL_CreateTexture(disp, SDL_PIXELFORMAT_RGBA8888,
          SDL_TEXTUREACCESS_TARGET, total_width-del_x, total_height-dy);
      external_map = SDL_CreateTextureFromSurface(disp, world_img);
      internal_map = internal;
      /* Event queue initialization */
/*      internal_map = al_create_bitmap(al_get_bitmap_width(external_map), al_get_bitmap_height(external_map));*/
/*      orig_wid = al_get_bitmap_width(world_img);
      orig_hgt = al_get_bitmap_height(world_img);*/
/*      SDL_QueryTexture(world_img, nullptr, nullptr, &orig_wid, &orig_hgt);*/
      orig_wid = world_img->w;
      orig_hgt = world_img->h;
      float ratio = orig_wid / ((float) orig_hgt);
      float std_ratio = (float)(total_width-del_x) / (total_height-dy);
      if (std_ratio < ratio) {
        /* Scale the sprite bitmap based on WIDTH   */
        scalar = ((float)(total_width-del_x))/orig_wid;
        std::cout << "[width] scale factor for external map is " << scalar << std::endl;
        std::cout << "ORIGINAL width " << orig_wid << ", height " << orig_hgt << std::endl;
        std::cout << "[DEBUG] [if] BY WIDTH Scale factor " << scalar << std::endl;
        std::cout << "\tnew width " << (orig_wid*scalar) << "\tnew height " << (orig_hgt*scalar) << std::endl;
      } else {
        /* Scale the sprite bitmap based on HEIGHT  */
        scalar = ((float)total_height-dy)/orig_hgt;
        std::cout << "[height] scale factor for external map is " << scalar << std::endl;
      }
      font = text_font;
      fc_luxirb_mid = FC_CreateFont();
      FC_LoadFont(fc_luxirb_mid, disp, "res/fonts/luxirb.ttf", 16, FC_MakeColor(2, 2, 2, 255), TTF_STYLE_NORMAL);
    }
    MapDisplay(/*SDL_Renderer *disp_p, SDL_Window *win, */SDL_Window *wm_win,
        int del, SDL_Surface *world_img, SDL_Surface *internal) {
      /* In the constructor, I need to set a NEW scale factor,
       * based on the width/height of the image and 
       * the values for total_width -  del_x and total_height
       * (the dimensions of this temporary display struct). */
/*      disp = disp_p;
      window = win;*/
      window = SDL_CreateWindow("Map Display", global::screen_width, 0,
        TMB_SCREEN_WIDTH, TMB_SCREEN_HEIGHT, SDL_WINDOW_OPENGL);
      disp = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
      wmsl_win = wm_win;
      SDL_SetRenderTarget(disp, nullptr);
      del_x = del;

      /* TODO is this necessary? Can I just replace total_width with 
       * TMB_SCREEN_WIDTH and total_height with TMB_SCREEN_HEIGHT?
       * This note is a "Porting to SDL" vestige. */
      total_width = TMB_SCREEN_WIDTH;
      total_height = TMB_SCREEN_HEIGHT;
      
      left_half = SDL_CreateTexture(disp, SDL_PIXELFORMAT_RGBA8888,
          SDL_TEXTUREACCESS_TARGET, del_x, total_height-dy);
      right_half = SDL_CreateTexture(disp, SDL_PIXELFORMAT_RGBA8888,
          SDL_TEXTUREACCESS_TARGET, total_width-del_x, total_height-dy);
      external_map = SDL_CreateTextureFromSurface(disp, world_img);
      internal_map = internal;
      
      orig_wid = world_img->w;
      orig_hgt = world_img->h;
      float ratio = orig_wid / ((float) orig_hgt);
      float std_ratio = (float)(total_width-del_x) / (total_height-dy);
      std::cout<<"total_width is "<<total_width<<"; "
          <<"less del_x equals "<<(total_width-del_x)<< std::endl;
      if (std_ratio < ratio) {
        /* Scale the sprite bitmap based on WIDTH   */
        scalar = ((float)(total_width-del_x))/orig_wid;
        std::cout << "[width] scale factor for external map is "
            << scalar << std::endl;
        std::cout << "ORIGINAL width " << orig_wid << ", height "
            << orig_hgt << std::endl;
        std::cout << "[DEBUG] [if] BY WIDTH Scale factor "
            << scalar << std::endl;
        std::cout << "\tnew width " << (orig_wid*scalar) << "\tnew height "
            << (orig_hgt*scalar) << std::endl;
      } else {
        /* Scale the sprite bitmap based on HEIGHT  */
        scalar = ((float)total_height-dy)/orig_hgt;
        std::cout << "[height] scale factor for external map is " << scalar << std::endl;
      }
      if (internal_map->w != orig_wid) {
        std::cerr<<"[ AHA ]    widths do not match!"<<std::endl;
      }
      if (internal_map->h != orig_hgt) {
        std::cerr<<"[ AHA ]    heights do not match!"<<std::endl;
      }
      font = TTF_OpenFont("res/fonts/luxirb.ttf", MID_FONT_SIZE);
      fc_luxirb_mid = FC_CreateFont();
      FC_LoadFont(fc_luxirb_mid, disp, "res/fonts/luxirb.ttf", 16, FC_MakeColor(2, 2, 2, 255), TTF_STYLE_NORMAL);
    }
    virtual ~MapDisplay() {
      SDL_DestroyTexture(left_half);
      SDL_DestroyTexture(right_half);
      SDL_DestroyRenderer(disp);
      SDL_DestroyWindow(window);
/*      delete utils;*/
/*      al_destroy_bitmap(internal_map);*/
      TTF_CloseFont(font);
      FC_FreeFont(fc_luxirb_mid);
    }
    
    virtual unsigned int launch() = 0;
  protected:
    /*    Util *utils;*/
    SDL_Renderer *disp;
    SDL_Window *window;
    SDL_Window *wmsl_win;
    SDL_Texture *external_map;
    SDL_Surface *internal_map;
    SDL_Texture *left_half;
    SDL_Texture *right_half;
    TTF_Font *font;
    FC_Font *fc_luxirb_mid;
    float scalar/* = 1.0f*/;
    int total_width, total_height;
    int orig_wid, orig_hgt;
    int del_x;
    SDL_Surface *instructSurf = nullptr;
    SDL_Texture *instructText = nullptr;
    SDL_Surface *instructSurf2 = nullptr;
    SDL_Texture *instructText2 = nullptr;
    /*    SDL_Color blue_text = {10, 140, 220, 255};*/
    /*    SDL_Color black_text = {2, 2, 2, 255};*/
    const SDL_MessageBoxButtonData cancel_buttons[2] =
    {
      {SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1,
        "Close"},
      {SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 2,
        "Cancel"}
    };
    SDL_PixelFormat *rgb_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGB888);
    SDL_PixelFormat *rgb_format24 = SDL_AllocFormat(SDL_PIXELFORMAT_RGB24);
    /*    SDL_PixelFormat *rgba_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA8888);*/
};

class InvadeProvinceDisplay : public MapDisplay {
  public:
    InvadeProvinceDisplay(/*SDL_Renderer *disp_p, SDL_Window *win, */
        SDL_Window *wm_win, int del, Province **p,
        unsigned int total_num_provinces, SDL_Surface *world_img,
        SDL_Surface *internal, TTF_Font *f) : MapDisplay(wm_win, del, world_img, internal, f) {
      p_list = p;
      prov_lim = total_num_provinces;
      
      instructSurf = TTF_RenderUTF8_Solid(font,
          "HOVER OVER A PROVINCE TO SEE DATA;", Util::white_text);
      instructText = SDL_CreateTextureFromSurface(disp, instructSurf);
      
      instructSurf2 = TTF_RenderUTF8_Solid(font,
          "RIGHT-CLICK A PROVINCE TO FINALIZE THE CHOICE", Util::white_text);
      instructText2 = SDL_CreateTextureFromSurface(disp, instructSurf2);
    }
    InvadeProvinceDisplay(/*SDL_Renderer *disp_p, SDL_Window *win, */
        SDL_Window *wm_win, int del, Province **p,
        unsigned int total_num_provinces, SDL_Surface *world_img,
        SDL_Surface *internal) : MapDisplay(wm_win, del, world_img, internal) {
      p_list = p;
      prov_lim = total_num_provinces;
      
      instructSurf = TTF_RenderUTF8_Solid(font,
          "HOVER OVER A PROVINCE TO SEE DATA;", Util::white_text);
      instructText = SDL_CreateTextureFromSurface(disp, instructSurf);
      
      instructSurf2 = TTF_RenderUTF8_Solid(font,
          "RIGHT-CLICK A PROVINCE TO FINALIZE THE CHOICE", Util::white_text);
      instructText2 = SDL_CreateTextureFromSurface(disp, instructSurf2);
    }
    
    unsigned int launch() {
      const SDL_MessageBoxData cancel_box = {SDL_MESSAGEBOX_INFORMATION, window,
          "Close the destination selection window?",
          "Closing this window will take you back to the world map",
          2, cancel_buttons, nullptr};
      const SDL_Rect instructTextLoc = {
          del_x+8, total_height-82,
          static_cast<int>(global::screen_width-del_x-10),
          38
      };
      const SDL_Rect instructText2Loc = {
          del_x+8, total_height-42,
          static_cast<int>(global::screen_width-del_x-10),
          38
      };
      
      unsigned int result = prov_lim + 2;
      last_pid = result;
      bool redraw = true;
/*      unsigned int temp = result;*/
/*      utils = new Util();*/
      SDL_SetRenderTarget(disp, left_half);
      SDL_SetRenderDrawColor(disp, 2, 100, 2, 100);
      SDL_RenderFillRect(disp, nullptr);
      
      SDL_SetRenderTarget(disp, right_half);
      const SDL_Rect dt = { 0, 0, static_cast<int>(scalar*orig_wid),
                                 static_cast<int>(scalar*orig_hgt) };
      SDL_RenderCopy(disp, external_map, nullptr, &dt);
      SDL_SetRenderTarget(disp, nullptr);
      
      /* Font initialization */
      TTF_Font *font_luxirb_mid = TTF_OpenFont("./res/fonts/luxirb.ttf", MID_FONT_SIZE);
      
      SDL_RenderCopy(disp, instructText, nullptr, &instructTextLoc);
      SDL_RenderCopy(disp, instructText2, nullptr, &instructText2Loc);
      SDL_RenderPresent(disp);
      
      /* The display loop */
      while (result > prov_lim) {
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
          if (ev.type == SDL_KEYDOWN) {
            if (ev.key.keysym.sym == SDLK_q) {
              int cancel = 0;
              SDL_ShowMessageBox(&cancel_box, &cancel);
              if (cancel == 1) {
                result = prov_lim;
              }
            }
/*        } else if (ev.type == SDL_Event_DISPLAY_CLOSE) {*/
          } else if (ev.type == SDL_WINDOWEVENT &&
                 ev.window.event == SDL_WINDOWEVENT_CLOSE) {
            int cancel = 0;
            SDL_ShowMessageBox(&cancel_box, &cancel);
            if (cancel == 1) {
              result = prov_lim;
            }
          } else if (ev.type == SDL_MOUSEMOTION) {
            int xClick = (ev.motion.x - del_x) / scalar;
            int yClick = (ev.motion.y - dy) / scalar;
            if (xClick > 0 && yClick > 0) {
              size_t prov_id, iter;
              Uint8 red_in, green_in, blue_in;
              
              Uint32 pixel = Util::getPixel(internal_map, xClick, yClick);
              SDL_GetRGB(pixel, rgb_format24, &red_in, &green_in, &blue_in);
              prov_id = Util::hash(red_in, green_in, blue_in, 0);
              if (prov_id != last_pid && prov_id < prov_lim) {
                last_pid = prov_id;
                Province *selected = p_list[prov_id];
                std::stringstream name_gen;
                name_gen << "Name: " << selected->getName().c_str();
                
                SDL_SetRenderTarget(disp, left_half);
                SDL_SetRenderDrawColor(disp, 2, 100, 2, 100);
                SDL_RenderFillRect(disp, nullptr);
                SDL_SetRenderDrawColor(disp, 2, 2, 2, 255);
                FC_Draw(fc_luxirb_mid, disp, 3, 3, "PROVINCE DATA");
                FC_Draw(fc_luxirb_mid, disp, 3, (0.10)*(global::screen_height - dy),
                    name_gen.str().c_str());
                
                FC_Draw(fc_luxirb_mid, disp, 3, (0.15)*(global::screen_height - dy),
                  Util::stat_msgs[selected->getStatus()]/*name_gen.str().c_str()*/);
                unsigned int num_settlements = selected->
                  getSettlements()->size();
                
                /* Print each settlement's name in this province */
                for (iter=0; iter < num_settlements; iter ++) {
                  name_gen.str("");
                  name_gen << selected->getSettlements()->at(iter)->getName();
                  FC_Draw(fc_luxirb_mid, disp, 3, (0.20+(0.05*(iter)))*(global::screen_height-dy),
                      /*Util::stat_msgs[selected->getStatus()]*/name_gen.str().c_str());
                }
                SDL_SetRenderTarget(disp, nullptr);
              }
            }
            redraw = true;
          } else if ((ev.type == SDL_MOUSEBUTTONUP) &&
              (ev.button.x > del_x) && (ev.button.y > dy)) {
            int xClick = (ev.button.x - del_x);
            xClick = (int) (((float)xClick) / scalar);
            int yClick = (ev.button.y - dy);
            yClick = (int) (((float)yClick) / scalar);
/*            std::cout<<"[DEBUG]    scalar="<<scalar<<std::endl;*/
/*            std::cout<<"[DEBUG]    internal x_coord="<<xClick<<std::endl;
            std::cout<<"[DEBUG]    internal y_coord="<<yClick<<std::endl;*/
/*            SDL_Rect rframe = {xClick, yClick, 1, 1};
            Uint8 red_ext, green_ext, blue_ext;*/
            Uint8 red_in, green_in, blue_in;
            
/*            std::cout << Util::getFormat(internal_map) << std::endl;*/
            Uint32 pixel = Util::getPixel(internal_map, xClick, yClick);
            SDL_GetRGB(pixel, rgb_format24, &red_in, &green_in, &blue_in);
/*            std::cout<<"[DEBUG]    red, green, blue = {"<<(int)(red_in)<<", "<<(int)(green_in)
                <<", "<<(int)(blue_in)<<"}"<<std::endl;*/
            if (ev.button.button == SDL_BUTTON_LEFT) {
//              if ((red_ext == 255) && (green_ext == 2) && (blue_ext == 6)) {
//                /*     */
//              } else if ((blue_ext != 232) || (green_ext != 162) || (red_ext >= 10)) {
/*                int prov_id = province_map[yClick / scaleF][xClick / scaleF];*/
                unsigned int prov_id, iter;
                prov_id = Util::hash(red_in, green_in, blue_in, 0);
/*                std::cout << "[DEBUG]    Comparing " << prov_id*//* << std::endl*//*;*/
/*                std::cout << " to " << prov_lim << " (province count)" << std::endl;*/
                if (prov_id < prov_lim) {
/*                  temp = prov_id;*/
                  Province *selected = p_list[prov_id];
                  std::stringstream name_gen;
                  name_gen << "Name: " << selected->getName();
                  
                  SDL_SetRenderTarget(disp, left_half);
                  SDL_SetRenderDrawColor(disp, 2, 100, 2, 100);
                  SDL_RenderFillRect(disp, nullptr);
                  SDL_SetRenderDrawColor(disp, 2, 2, 2, 255);
                  FC_Draw(fc_luxirb_mid, disp, 3, 3, "PROVINCE DATA");
                  FC_Draw(fc_luxirb_mid, disp, 3, (0.10)*(global::screen_height - dy),
                      name_gen.str().c_str());
                  
/*                  name_gen.str("");*/
/*                  name_gen << *(utils->get_status_str(selected->getStatus()));*/
                  FC_Draw(fc_luxirb_mid, disp, 3, (0.15)*(global::screen_height - dy), Util::stat_msgs[selected->getStatus()]/*name_gen.str().c_str()*/);
                  unsigned int num_settlements = selected->getSettlements()->size();
                  
                  /* Print each settlement's name in this province */
                  for (iter=0; iter < num_settlements; iter ++) {
                    name_gen.str("");
                    name_gen << selected->getSettlements()->at(iter)->getName();
                    FC_Draw(fc_luxirb_mid, disp, 3, (0.20+(0.05*(iter)))*(global::screen_height-dy),
                        /*Util::stat_msgs[selected->getStatus()]*/name_gen.str().c_str());
                  }
                  SDL_SetRenderTarget(disp, nullptr);
                }
//              }
            } else if (ev.button.button == SDL_BUTTON_RIGHT) {
              unsigned int prov_id;
              prov_id = Util::hash(red_in, green_in, blue_in, 0);
//              std::cout << "[DEBUG]    Comparing " << prov_id/* << std::endl*/;
//              std::cout << " to " << prov_lim << " (province count)" << std::endl;
              if (prov_id < prov_lim) {
                /* if there is a discrepancy, can print it here */
/*                if (prov_id != temp) {
                  std::cout << "[Non-fatal warning] chosen province does not match the data being displayed" << std::endl;
                }*/
                result = prov_id;
              }
            }
            redraw = true;
          } else if (ev.type == SDL_MOUSEBUTTONUP) {
/*            std::cout << "Event type is buttonup!" << std::endl;
            std::cout << "BUT   x coord is " << ev.button.x << std::endl;
            std::cout << "AND   y coord is " << ev.button.y << std::endl;*/
          } else if ((ev.type == SDL_WINDOWEVENT) &&
              (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
            redraw = true;
          }/* else if (ev.type == SDL_Event_TIMER) {
            if () {
              
              al_set_target_bitmap(left_half);
              al_set_target_bitmap(right_half);
              SDL_SetRenderTarget(disp, nullptr);
            }
          } else {
            
          }*/
        }
        if (redraw) {
          redraw = false;
          const SDL_Rect left_reg = {
              0, dy,
              del_x, static_cast<int>(global::screen_height-dy)
          };
          const SDL_Rect right_reg = {
              del_x, dy,
              static_cast<int>(global::screen_width-del_x),
              static_cast<int>(global::screen_height-dy)
          };
          SDL_RenderCopy(disp, left_half, nullptr, &left_reg);
          SDL_RenderCopy(disp, right_half, nullptr, &right_reg);
          SDL_RenderCopy(disp, instructText, nullptr, &instructTextLoc);
          SDL_RenderCopy(disp, instructText2, nullptr, &instructText2Loc);
          SDL_RenderPresent(disp);
        }
        SDL_Delay(33); // TODO make FPS a global var?
      }
      TTF_CloseFont(font_luxirb_mid);
      return result;
    }
  protected:
    Province **p_list;
    unsigned int prov_lim;
    size_t last_pid;
};

class InvadeSettleDisplay : public MapDisplay {
  public:
    InvadeSettleDisplay(/*SDL_Renderer *disp_p, SDL_Window n_win*, */
        SDL_Window *wm_win, int del, Province *prov,
        SDL_Surface *world_img, SDL_Surface *internal, TTF_Font *text_font) : 
            MapDisplay(/*disp_p, */wm_win, del, world_img, internal, text_font) {
      locale = prov;
      top_settlement_ind = prov->getSettlements()->size()-1;
      
      instructSurf = TTF_RenderUTF8_Solid(font,
          "ARROW KEYS - SEE SETTLEMENT DATA;", Util::white_text);
      instructText = SDL_CreateTextureFromSurface(disp, instructSurf);

      instructSurf2 = TTF_RenderUTF8_Solid(font,
          "ENTER - FINALIZE THE CHOICE", Util::white_text);
      instructText2 = SDL_CreateTextureFromSurface(disp, instructSurf2);
    }
    InvadeSettleDisplay(/*SDL_Renderer *disp_p, SDL_Window n_win*, */
        SDL_Window *wm_win, int del, Province *prov,
        SDL_Surface *world_img, SDL_Surface *internal) : 
            MapDisplay(/*disp_p, */wm_win, del, world_img, internal) {
      locale = prov;
      top_settlement_ind = prov->getSettlements()->size()-1;
      
      instructSurf = TTF_RenderUTF8_Solid(font,
          "ARROW KEYS - SEE SETTLEMENT DATA;", Util::white_text);
      instructText = SDL_CreateTextureFromSurface(disp, instructSurf);

      instructSurf2 = TTF_RenderUTF8_Solid(font,
          "ENTER - FINALIZE THE CHOICE", Util::white_text);
      instructText2 = SDL_CreateTextureFromSurface(disp, instructSurf2);
    }
    
    unsigned int launch() {
      const SDL_MessageBoxData cancel_box = {SDL_MESSAGEBOX_INFORMATION, window,
          "Close the Settlement selection window?",
          "Closing this window will take you back to the world map.",
          2, cancel_buttons, nullptr};
      const SDL_Rect instructTextLoc = {del_x+8, total_height-82,
          static_cast<int>(global::screen_width-del_x-10), 38};
      const SDL_Rect instructText2Loc = {del_x+8, total_height-42,
          static_cast<int>(global::screen_width-del_x-10), 38};
      int result = -2;
      bool redraw = true;
      int temp = 0/*result*/;
      int iter;
/*      utils = new Util();*/
      SDL_SetRenderTarget(disp, left_half);
      SDL_SetRenderDrawColor(disp, 2, 100, 2, 100);
      SDL_RenderFillRect(disp, nullptr);
      SDL_SetRenderTarget(disp, right_half);
    const SDL_Rect dt = {0, 0, static_cast<int>(scalar*orig_wid), static_cast<int>(scalar*orig_hgt)};
      SDL_RenderCopy(disp, external_map, nullptr, &dt);
      SDL_SetRenderTarget(disp, nullptr);
      
      /* Font initialization */
      TTF_Font *font_luxirb_mid = TTF_OpenFont("./res/fonts/luxirb.ttf", MID_FONT_SIZE);
      
      SDL_RenderFillRect(disp, nullptr); // Color (2, 160, 2, 100)
      const SDL_Rect left_reg = { 0, dy,
          del_x,
          static_cast<int>(global::screen_height-dy)
      };
      const SDL_Rect right_reg = { del_x, dy,
          static_cast<int>(global::screen_width-del_x),
          static_cast<int>(global::screen_height-dy)
      };
      SDL_RenderCopy(disp, left_half, nullptr, &left_reg);
      SDL_RenderCopy(disp, right_half, nullptr, &right_reg);
      SDL_SetRenderTarget(disp, left_half);
      SDL_RenderFillRect(disp, nullptr);
      std::stringstream name_gen;
      SDL_SetRenderDrawColor(disp, 2, 2, 2, 255);
      
      /* Print each settlement's name in this province */
      FC_Draw(fc_luxirb_mid, disp, 3, (0.15*(global::screen_height-dy)), "CANCEL");
      for (iter=0; iter <= top_settlement_ind; iter ++) {
        name_gen.str("");
        name_gen << locale->getSettlements()->at(iter)->getName();
        FC_Draw(fc_luxirb_mid, disp, 3, (0.20+(0.05*(iter)))*(global::screen_height-dy), name_gen.str().c_str());
      }
      SDL_Rect frame = {
          3, static_cast<int>(global::screen_height-dy) / 5,
          del_x-6, static_cast<int>(global::screen_height-dy) / 20
      };
      SDL_RenderDrawRect(disp, &frame);
      SDL_SetRenderTarget(disp, nullptr);
      SDL_SetRenderDrawColor(disp, 2, 160, 2, 100);
      SDL_RenderFillRect(disp, nullptr);
      
      frame.x = 0;
      frame.y = 0;
      frame.w = del_x;
      frame.h = total_height;
      SDL_RenderCopy(disp, left_half, nullptr, &frame);
      
      frame.x = del_x;
      frame.y = 0;
      frame.w = total_width-del_x;
      frame.h = total_height;
      SDL_RenderCopy(disp, right_half, nullptr, &frame);
      
      SDL_RenderCopy(disp, instructText, nullptr, &instructTextLoc);
      SDL_RenderCopy(disp, instructText2, nullptr, &instructText2Loc);
      SDL_RenderPresent(disp);
      
      /* The display loop */
      while (result < -1) {
        SDL_Event ev;
        while (SDL_PollEvent(&ev)) {
          if (ev.type == SDL_KEYDOWN) {
            if (ev.key.keysym.sym == SDLK_q) {
              int cancel = 0;
              SDL_ShowMessageBox(&cancel_box, &cancel);
              if (cancel == 1) {
                result = -1;
              }
            } else {
              redraw = true;
              switch (ev.key.keysym.sym) {
                case SDLK_KP_1:
                case SDLK_1:
                  {
                    temp = ((0 < top_settlement_ind) ? 0 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_2:
                case SDLK_2:
                  {
                    temp = 1;
                    temp = ((1 < top_settlement_ind) ? 1 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_3:
                case SDLK_3:
                  {
                    temp = ((2 < top_settlement_ind) ? 2 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_4:
                case SDLK_4:
                  {
                    temp = ((3 < top_settlement_ind) ? 3 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_5:
                case SDLK_5:
                  {
                    temp = ((4 < top_settlement_ind) ? 4 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_6:
                case SDLK_6:
                  {
                    temp = ((5 < top_settlement_ind) ? 5 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_7:
                case SDLK_7:
                  {
                    temp = ((6 < top_settlement_ind) ? 6 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_8:
                case SDLK_8:
                  {
                    temp = ((7 < top_settlement_ind) ? 7 : top_settlement_ind);
                    break;
                  }
                case SDLK_KP_9:
                case SDLK_9:
                  {
                    temp = ((8 < top_settlement_ind) ? 8 : top_settlement_ind);
                    break;
                  }
                case SDLK_DOWN:
                  {
                    if (temp < top_settlement_ind) {
                      temp++;
                    }
                    break;
                  }
                case SDLK_UP:
                  {
                    if (temp >= 0) {
                      temp--;
                      redraw = true;
                    }
                    break;
                  }
                case SDLK_RETURN:
                case SDLK_KP_ENTER:
                  {
                    /* confirm the settlement choice */
                    result = temp;
                    break;
                  }
                case SDLK_KP_0:
                case SDLK_0:
                case SDLK_PAGEUP:
                  {
                    temp = -1;
                    break;
                  }
                case SDLK_PAGEDOWN:
                  {
                    temp = top_settlement_ind;
                    break;
                  }
                default:
                  
                  break;
              }
              SDL_SetRenderTarget(disp, left_half);
              SDL_SetRenderDrawColor(disp, 2, 100, 2, 100);
              SDL_RenderFillRect(disp, nullptr);
              std::stringstream name_gen;
              /* Print each settlement's name in this province */
              FC_Draw(fc_luxirb_mid, disp, 3, (0.15*(global::screen_height-dy)), "CANCEL");
              for (iter=0; iter <= top_settlement_ind; iter ++) {
                name_gen.str("");
                name_gen << locale->getSettlements()->at(iter)->getName();
                FC_Draw(fc_luxirb_mid, disp, 3, (0.20+(0.05*(iter)))*(global::screen_height-dy), name_gen.str().c_str());
              }
              SDL_SetRenderDrawColor(disp, 2, 2, 2, 255);
              frame.x = 3;
              frame.y = (0.21+(0.05*temp))*(global::screen_height-dy);
              frame.w = del_x-6;
              frame.h = 0.05*(global::screen_height-dy);
              SDL_RenderDrawRect(disp, &frame);
              SDL_SetRenderTarget(disp, nullptr);
/*              }if () {
              int = ( - 48);*/
            }
          } else if (ev.type == SDL_WINDOWEVENT &&
              (ev.window.event == SDL_WINDOWEVENT_CLOSE)) {
            int cancel = 0;
            SDL_ShowMessageBox(&cancel_box, &cancel);
            if (cancel == 1) {
              result = -1;
            }
          } else if ((ev.type == SDL_MOUSEBUTTONUP) &&
              (ev.button.windowID == SDL_GetWindowID(wmsl_win))) {
/*            int xClick = (ev.button.x - del_x) / scalar;*/
            int yClick = ev.button.y / scalar;
            if (ev.button.which == SDL_BUTTON_LEFT) { /* left click */
              if ((ev.button.x / scalar) < del_x) {
                if (yClick > MID_FONT_SIZE) {
                  /* Box the chosen Settlement name... maybe even show data
                   * about that settlement though the Player wouldn't normally
                   * know much about the Settlement a priori... */
                  float prop = ((ev.button.y / 0.05) / global::screen_height) - 4;
                  std::cout << "    Proportion of the screen calculated as " << prop << std::endl;
                  if (prop >= 0 && ((int)prop) <= top_settlement_ind) {
                    temp = (int) prop;
#ifdef DEBUG_MSG
                    std::cout << "[DEBUG] Proportion NOT flagged as invalid. Full details:" << std::endl;
                    std::cout << "[DEBUG] ((y_coord / 0.05) / global::screen_height) - 4 = ";
                    std::cout << "(" << ev.button.y << "/0.05) / " << global::screen_height << " - 4" << std::endl;
                    std::cout << "[DEBUG] (" << (ev.button.y/0.05) << ") / " << global::screen_height << std::endl;
                    std::cout << "[DEBUG] (" << (ev.button.y/0.05/global::screen_height) << ") - 4" << std::endl;
                    std::cout << "[DEBUG] " << prop << std::endl;
#endif
                  } else {
                    std::cout << "[ ERR ] Proportion invalid! Full details:" << std::endl;
                    std::cout << "[ ERR ] ((y_coord / 0.05) / global::screen_height) - 4 = ";
                    std::cout << "(" << ev.button.y << "/0.05) / " << global::screen_height << " - 4" << std::endl;
                    std::cout << "[ ERR ] (" << (ev.button.y/0.05) << ") / " << global::screen_height << std::endl;
                    std::cout << "[ ERR ] (" << ((ev.button.y/0.05)/global::screen_height) << ") - 4" << std::endl;
  /*                   << " - 4"*/
                    std::cout << "[ ERR ] " << prop << std::endl;
                  }
                  SDL_SetRenderDrawColor(disp, 2, 2, 2, 255);
                  frame.x = 3;
                  frame.y = (0.21+(0.05*temp))*(global::screen_height-dy);
                  frame.w = (del_x-3)-(3);
                  frame.h = ((0.26+(0.05*temp))*(global::screen_height-dy))-((0.21+(0.05*temp))*(global::screen_height-dy));
                  SDL_RenderDrawRect(disp, &frame);
                }
              } else { }
            } else if (ev.button.which == SDL_BUTTON_RIGHT) {
            }
            redraw = true;
          } else if ((ev.type == SDL_WINDOWEVENT) &&
              (ev.window.event == SDL_WINDOWEVENT_ENTER)) {
            redraw = true;
          }/* else if (ev.type == SDL_Event_TIMER) {
            if () {
              
              al_set_target_bitmap(left_half);
              al_set_target_bitmap(right_half);
              SDL_SetRenderTarget(disp, nullptr);
            }
          } else {
            
          }*/
        }
        if (redraw) {
          redraw = false;
          SDL_SetRenderDrawColor(disp, 2, 160, 2, 100);
          SDL_RenderFillRect(disp, nullptr);
          const SDL_Rect left_reg = {
              0, dy, del_x,
              static_cast<int>(global::screen_height-dy)
          };
          const SDL_Rect right_reg = {
              del_x, dy,
              static_cast<int>(global::screen_width-del_x),
              static_cast<int>(global::screen_height-dy)
          };
          SDL_RenderCopy(disp, left_half, nullptr, &left_reg);
          SDL_RenderCopy(disp, right_half, nullptr, &right_reg);
          SDL_RenderCopy(disp, instructText, nullptr, &instructTextLoc);
          SDL_RenderCopy(disp, instructText2, nullptr, &instructText2Loc);
/*          al_draw_text(font_luxirb_mid, al_map_rgb(250, 250, 250),
            del_x+8, total_height-82, ALLEGRO_ALIGN_LEFT,
            "ARROW KEYS - SEE SETTLEMENT DATA;");
          al_draw_text(font_luxirb_mid, al_map_rgb(250, 250, 250),
            del_x+8, total_height-42, ALLEGRO_ALIGN_LEFT,
            "ENTER - FINALIZE THE CHOICE");*/
          SDL_RenderPresent(disp);
        }
        SDL_Delay(33); // TODO make FPS a global var?
      }
      TTF_CloseFont(font_luxirb_mid);
      return result;
    }
  
  private:
  Province *locale;
  int top_settlement_ind=(-1);
};

