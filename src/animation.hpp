#pragma once
/* My own CIRCULAR linked list implementation,
 * to be used exclusively with movement animation sequences. 
 * -VC, created 2019-07-17/18. */
#ifndef SDL2_image
  #ifdef __linux__
    #include <SDL2/SDL_image.h>
  #else
    #include <SDL_image.h>
  #endif
  #define SDL2_image
#endif

#include <string>

class Frame {
  public:
/*    Frame(SDL_Texture *sprite);*/
/*    Frame(SDL_Renderer *r, SDL_Surface *sprite);*/
    Frame(/*SDL_Renderer *r, */const std::string &sprite);
    ~Frame();
    void setNextFrame(Frame *s);
    Frame *getNextFrame() const;
    SDL_Texture *getSprite();
    void actualize(SDL_Renderer *renderer);
    void deActualize();

  protected:
    SDL_Surface *m_surf;
    SDL_Texture *m_sprite;
    Frame *m_next;
};

class CircularList {
  public:
    CircularList();
    ~CircularList();
/*    void addFrame(SDL_Texture *s);*/
    void addFrame(/*SDL_Renderer *ren, */const std::string &s);
    Frame *getHead() const;
    void actualizeAll(SDL_Renderer *r);
    void deActualizeAll();

  protected:
    Frame *head;

  private:
    /* A recursive (non-tail recursive!) method
     * in order to free ALL the data in the circular list.
     * But, since it is only expected to hold < 10 items, 
     * I'm pretty sure it's fine that it's not tail-recursive.
     * We won't break the stack :D */
    void deleteAllFrames(Frame *current);
};
