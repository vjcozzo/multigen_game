#pragma once

/* ***
 * EnemyMove - an object to store concise data
 * regarding the move that an enemy is going to take.

 * For now, all this data includes is:
 * - a destination (an ordered pair representing where the enemy plans to move)
 * - a target (an ordered pair representing the target of the enemy unit,
 or null if not applicable)
 * - action, an integer obeying the following code:
 0 => idle (meaning no target should be supplied)
 1 => attack (meaning the target is indeed an attack target)
 2 => give-item?
 4 => use-item?
 5 => ... haven't decided yet.
 6 => ... what other actions can an enemy take?
 7 => ... idk, taunt the player?
 8 => ... we'll have to think/talk about it.
 9 => Ok, so no one else is working on this project.
 10=> (Thanks guys). So That means I have to do it myself.
 * I expect that the actual response to an action
 * (e.g., the actual call to .attack(...))
 * will take place somewhere else.
 * This class is just a structure holding a bunch of data.  */
enum ENEMY_ACT {
  NOP, ATTACK, GIVE_ITEM, USE_ITEM, TAUNT, SURRENDER
};

#include "entity.hpp"
#include "unit.hpp"

/* For TMB Terrain Tiles */
enum TERRAIN_TYPE {
  OCEAN=0, PLAINS, FOREST, HILLS, ICE, SAND, MOUNTAINS
};

class EnemyMove {
  public:
    EnemyMove(Entity *dest, Entity *tar, ENEMY_ACT intention) {
      destination = dest;
      target = tar;
      action = intention;
    }
    ~EnemyMove() {
      if (destination != nullptr) {
        delete destination;
      }
      if (target != nullptr) {
        delete target;
      }
    }

    Entity *getDestination() const {
      return destination;
    }

    Entity *getTarget() const {
      return target;
    }

    ENEMY_ACT getAction() const {
      return action;
    }

    std::string toString() const {
      std::stringstream nameGen;
      std::string result = "";
      nameGen<<"EnemyMove: to ("<<destination->getX()<<", "
          <<destination->getY()<<")";
      nameGen.flush();
      result = nameGen.str();
      return result;
    }
  
  private:
    Entity *destination, *target;
    ENEMY_ACT action;
};

/* A basic AI for the enemy team of a turn-based tactical game.
 * For now, it makes a decision for an individual enemy unit.
 * (A work in progress) */
class EnemyAI {
  public:
    EnemyAI(Unit*** map, TERRAIN_TYPE** land, Unit *unit, int num_tiles_w, int num_tiles_h) {
      unit_map = map;
      land_map = land;
      currentEnemy = unit;
      /*            if (unit == nullptr) {
       throw new UnsupportedOperationException("ERR - no unit found at that tile!");
       }
       else if (unit->getTeam() != 1) {
       throw new UnsupportedOperationException("ERR - unit at that tile is not an enemy unit!");
       }*/
      tilesX = num_tiles_w;
      tilesY = num_tiles_h;
      opponentSideNW = 0;
      opponentSideNE = 0;
      opponentSideSE = 0;
      opponentSideSW = 0;
      enemySide = 0;
    }
    
    /* An alternate Constructor, in case we need it.
     * I doubt we will, but I include it just in case 
     * for some reason we DON'T have immediate access to
     * the enemy Unit object reference
     * when we want to use this class. 
     * 
     * Or, better yet, in the future we might consolidate the EnemyAI framework,
     * e.g. make just one massive EnemyAI rather than separate
     * EnemyAI objects for each enemy unit. (That way, enemy's strategies 
     * may be better coordinated with one another in a grand strategy.) 
     * */
    EnemyAI(Unit*** map, TERRAIN_TYPE** land, int col, int row, int num_tiles_w, int num_tiles_h) {
      unit_map = map;
      land_map = land;
      currentEnemy = map[row][col];
      /*            if (currentEnemy == nullptr) {
       throw new UnsupportedOperationException("ERR - no unit found at that tile!");
       }
       else if (currentEnemy->getTeam() != 1) {
       throw new UnsupportedOperationException("ERR - unit at that tile is not an enemy unit!");
       }*/
      tilesX = num_tiles_w;
      tilesY = num_tiles_h;
    }
    ~EnemyAI() {
      
    }
    
    EnemyMove *decide() {
      int playerUnits;
      scanAllZones();
      playerUnits = (opponentSideNW + opponentSideNE + opponentSideSE + opponentSideSW);
      /*        printf("\tThere are " + playerUnits
       + " player units within range...");
       printf("\t" + opponentSideNW +
       " players to the NorthWest,\n\t" + opponentSideNE
       + " players to the NorthEast,");
       printf("\t" + opponentSideSE +
       " players to the SouthEast,\n\t" + opponentSideSW
       + " players to the SouthWest,");
       */
      
      if ((enemySide+1) < playerUnits) {
        if ((opponentSideNW > opponentSideNE) &&
            (opponentSideNW > opponentSideSE) &&
            (opponentSideNW > opponentSideSW)) {
          /*          std::cout<<"Need to retreat SE"std::endl;*/
          Entity *destination = eagerTravelSE();
          // Make the EnemyMove objec and then return it!
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNE > opponentSideNW) &&
                   (opponentSideNE > opponentSideSE) &&
                   (opponentSideNE > opponentSideSW)) {
          /*          std::cout<<"Need to retreat SW"std::endl;*/
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideSE > opponentSideNE) &&
                   (opponentSideSE > opponentSideNW) &&
                   (opponentSideSE > opponentSideSW)) {
          /*          std::cout<<"Need to retreat NW"<<std::endl;*/
          Entity *destination = eagerTravelNW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideSW > opponentSideNE) &&
                   (opponentSideSW > opponentSideSE) &&
                   (opponentSideSW > opponentSideNW)) {
          /*          std::cout<<"Need to retreat NW"<<std::endl;*/
          Entity *destination = eagerTravelNW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        }/* ***
          * TO DO: add more cases, so that
          * even if there is an equal number of player units
          * located in different zones,
          * the AI makes a decision that makes sense.
          * 
          * To do this, I might want to write a moveWest() method, etc.
          * to be able to travel in the four primary directions.
          * **/
        /* NOTE: if none of the above four cases applies,
         * then there is a tie.
         * */
        else if ((opponentSideNE == opponentSideNW) &&
                 (opponentSideNE > opponentSideSE) &&
                 (opponentSideNE > opponentSideSW)) {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNE > opponentSideNW) &&
                   (opponentSideNE == opponentSideSE) &&
                   (opponentSideNE > opponentSideSW)) {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNE > opponentSideNW) &&
                   (opponentSideNE > opponentSideSE) &&
                   (opponentSideNE == opponentSideSW)) {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNW > opponentSideNE) &&
                   (opponentSideNW > opponentSideSE) &&
                   (opponentSideNW == opponentSideSW)) {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNW > opponentSideNE) &&
                   (opponentSideNW == opponentSideSE) &&
                   (opponentSideNW > opponentSideSW)) {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideSW > opponentSideNE) &&
                   (opponentSideSW == opponentSideSE) &&
                   (opponentSideSW > opponentSideNW)) {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
          /* Only one broad case remains:
           * there is a three-way tie (or four-way tie, but that can fit into an else case)
           * */
        } else if ((opponentSideNE < opponentSideNW) &&
                   (opponentSideNE < opponentSideSE) &&
                   (opponentSideNE < opponentSideSW)) {
          /*          std::cout<<"Need to retreat SW"<<std::endl;*/
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNE < opponentSideNW) &&
                   (opponentSideNE < opponentSideSE) &&
                   (opponentSideNE < opponentSideSW)) {
          /*          std::cout<<"Need to retreat SW"<<std::endl;*/
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else if ((opponentSideNE < opponentSideNW) &&
                   (opponentSideNE < opponentSideSE) &&
                   (opponentSideNE < opponentSideSW)) {
          /*          std::cout<<"Need to retreat SW"std::endl;*/
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        } else {
          Entity *destination = eagerTravelSW();
          EnemyMove *result = new EnemyMove(destination, nullptr, NOP);
          return result;
        }
      } else {
        /* No need to retreat.... yet. */
        Entity *destination = eagerTravelNW();
        EnemyMove *attack = new EnemyMove(destination, nullptr, NOP);
        return attack;
      }
    }
  private:
    Unit*** unit_map;
    TERRAIN_TYPE** land_map;
    Unit *currentEnemy;
    int tilesX, tilesY;
    int enemySide;
    int opponentSideNW, opponentSideNE,
        opponentSideSE, opponentSideSW;

    void scanAllZones() {
      scanNE();
      scanSE();
      scanSW();
      scanNW();
    }

    void scanNE() {
      int speed = currentEnemy->getSpeed();
      int y = currentEnemy->getY()-1;
      int j = 1;
      while ((j <= speed) && (y >= 0)) {
        int x = currentEnemy->getX();
        int i = 0;
        while (((i+j) <= speed) && (x < tilesX)) {
          if (unit_map[y][x] != nullptr) {
            Unit *nextUnit = unit_map[y][x];
            if (nextUnit->getTeam() == 1) {
              enemySide ++;
            } else {
              opponentSideNE ++;
            }
          }
          x++;
          i++;
        }
        y--;
        j++;
      }
    }

    void scanSE() {
      int speed = currentEnemy->getSpeed();
      int x = currentEnemy->getX()+1;
      int i = 1;
      while ((i <= speed) && (x < tilesX)) {
        int y = currentEnemy->getY(); int j = 0;
        while (((i+j) <= speed) && (y < tilesY)) {
          if (unit_map[y][x] != nullptr) {
            Unit *nextUnit = unit_map[y][x];
            if (nextUnit->getTeam() == 1) {
              enemySide ++;
            } else {
              opponentSideSE ++;
            }
          }
          y++;
          j++;
        }
        x++;
        i++;
      }
    }

    void scanSW() {
      int speed = currentEnemy->getSpeed();
      int y = currentEnemy->getY()+1;
      int j = 1;
      while ((j <= speed) && (y < tilesY)) {
        int x = currentEnemy->getX();
        int i = 0;
        while (((i+j) <= speed) && (x >= 0)) {
          if (unit_map[y][x] != nullptr) {
            Unit *nextUnit = unit_map[y][x];
            if (nextUnit->getTeam() == 1) {
              enemySide ++;
            } else {
              opponentSideSW ++;
            }
          }
          x--;
          i++;
        }
        y++;
        j++;
      }
    }

    void scanNW() {
      int speed = currentEnemy->getSpeed();
      int x = currentEnemy->getX()-1;
      int i = 1;
      while ((i <= speed) && (x >= 0)) {
        int y = currentEnemy->getY();
        int j = 0;
        while (((i+j) <= speed) && (y >= 0)) {
          if (unit_map[y][x] != nullptr) {
            Unit *nextUnit = unit_map[y][x];
            if (nextUnit->getTeam() == 1) {
              enemySide ++;
            } else {
              opponentSideNW ++;
            }
          }
          y--;
          j++;
        }
        x--;
        i++;
      }
    }

    /* Below are 'movement' methods */
    Entity *eagerTravelNEAux(int remainingSpeed, int x, int y) {
      if ((x < tilesX) && (x >= 0) && (y >= 0) && (y < tilesY)) {
        if (remainingSpeed == 0) {
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          return (new Entity(x, y, nullptr));
        } else {
          /* The algorithm should be along the lines of:
           * 1) Try this point
           * 2) If it fails for some reason (ocean or occupied) then:
           * 3) -move up or right depending on remainingSpeed.
           * */
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          Entity *result = new Entity(x, y, nullptr);
          if ((remainingSpeed % 2) == 0) {
            Entity *fartherResult = eagerTravelNEAux(remainingSpeed-1, x, y-1);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelNEAux(remainingSpeed-1, x+1, y);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          } else {
            Entity *fartherResult = eagerTravelNEAux(remainingSpeed-1, x+1, y);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelNEAux(remainingSpeed-1, x, y-1);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          }
        }

      } else {
        return nullptr;
      }
    }

    Entity *eagerTravelNE() {
      Entity *recResult = eagerTravelNEAux(1+(generator() % currentEnemy->getSpeed()), currentEnemy->getX(), currentEnemy->getY());
      if (recResult == nullptr) {
        return (new Entity(currentEnemy->getX(), currentEnemy->getY(), nullptr));
      } else {
        return recResult;
      }
    }

    Entity *eagerTravelSEAux(int remainingSpeed, int x, int y) {
      if ((x < tilesX) && (x >= 0) && (y >= 0) && (y < tilesY)) {
        if (remainingSpeed == 0) {
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          return (new Entity (x, y, nullptr));
        } else {
          /* The algorithm should be along the lines of:
           * 1) Try this point
           * 2) If it fails for some reason (ocean or occupied) then:
           * 3) -move up or right depending on remainingSpeed.
           * */
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          Entity *result = new Entity(x, y, nullptr);;
          if ((remainingSpeed % 2) == 0) {
            Entity *fartherResult = eagerTravelSEAux(remainingSpeed-1, x+1, y);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelSEAux(remainingSpeed-1, x, y+1);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          } else {
            Entity *fartherResult = eagerTravelSEAux(remainingSpeed-1, x, y+1);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelSEAux(remainingSpeed-1, x+1, y);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          }
        }
      } else {
        return nullptr;
      }
    }

    Entity *eagerTravelSE() {
      Entity *recResult = eagerTravelSEAux(1+(generator() % currentEnemy->getSpeed()), currentEnemy->getX(), currentEnemy->getY());
      if (recResult == nullptr) {
        return (new Entity(currentEnemy->getX(), currentEnemy->getY(), nullptr));
      } else {
        return recResult;
      }
    }

    Entity *eagerTravelSWAux(int remainingSpeed, int x, int y) {
      if ((x < tilesX) && (x >= 0) && (y >= 0) && (y < tilesY)) {
        if (remainingSpeed == 0) {
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          return (new Entity (x, y, nullptr));
        } else {
          /* The algorithm should be along the lines of:
           * 1) Try this point
           * 2) If it fails for some reason (ocean or occupied) then:
           * 3) -move up or right depending on remainingSpeed.
           * */
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          Entity *result = new Entity(x, y, nullptr);
          if ((remainingSpeed % 2) == 0) {
            Entity *fartherResult = eagerTravelSWAux(remainingSpeed-1, x, y+1);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelSWAux(remainingSpeed-1, x-1, y);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult; /* nullptr! */
              }
            } else {
              delete result;
              return fartherResult;
            }
          } else {
            Entity *fartherResult = eagerTravelSWAux(remainingSpeed-1, x-1, y);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelSWAux(remainingSpeed-1, x, y+1);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          }
        }
      } else {
        return nullptr;
      }
    }

    Entity *eagerTravelSW() {
      Entity *recResult = eagerTravelSWAux(1+(generator() % currentEnemy->getSpeed()), currentEnemy->getX(), currentEnemy->getY());
      if (recResult == nullptr) {
        return (new Entity(currentEnemy->getX(), currentEnemy->getY(), nullptr));
      } else {
        return recResult;
      }
    }

    Entity *eagerTravelNWAux(int remainingSpeed, int x, int y) {
      /*        printf("\t\tnow searching NorthWest at the point ("+x+","+y+") where decremented val="+remainingSpeed);*/
      if ((x < tilesX) && (x >= 0) && (y >= 0) && (y < tilesY)) {
        if (remainingSpeed == 0) {
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          return (new Entity (x, y, nullptr));
        } else {
          /* The algorithm should be along the lines of:
           * 1) Try this point
           * 2) If it fails for some reason (ocean or occupied) then:
           * 3) -move up or right depending on remainingSpeed.
           * */
          if ((land_map[y][x] == OCEAN) || ((unit_map[y][x] != nullptr) && (unit_map[y][x] != currentEnemy))) {
            return nullptr;
          }
          Entity *result = new Entity(x, y, nullptr);
          if ((remainingSpeed % 2) == 0) {
            Entity *fartherResult = eagerTravelNWAux(remainingSpeed-1, x, y-1);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelNWAux(remainingSpeed-1, x-1, y);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          } else {
            Entity *fartherResult = eagerTravelNWAux(remainingSpeed-1, x-1, y);
            if (fartherResult == nullptr) {
              fartherResult = eagerTravelNWAux(remainingSpeed-1, x, y-1);
              if (fartherResult == nullptr) {
                return result;
              } else {
                delete result;
                return fartherResult;
              }
            } else {
              delete result;
              return fartherResult;
            }
          }
        }
      } else {
        return nullptr;
      }
    }

    Entity *eagerTravelNW() {
      Entity *recResult = eagerTravelNWAux(1+(generator() % currentEnemy->getSpeed()), currentEnemy->getX(), currentEnemy->getY());
      if (recResult == nullptr) {
        return (new Entity(currentEnemy->getX(), currentEnemy->getY(), nullptr));
      } else {
        return recResult;
      }
    }
};
